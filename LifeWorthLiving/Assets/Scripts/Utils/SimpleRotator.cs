﻿using UnityEngine;
using System.Collections;

public class SimpleRotator : MonoBehaviour {

	public float speed;
	private Transform m_Transform;

	// Use this for initialization
	void Start () {
		m_Transform = transform;
	}
	
	// Update is called once per frame
	void Update () {
		m_Transform.Rotate (0, 0, speed * Time.deltaTime);
	}
}
