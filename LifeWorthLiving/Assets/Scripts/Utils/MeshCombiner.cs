﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class MeshCombiner : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Combine ();
	}
	
	public void Combine() {
		List<CombineInstance> combineList = new List<CombineInstance> ();

		MeshRenderer myMeshRenderer = GetComponent<MeshRenderer> ();
		Material materialToCombine = myMeshRenderer.sharedMaterial;

		MeshFilter[] meshFilters = GetComponentsInChildren<MeshFilter>();
		int i = 0;
		while (i < meshFilters.Length) {
			CombineInstance tempCombine = new CombineInstance ();

			MeshRenderer tempRenderer = meshFilters [i].gameObject.GetComponent<MeshRenderer> ();
			if (tempRenderer != null && tempRenderer.sharedMaterial == materialToCombine) {
				tempCombine.mesh = meshFilters [i].sharedMesh;
				tempCombine.transform = meshFilters [i].transform.localToWorldMatrix;
				meshFilters [i].gameObject.SetActive (false);
			}
			i++;
		}
		transform.GetComponent<MeshFilter>().mesh = new Mesh();
		transform.GetComponent<MeshFilter>().mesh.CombineMeshes(combineList.ToArray());
		transform.gameObject.SetActive (true);

		//combineList.Clear ();
		//combineList = null;
	}
}
