﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public enum AnimationMoveType {
	FromLeft,
	FromRight,
	FromAbove,
	FromBelow,
	ToLeft,
	ToRight,
	ToAbove,
	ToBelow
};
public enum AnimationType {
	In,
	Out,
	Pop
};

public enum GameState {
	FirstLoad,
	MainMenu,
	LoadingGame,
	Playing,
	Paused,
	GameOver
}

/// <summary>
/// Representation of 2D integer points.
/// </summary>
[System.Serializable]
public struct Vector2i {
	[SerializeField] public int a, b;

	/// <summary>
	/// Initializes a new instance of the <see cref="Vector2i"/> class.
	/// </summary>
	/// <param name="a">The a component.</param>
	/// <param name="b">The b component.</param>
	public Vector2i(int a = 0, int b = 0) {
		this.a = a;
		this.b = b;
	}

	/// <summary>
	/// Calculates the sum of this Vector2i and the specified Vector2i V.
	/// Returns a new instance of Vector2i that equals the sum of this Vector2i and V.
	/// </summary>
	/// <param name="v">Vector2i to be added.</param>
	public Vector2i Add(Vector2i v) {
		return Add (v.a, v.b);
	}
	/// <summary>
	/// Add the specified a and b.
	/// Returns a new instance of Vector2i that equals the sum of this Vector2i and Vector2i(a, b).
	/// </summary>
	/// <param name="a">The value to be added to the a component.</param>
	/// <param name="b">The value to be added to the b component.</param>
	public Vector2i Add(int a, int b) {
		return new Vector2i (this.a + a, this.b + b);
	}

	/// <summary>
	/// Returns true if this Vector2i has the same values as the one passed as parameter.
	/// </summary>
	/// <returns><c>true</c>, if both Vector2i have the same values <c>false</c> otherwise.</returns>
	/// <param name="v">V.</param>
	public bool isEqual(Vector2i v) {
		return (a == v.a && b == v.b);
	}

	public static float DistanceSqr(Vector2i v1, Vector2i v2) {
		return ((v1.a - v2.a) * (v1.a - v2.a)) + ((v1.b - v2.b) * (v1.b - v2.b));
	}

	public static int ManhattanDistance(Vector2i v1, Vector2i v2) {
		return Mathf.Abs (v1.a - v2.a) + Mathf.Abs (v1.b - v2.b);
	}

	/// <summary>
	/// Shorthand for writing Vector2i(0, 0).
	/// </summary>
	/// <value>A new instance of Vector2i with both a and b set to 0.</value>
	public static Vector2i Zero {get{ return new Vector2i (); }
	}
		
	public override string ToString() {
		return "(" + a + ", " + b + ")";
	}
}

/// <summary>
/// Custom Update Interface. Must be implemented by all objects that would have Update() implemented.
/// </summary>
public interface ICustomUpdate {
	/// <summary>
	/// The Update method. Called every frame that the object should be updated.
	/// </summary>
	/// <param name="deltaTime">Custom delta time. Takes into account Pauses, freezes, turbos, etc.</param>
	void DoUpdate (float deltaTime);
}

/// <summary>
/// Custom Fixed Update Interface. Must be implemented by all objects that would have FixedUpdate() implemented.
/// </summary>
public interface ICustomFixedUpdate {
	/// <summary>
	/// The Fixed Update method. Called every Fixed Time that the object should be updated.
	/// </summary>
	void DoFixedUpdate ();
}

/// <summary>
/// A Poolable Object.
/// </summary>
public interface IPoolable {
	/// <summary>
	/// Activate this instance.
	/// </summary>
	void activate();

	/// <summary>
	/// Deactivate this instance.
	/// </summary>
	void deactivate();
}

/// <summary>
/// A Hashable Object.
/// </summary>
public interface IHashable {
	/// <summary>
	/// Gets the hash.
	/// </summary>
	/// <returns>The hash.</returns>
	int getHash ();
}

/// <summary>
/// Used to receive calls that an object is no longer needed, and may be deactivated.
/// Example: platforms that left the player's field of view.
/// </summary>
public interface IDeactivateSignalling {
	/// <summary>
	/// Signals to deactivate the object.
	/// </summary>
	void signalDeactivate ();
}

/// <summary>
/// On operation done.
/// </summary>
public delegate void OnOperationDone ();

/// <summary>
/// An Indexed Array.
/// Encapsulates an array and an index in the array. 
/// Each getNext() call returns the object in that position and increments the array, looping back to the begining.
/// Can be used as a circular pool.
/// </summary>
public class IndexedArray<T>
{
	private T[] objects;
	private int index;

	/// <summary>
	/// Initializes a new instance of the <see cref="IndexedArray"/> class.
	/// </summary>
	/// <param name="count">The starting length of the array.</param>
	public IndexedArray(int count) {
		objects  = new T[count];
		index = 0;
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="IndexedArray`1"/> class.
	/// </summary>
	/// <param name="_objects">The array to be used.</param>
	public IndexedArray(T[] _objects) {
		objects = _objects;
		index = 0;
	}

	/// <summary>
	/// Gets the next object on the array, and increments the index.
	/// </summary>
	/// <returns>The next object.</returns>
	public T getNext() {
		T value = objects [index];
		index = (index+1) % objects.Length;
		return value;
	}

	public T getPrevious() {
		T value = objects [index--];
		if (index == -1)
			index += objects.Length;
		return value;
	}

	/// <summary>
	/// Adds the specified objects to the end of the array.
	/// </summary>
	/// <param name="_objects">The objects to be added at the end of the array.</param>
	public void addToArray(T[] _objects) {
		T[] newObjects = new T[objects.Length + _objects.Length];
		int i = 0;
		for (i = 0; i < objects.Length; i++) {
			newObjects [i] = objects [i];
		}
		for (i = 0; i < _objects.Length; i++) {
			newObjects [i + objects.Length] = _objects [i];
		}
		objects = newObjects;
	}

	/// <summary>
	/// Reset the index.
	/// </summary>
	public void resetIndex() {
		index = 0;
	}

	/// <summary>
	/// Get all the objects currently in this array.
	/// </summary>
	/// <returns>All the objects.</returns>
	public T[] getAllObjects() {
		return objects;
	}
}

/// <summary>
/// A Serializable Indexed Array.
/// Encapsulates an array and an index in the array. 
/// Each getNext() call returns the object in that position and increments the array, looping back to the begining.
/// Can be used as a circular pool.
/// </summary>
[System.Serializable]
public class SerialIndexedArray<T>
{
	[SerializeField] private T[] objects;
	[SerializeField] private int index;

	/// <summary>
	/// Initializes a new instance of the <see cref="IndexedArray"/> class.
	/// </summary>
	/// <param name="count">The starting length of the array.</param>
	public SerialIndexedArray(int count) {
		objects  = new T[count];
		index = 0;
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="IndexedArray`1"/> class.
	/// </summary>
	/// <param name="_objects">The array to be used.</param>
	public SerialIndexedArray(T[] _objects) {
		objects = _objects;
		index = 0;
	}

	/// <summary>
	/// Gets the next object on the array, and increments the index.
	/// </summary>
	/// <returns>The next object.</returns>
	public T getNext() {
		T value = objects [index];
		index = (index+1) % objects.Length;
		return value;
	}

	public T getPrevious() {
		T value = objects [index--];
		if (index == -1)
			index += objects.Length;
		return value;
	}

	/// <summary>
	/// Adds the specified objects to the end of the array.
	/// </summary>
	/// <param name="_objects">The objects to be added at the end of the array.</param>
	public void addToArray(T[] _objects) {
		T[] newObjects = new T[objects.Length + _objects.Length];
		int i = 0;
		for (i = 0; i < objects.Length; i++) {
			newObjects [i] = objects [i];
		}
		for (i = 0; i < _objects.Length; i++) {
			newObjects [i + objects.Length] = _objects [i];
		}
		objects = newObjects;
	}

	/// <summary>
	/// Reset the index.
	/// </summary>
	public void resetIndex() {
		index = 0;
	}

	/// <summary>
	/// Get all the objects currently in this array.
	/// </summary>
	/// <returns>All the objects.</returns>
	public T[] getAllObjects() {
		return objects;
	}
}

/// <summary>
/// A special kind of Object pool that uses a hash to get to the underlying IndexedArrays.
/// </summary>
public class ObjectHashPool<T> {

	private Dictionary<int, IndexedArray<T>> dictionary;
	private int defaultEntry = int.MinValue;

	/// <summary>
	/// Initializes a new instance of the <see cref="ObjectHashPool"/> class.
	/// </summary>
	/// <param name="_capacity">Initial dictionary capacity.</param>
	public ObjectHashPool(int _capacity) {
		dictionary = new Dictionary<int, IndexedArray<T>> (_capacity);
	}

	/// <summary>
	/// Gets the next object on the IndexedArray of the specified hash.
	/// </summary>
	/// <param name="_hash">The hash.</param>
	public T get(int _hash) {
		IndexedArray<T> array;
		if (dictionary.TryGetValue (_hash, out array)) {
			return array.getNext();
		} else {
			if (defaultEntry != int.MinValue) {
				if (dictionary.TryGetValue (_hash, out array)) {
					return array.getNext();
				}
			}
			return default(T);
		}
	}

	/// <summary>
	/// Adds the specified array to the end of the IndexedArray of the specified Hash.
	/// </summary>
	/// <param name="_hash">The hash.</param>
	/// <param name="_array">The array.</param>
	public void put(T[] _array, int _hash) {
		IndexedArray<T> array;
		if (dictionary.TryGetValue (_hash, out array)) {
			array.addToArray (_array);
		} else {
			dictionary.Add (_hash, new IndexedArray<T> (_array));
		}
		if (defaultEntry == int.MinValue)
			defaultEntry = _hash;
	}
}

/// <summary>
/// Lock mechanism with counter and busy-waiting.
/// </summary>
public class TagLock {
	private int lockValue;
	private int lockCount;
	public TagLock() {
		lockValue = -1;
		lockCount = 0;
	}
	/// <summary>
	/// Attempts to lock with the given tag.
	/// </summary>
	/// <returns><c>true</c>, if unlocked or locked with same tag, <c>false</c> otherwise.</returns>
	/// <param name="_tag">The tag.</param>
	public bool tryLock(int _tag) {
		if (lockValue == _tag || lockValue == -1) {
			lockValue = _tag;
			lockCount++;
			return true;
		} else {
			return false;
		}
	}
	/// <summary>
	/// Decrementes the counter (number of locks). If the counter reached 0, the lock is unlocked.
	/// Assumes that this method was called from a thread with the lock.
	/// </summary>
	public void unlock() {
		lockCount--;
		if(lockCount <= 0) {
			lockCount = 0;
			lockValue = -1;
		}
	}
}
	
public static class Coroutines {

	/// <summary>
	/// Animates the localScale of the given GameObject.
	/// </summary>
	/// <returns>The IEnumerator for Coroutine.</returns>
	/// <param name="_gameObject">The game object being animated.</param>
	/// <param name="_animationType">The animation type.</param>
	/// <param name="_alpha">The animation alpha speed. Value should be between 0 and 1.</param>
	/// <param name="_tagLock">[Optional] Tag lock (only makes sense if setting _animationTag). </param>
	/// <param name="_animationTag">[Optional] The tag for this animation. If there's already an animation with another tag on the same lock, the coroutine will wait for that to finish. </param>
	/// <param name="_onAnimationDone">[Optional] Callback event for when the animation completed. </param>
	public static IEnumerator AnimateScale (GameObject _gameObject, AnimationType _animationType, float _alpha, 
		TagLock _tagLock = null, int _animationTag = -1, OnOperationDone _onAnimationDone = null) {

		if (_tagLock != null && _animationTag > -1) {
			while (_tagLock.tryLock (_animationTag) == false)
				yield return new WaitForFixedUpdate ();
		}

		if (_animationType == AnimationType.In) {
			_gameObject.SetActive (true);
			_gameObject.transform.localScale = new Vector3(0.001f, 0.001f, 1.0f);
		}
		Vector3 fScale = Vector3.one * 1.05f;
		while (Vector3.Distance(_gameObject.transform.localScale, fScale) > 0.015f) {
			_gameObject.transform.localScale = Vector3.Lerp (_gameObject.transform.localScale, fScale, _alpha);
			yield return new WaitForFixedUpdate ();
		}
		_gameObject.transform.localScale = fScale;
		switch (_animationType) {
		case AnimationType.In:
			fScale = Vector3.one;
			break;
		case AnimationType.Out:
			fScale = new Vector3(0.001f, 0.001f, 1.0f);
			break;
		case AnimationType.Pop:
			fScale = Vector3.one;
			break;
		}
		while (Vector3.Distance(_gameObject.transform.localScale, fScale) > 0.015f) {
			_gameObject.transform.localScale = Vector3.Lerp (_gameObject.transform.localScale, fScale, _alpha);
			yield return new WaitForFixedUpdate ();
		}
		_gameObject.transform.localScale = fScale;
		if (_animationType == AnimationType.Out)
			_gameObject.SetActive (false);

		if (_tagLock != null && _animationTag > -1)
			_tagLock.unlock ();

		if (_onAnimationDone != null)
			_onAnimationDone ();
	}

	// Animates the anchoredPosition of the given rectTransform
	public static IEnumerator AnimateUIMovement (RectTransform _rectTransform, AnimationMoveType _animationType, 
		float _alpha, TagLock _tagLock = null, int _animationTag = -1, OnOperationDone _onAnimationDone = null) {

		if (_tagLock != null && _animationTag > -1) {
			while (_tagLock.tryLock (_animationTag) == false)
				yield return new WaitForFixedUpdate ();
		}

		_rectTransform.gameObject.SetActive (true);

		Vector2 targetPosition = _rectTransform.anchoredPosition;
		Vector2 originalPosition = _rectTransform.anchoredPosition;

		switch (_animationType) {
		case AnimationMoveType.FromRight:
			targetPosition = _rectTransform.anchoredPosition;
			_rectTransform.anchoredPosition = _rectTransform.anchoredPosition + new Vector2 (+Screen.width, 0) + 
				new Vector2(_rectTransform.sizeDelta.x, 0);
			break;
		case AnimationMoveType.FromLeft:
			targetPosition = _rectTransform.anchoredPosition;
			_rectTransform.anchoredPosition = _rectTransform.anchoredPosition + new Vector2 (-Screen.width, 0) + 
				new Vector2(-_rectTransform.sizeDelta.x, 0);
			break;
		case AnimationMoveType.FromBelow:
			targetPosition = _rectTransform.anchoredPosition;
			_rectTransform.anchoredPosition = _rectTransform.anchoredPosition + new Vector2 (0, -Screen.height) + 
				new Vector2(0, -_rectTransform.sizeDelta.y);
			break;
		case AnimationMoveType.FromAbove:
			targetPosition = _rectTransform.anchoredPosition;
			_rectTransform.anchoredPosition = _rectTransform.anchoredPosition + new Vector2 (0, +Screen.height) + 
				new Vector2(0, _rectTransform.sizeDelta.y);
			break;
		case AnimationMoveType.ToRight:
			targetPosition = _rectTransform.anchoredPosition + new Vector2 (+Screen.width, 0) + 
				new Vector2(_rectTransform.sizeDelta.x, 0);
			break;
		case AnimationMoveType.ToLeft:
			targetPosition = _rectTransform.anchoredPosition + new Vector2 (-Screen.width, 0) + 
				new Vector2(-_rectTransform.sizeDelta.x, 0);
			break;
		case AnimationMoveType.ToBelow:
			targetPosition = _rectTransform.anchoredPosition + new Vector2 (0, -Screen.height) + 
				new Vector2(0, -_rectTransform.sizeDelta.y);
			break;
		case AnimationMoveType.ToAbove:
			targetPosition = _rectTransform.anchoredPosition + new Vector2 (0, +Screen.height) + 
				new Vector2(0, _rectTransform.sizeDelta.y);
			break;
		}

		while (Vector2.Distance(_rectTransform.anchoredPosition, targetPosition) > 0.015f) {
			_rectTransform.anchoredPosition = Vector2.Lerp (_rectTransform.anchoredPosition, targetPosition, _alpha);
			yield return new WaitForFixedUpdate ();
		}
		_rectTransform.anchoredPosition = targetPosition;

		if (_animationType == AnimationMoveType.ToLeft ||
			_animationType == AnimationMoveType.ToRight ||
			_animationType == AnimationMoveType.ToAbove ||
			_animationType == AnimationMoveType.ToBelow) {
			_rectTransform.gameObject.SetActive (false);
			_rectTransform.anchoredPosition = originalPosition;
		}

		if (_tagLock != null && _animationTag > -1)
			_tagLock.unlock ();

		if (_onAnimationDone != null)
			_onAnimationDone ();
	}

	// Make _gameObject follow _target forever
	public static IEnumerator FollowForever(Transform _target, GameObject _gameObject, Vector3 _offset, float _alpha) {
		while (true) {
			_gameObject.transform.position = Vector3.Lerp (_gameObject.transform.position,
				_target.position + _offset, _alpha);

			yield return new WaitForFixedUpdate ();
		}
	}

	// Make _gameObject follow _target forever, calling updateResident every time
	/*public static IEnumerator FollowForeverUpdateResident(WorldUIController worldUIController, 
		Transform _target, GameObject _gameObject, Vector3 _offset, float _alpha) {
		while (true) {
			_gameObject.transform.position = Vector3.Lerp (_gameObject.transform.position,
				_target.position + _offset, _alpha);
			worldUIController.updateResidentPanel ();

			yield return new WaitForFixedUpdate ();
		}
	}*/

	// Make _gameObject follow _targetPosition forever
	public static IEnumerator FollowForever(Vector3 _targetPosition, GameObject _gameObject, float _alpha) {
		while (true) {
			_gameObject.transform.position = Vector3.Lerp (_gameObject.transform.position, _targetPosition, _alpha);

			yield return new WaitForFixedUpdate ();
		}
	}

	// Make _gameObject follow _targetPosition forever, calling updateObstruction panel every time
	/*public static IEnumerator FollowForeverUpdateObstruction(WorldUIController worldUIController, 
		Vector3 _targetPosition, GameObject _gameObject, float _alpha) {
		while (true) {
			_gameObject.transform.position = Vector3.Lerp (_gameObject.transform.position, _targetPosition, _alpha);
			worldUIController.updateObstructionPanel ();

			yield return new WaitForFixedUpdate ();
		}
	}*/

	// Instantly go to black, and fade to clear (scene). When done, callback will be called.
	public static IEnumerator FadeIn (Image _faderImage, float _initialDelay, float _alphaTime, 
		float _finalAlpha = 0.0f, TagLock _tagLock = null, int _animationTag = -1, OnOperationDone _onFaded = null) {

		if (_tagLock != null && _animationTag > -1) {
			while (_tagLock.tryLock (_animationTag) == false)
				yield return new WaitForFixedUpdate ();
		}

		_faderImage.gameObject.SetActive (true);
		Color color = _faderImage.color;
		yield return new WaitForSeconds (_initialDelay);
		while (color.a > _finalAlpha+0.2f) {
			color.a = Mathf.Lerp (color.a, _finalAlpha, _alphaTime);
			_faderImage.color = color;
			yield return new WaitForFixedUpdate ();
		}
		color.a = _finalAlpha;
		_faderImage.color = color;
		if (_finalAlpha < 0.001f)
			_faderImage.gameObject.SetActive (false);

		if (_tagLock != null && _animationTag > -1)
			_tagLock.unlock ();

		if (_onFaded != null)
			_onFaded ();
	}

	// Instantly go to clear, and fade to black. When done, callback will be called.
	public static IEnumerator FadeOut (Image _faderImage, float _alphaTime, float _afterDelay, 
		float _finalAlpha=1.0f, TagLock _tagLock = null, int _animationTag = -1, OnOperationDone _onFaded = null) {

		if (_tagLock != null && _animationTag > -1) {
			while (_tagLock.tryLock (_animationTag) == false)
				yield return new WaitForFixedUpdate ();
		}

		_faderImage.gameObject.SetActive (true);
		Color color = _faderImage.color;
		while (color.a < _finalAlpha-0.2f) {
			color.a = Mathf.Lerp (color.a, _finalAlpha, _alphaTime);
			_faderImage.color = color;
			yield return new WaitForFixedUpdate ();
		}
		color.a = _finalAlpha;
		_faderImage.color = color;
		yield return new WaitForSeconds (_afterDelay);

		if (_tagLock != null && _animationTag > -1)
			_tagLock.unlock ();

		if (_onFaded != null)
			_onFaded ();
	}
}