﻿using UnityEngine;
using System.Collections;


public interface IOrdable {
	bool GiveOrder();
}

public interface IAttackable {
	Transform getTransform();
	void Hit(float damage);
}

public interface IUsable {
	void Use();
}