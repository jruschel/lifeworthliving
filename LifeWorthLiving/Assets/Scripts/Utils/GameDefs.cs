﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Resource types.
/// </summary>
public enum ResourceType {
	// for instance:
	Construction,
	Food
}

/// <summary>
/// Struct for counting all resources.
/// </summary>
public struct ResourcesCount {
	public int construction;
	public int food;

	public ResourcesCount(int construction, int food) {
		this.construction = construction;
		this.food = food;
	}
}

/// <summary>
/// Representation of current HP.
/// </summary>
[System.Serializable]
public class HealthPoints {
	[SerializeField] public float current;
	[SerializeField] public float max;
    [SerializeField] public float hunger;

	public HealthPoints(int current, int max) {
		this.current = current;
		this.max = max;
	}

    public void calculateHungerIteration(float bonus) {
        hunger = Mathf.Min (1, hunger + ((1+bonus) * (0.0001f)));

        if (hunger > 0.5f) {
            current -= 0.001f;
        }
        if (hunger > 0.75f) {
            current -= 0.001f;
        }
        if (hunger > 0.9f) {
            current -= 0.01f;
        }
    }

    public void giveHP(float value) {
        current = Mathf.Min (max, current + value);
    }

    public void giveFood (float value) {
        hunger = Mathf.Max (0, hunger - value);
    }
}

public enum WeaponType {
	None,
	Knife,
	Sword,
	Bat,
	Pistol,
	Shotgun,
	SMG,
	AssaultRifle
}

[System.Flags]
public enum WeaponBuff {
	Knockback1  = 0x1,
	Knockback2 	= 0x2,
	Knockback3 	= 0x4
	//Bleed1     	= (1 << 3), 
	//Bleed2     	= (1 << 4), 
	//Bleed3     	= (1 << 5),
	//InfiniteAmmo= (1 << 6)
}