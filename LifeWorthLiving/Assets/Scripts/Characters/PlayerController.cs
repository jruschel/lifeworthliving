﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : DefaultCharacterController {

	#region Variables

	
	public AdventureCameraController acController;
    
	
	//private float current_speed = 0, current_anim_speed = 0;
	private Vector3 v_Direction;

	// State
	[Header("State")]
	private List<DefaultItemController> itemsTouched;
    private MovementMode movementMode;

    // Weapons
    [Header ("Weapons")]
    public Transform shotSourceTransform;
    public float weaponSwitchTime = 100;
    public float hideWeaponTime = 200;
    private bool isWeaponUp = false;
    private int currentWeapon;   // 0 or 1
    private int weaponCount = 0;
    private Weapon[] weapons;
    private float hideWeaponTimer = 0;

    private float currentBusy = 0;
    public bool isBusy {
        get { return currentBusy >= 0.01f; }
    }

    private int anim_weaponInt = 0;
    private float anim_bodyHorizontal = 0, anim_headHorizontal = 0;
    private float anim_bodyHorizontalTarget = 0, anim_headHorizontalTarget = 0;
    public GameObject wObjectPistol, wObjectShotgun;
    public ParticleSystem wParticlesPistol, wParticlesShotgun;

    #endregion

    #region Control Methods

    // Use this for initialization
    new void Start () {
		m_Animator = GetComponentInChildren<Animator> ();
		itemsTouched = new List<DefaultItemController> ();

        weapons = new Weapon[2];
    }

    private bool unselectWeapon() {
        if (isWeaponUp && !isBusy) {
            isWeaponUp = false;

            anim_weaponInt = 0;
            m_Animator.SetInteger ("WeaponType_int", anim_weaponInt);
            anim_bodyHorizontalTarget = 0;
            anim_headHorizontalTarget = 0;
            wObjectPistol.SetActive (false);
            wObjectShotgun.SetActive (false);

            WeaponHUDController.Flip (WeaponType.None, 0);
            currentBusy = weaponSwitchTime;

            return true;
        } else {
            return false;
        }
    }

    private void switchWeapon() {
        if (!isBusy && weaponCount > 0) {

            if (weaponCount == 1 && !isWeaponUp) {
                currentWeapon = 0;
            } else
            if (weaponCount == 2) {
                if (isWeaponUp)
                    currentWeapon = (currentWeapon + 1) % 2;
            } else
                return;

            isWeaponUp = true;

            switch (weapons[currentWeapon].wType) {
                case WeaponType.None:
                    anim_weaponInt = 0;
                    m_Animator.SetInteger ("WeaponType_int", anim_weaponInt);
                    anim_bodyHorizontalTarget = 0;
                    anim_headHorizontalTarget = 0;
                    wObjectPistol.SetActive (false);
                    wObjectShotgun.SetActive (false);
                    break;
                case WeaponType.Pistol:
                    anim_weaponInt = 1;
                    m_Animator.SetInteger ("WeaponType_int", anim_weaponInt);
                    anim_bodyHorizontalTarget = 0;
                    anim_headHorizontalTarget = 0;
                    wObjectPistol.SetActive (true);
                    wObjectShotgun.SetActive (false);
                    break;
                case WeaponType.Shotgun:
                    anim_weaponInt = 4;
                    m_Animator.SetInteger ("WeaponType_int", anim_weaponInt);
                    anim_bodyHorizontalTarget = 0.6f;
                    anim_headHorizontalTarget = -0.6f;
                    wObjectPistol.SetActive (false);
                    wObjectShotgun.SetActive (true);
                    break;
            }
            WeaponHUDController.Flip (weapons[currentWeapon].wType, weapons[currentWeapon].ammo);
            currentBusy = weaponSwitchTime;
            AudioManager.Play_Clip (transform.position, weapons[currentWeapon].pickupAudio, 0.3f);
        }
    }

    // Update is called once per frame
    new void Update () {
        base.Update ();

        /* 
		TODO most of this code should be on DefaultCharacterController instead.
			the stuff now on DefaultCharacterController is pretty much useless -> DONE

		make this player controller be subclass of DefaultCharacterController, and pass
			movement information from user (inputX and inputY) to it, and it does the calculation. -> DONE

		also, fix rigidbody movement using better method (instead of MovePosition). ->DONE

		also, all player-related input (movement, attack, etc) will be handled HERE (not input manager)
			input manager can be deleted?

		pause and other inputs will be managed on game manager
		*/

        currentBusy -= Time.deltaTime * 100;

        anim_bodyHorizontal = Mathf.Lerp (anim_bodyHorizontal, anim_bodyHorizontalTarget, 0.2f);
        anim_headHorizontal = Mathf.Lerp (anim_headHorizontal, anim_headHorizontalTarget, 0.2f);
        m_Animator.SetFloat ("Body_Horizontal_f", anim_bodyHorizontal);
        m_Animator.SetFloat ("Head_Horizontal_f", anim_headHorizontal);

        if (GameSettings.IsInput_WeaponSwitch) {
            switchWeapon ();
            hideWeaponTimer = 0;
        }

        if (GameSettings.IsInput_WeaponSwitching) {
            hideWeaponTimer += Time.deltaTime * 100;
            if (hideWeaponTimer >= hideWeaponTime) {
                unselectWeapon ();
                hideWeaponTimer = 0;
            }
        }

        if (GameSettings.IsInput_PickUp) { 
            if (itemsTouched.Count > 0) {
                DefaultItemController pickedUpItem = itemsTouched[0];
                itemsTouched.RemoveAt (0);
                if (itemsTouched.Count > 0) itemsTouched[0].selectStart ();
                pickedUpItem.selectEnd ();
                pickedUpItem.pickUp (this);
            }
        }

        if (GameSettings.IsInput_Jump && !isBusy) {
            m_Animator.SetBool ("Jump_b", true);
        } else {
            m_Animator.SetBool ("Jump_b", false);
        }

        if (GameSettings.IsInput_Shooting && isWeaponUp && currentWeapon < weaponCount) {
            if (weapons[currentWeapon].ammo > 0 && !isBusy) {

                m_Animator.SetBool ("Shoot_b", true);

                currentBusy = weapons[currentWeapon].fireSingle (shotSourceTransform.position, transform.forward);

                if (weapons[currentWeapon].wType == WeaponType.Pistol) {
                    wParticlesPistol.Emit (5);
                }
                if (weapons[currentWeapon].wType == WeaponType.Shotgun) {
                    wParticlesShotgun.Emit (15);
                }

            } else {
                m_Animator.SetBool ("Shoot_b", false);
            }
        } else {
            m_Animator.SetBool ("Shoot_b", false);
        }

        PlayerHUDController.SetHP ((float)healthPoints.current / (float)healthPoints.max);
        PlayerHUDController.SetHunger (healthPoints.hunger);
    }

	void FixedUpdate() {
        //Movement
        // attempt movement based on current input
        float inputX = Input.GetAxisRaw("Horizontal");
        float inputY = Input.GetAxisRaw("Vertical");
        //Force Walk
        if (Input.GetKey(GameSettings.Key_Walk)) {
            movementMode = MovementMode.Walking;
            animateFootstepsAudioInterval = 4.5f;
        }
        else {
            movementMode = MovementMode.Running;
            animateFootstepsAudioInterval = 3.5f;
        }
        // calculate direction to go
        v_Direction = Vector3.ClampMagnitude( (acController.v_Forward * inputY) + (acController.v_Right * inputX), 2.0f);
        //Move
        moveTowards(v_Direction, movementMode);
        
        if (v_Direction.sqrMagnitude > 0.1f) {
            animateFootstepsAudio = true;
        } else {
            animateFootstepsAudio = false;
        }

        //Items
        if (itemsTouched.Count > 1) {
			itemsTouched.Sort (delegate(DefaultItemController a, DefaultItemController b) {
				return Vector3.Distance(a.transform.position, transform.position).CompareTo(
					Vector3.Distance(b.transform.position, transform.position));
			});
			itemsTouched [0].selectStart ();
			for (int i = 1; i < itemsTouched.Count; i++)
				itemsTouched [i].selectEnd ();
		}

        healthPoints.calculateHungerIteration ((inputX + inputY) * 0.5f);
    }

	void OnTriggerEnter(Collider other) {
		DefaultItemController item = other.gameObject.GetComponent<DefaultItemController> ();
		if (item != null) {
			//int id = 0;
			//float distance = Vector3.Distance (other.transform.position, transform.position);
			//while (id < itemsTouched.Count && Vector3.Distance(itemsTouched[id].transform.position, transform.position) < distance) {
			//	id++;
			//}
			//if (id == 0) {
			//	item.selectStart ();
			//	if (itemsTouched.Count > 0)
			//		itemsTouched [0].selectEnd ();
			//}

			itemsTouched.Add (item);

			if (itemsTouched.Count == 1)
				item.selectStart ();
		}
	}

	void OnTriggerExit(Collider other) {
		DefaultItemController item = other.gameObject.GetComponent<DefaultItemController> ();
		if (item != null) {
			int id = itemsTouched.IndexOf (item);
			if (id == 0) {
				item.selectEnd ();
				if (itemsTouched.Count > 1)
					itemsTouched [1].selectStart ();
			}
			itemsTouched.RemoveAt (id);
		}
	}

    #endregion

    #region Items

    public bool giveWeapon(Weapon newWeapon) {
        // just ammo - give to current weapon
        if (newWeapon.wType == WeaponType.None) {
            if (!isWeaponUp) return false;

            weapons[currentWeapon].giveAmmo (newWeapon.ammo);
            WeaponHUDController.UpdateAmmo (weapons[currentWeapon].ammo);

            return true;
        }

        if (weaponCount < 2) {
            weapons[weaponCount++] = newWeapon;
            switchWeapon ();
        } else {
            //Weapon oldWeapon = weapons[currentWeapon];

            weapons[currentWeapon] = newWeapon;
            switchWeapon ();
        }

        return true;
    }

    #endregion

    #region Combat Methods

    // TODO

    #endregion

    #region Enums
    #endregion
}
