﻿using UnityEngine;
using System.Collections;
using System.Linq;

/// <summary>
/// Default character controller.
/// Manages character movement (position, rotation), path-finding and basic actions.
/// Also manages an Animator Controller, that must have the following parameters:
/// 	float AbsSpeed: Absolute Speed (0..1)
/// </summary>
public class DefaultCharacterController : MonoBehaviour {

	#region Public Variables

	// Character
	[Header("Character")]
	public HealthPoints healthPoints;
	public LayerMask losBlockMask;      // cannot have characters!

    // TODO
    // Movement
    [Header("Movement")]
    public AudioClip[] footsteps;
    public float movementThreshold = 0.1f;
    public float smoothiness;
    public float runSpeed;
    [Range(0, 1)] public float walkAlpha = 0.5f;
    [Range(0, 1)] public float acceleration;
    private float current_speed = 0, current_anim_speed = 0;
    #endregion

    #region Private/Protected Variables

    protected State state;			// NEVER change this directly (plox :))

    protected Transform m_Transform;
	protected Animator m_Animator;
    protected Rigidbody m_Rigidbody;
    private RaycastHit[] rc_hits;

    protected bool animateFootstepsAudio = false;
    protected float animateFootstepsAudioInterval = 6;
    protected float footstepsTimer = 0;

	#endregion

	#region Control Methods

	virtual protected void Awake () {
		state = State.Idle;

		rc_hits = new RaycastHit[4];
        m_Rigidbody = GetComponent<Rigidbody>();
        m_Transform = transform;
		m_Animator = GetComponentInChildren<Animator> ();	// TODO john will configure animator stuff
	}

	// Use this for initialization
	virtual protected void Start () {
		
	}
	
	// Update is called once per frame
	// TODO this will be a virtuall call every frame 
	//   this is a VERY bad idea
	//   possible fix: use CustomUpdate
	virtual protected void Update () {
        if (animateFootstepsAudio) {

            footstepsTimer += Time.deltaTime * 10;
            if (footstepsTimer > animateFootstepsAudioInterval) {
                footstepsTimer = 0;
                AudioManager.Play_RandomClip (transform.position, footsteps, 0.2f);
            }
        }
    }

	private void setState(State newState) {
		state = newState;
	}

    #endregion

    #region API

    // Instantly move the character towards the direction (1 fixed update)
    /// <summary>
    /// Direction is a Vector3 ClampedMagnetude that says where to go, and movementMode is saying that, if he's walking or running
    /// </summary>
    /// <param name="direction"></param>
    /// <param name="movementMode"></param>
    protected void moveTowards(Vector3 direction, MovementMode movementMode) {
        float abs_inputX = Mathf.Abs(direction.x);
        float abs_inputY = Mathf.Abs(direction.z);
        // TODO
        //speed smoothing
        //The use of lerp in the AddForce causes some strange behavior caused when the player goes diagonal ( my english is bad)
        //To fix this shit, i used this if, don't found a better way right now ._.
        //Fixed like a sir ;3
        if (abs_inputY > movementThreshold || abs_inputX > movementThreshold) {
            current_speed = Mathf.Lerp(current_speed,
            ((movementMode == MovementMode.Walking) ? runSpeed * walkAlpha : runSpeed),
            acceleration) * ((direction.magnitude > 1) ? (int)direction.magnitude : direction.magnitude);
            //m_Rigidbody.MovePosition(transform.position + v_Direction.normalized * current_speed * Time.deltaTime);
            Quaternion currentLookRotation;
            currentLookRotation = Quaternion.LookRotation(direction, Vector3.up);
            currentLookRotation.x = 0;
            currentLookRotation.z = 0;
            m_Rigidbody.MoveRotation(Quaternion.Slerp(transform.localRotation,
                                                        currentLookRotation, acceleration * Time.deltaTime * smoothiness)); //0.7 0.6 0. 
            // animator speed
            current_anim_speed = Mathf.Clamp01(abs_inputX + abs_inputY);
            m_Animator.speed = current_anim_speed;
            m_Animator.SetFloat("Speed_f", current_speed / runSpeed);
            
        }
        else {
            // is not moving
            current_speed = Mathf.Lerp (current_speed, 0, acceleration * 0.7f);
            current_anim_speed = Mathf.Lerp (current_anim_speed, 1, acceleration);
			m_Animator.speed = 1;
			m_Animator.SetFloat ("Speed_f", 0);
		}
        m_Rigidbody.AddForce(transform.forward * current_speed, ForceMode.Impulse);
        
        
	}

	// Attempt to move in a straight line to position X,Z coordinates (y is not used).
	//	Will only stop if other command is given or reached destination
	protected void moveTo(Vector3 position) {
		setState (State.Moving);
        //transform.position = Vector3.MoveTowards(transform.position, -position, 0.2f);
        //m_Rigidbody.MovePosition(transform.position + position.normalized * 0.2f);
        if (Vector3.Magnitude(transform.position - position) < 0.7f)
            m_Animator.SetFloat ("Speed_f", 0);
        else
            m_Animator.SetFloat ("Speed_f", 1);
        transform.position = Vector3.Slerp(transform.position, position, Time.deltaTime * walkAlpha);
        
        // TODO
    }

	// Continuosly follows the target in a straight line
	protected void follow(Transform target, float maxSqrDistance) {
		setState (State.Following);

		// TODO
	}

	// Move back and forth from current position to this position, attacking if sees anything
	protected void patrol(Vector3 position) {
		// TODO
	}

	// Attempt to move in a straight line to position X,Z coordinates (y is not used).
	//	Will stop if other command is given, reached destination or an attackable is in reach
	protected void attack(Vector3 position) {
		// TODO
	}

	// Simple rotation towards position
	protected void rotateTo(Vector3 position) {
		setState (State.Idle);

		// TODO
	}

	// Keep looking towards the target, standing still.
	protected void rotateTo(Transform target) {
		setState (State.Idle);

		// TODO
	}

    // Wander around from a random point around its position, will be used for zombies and other non-usable characters mainly.
    protected void wander() {
		// TODO
        
		/*if (this.state == State.Idle && wandering) {
            moveTo(new Vector2i(((int)this.transform.position.x -wanderingValue +wanderingValue), (int)this.transform.position.y - wanderingValue + wanderingValue));
        }*/

    }

	// Returns true if the given target is in the line of sight
    protected bool isInLOS(DefaultCharacterController target, float radius) {
		
		Vector3 fromPosition = transform.position;
		Vector3 toPosition = target.transform.position;
		float distance = Vector3.Distance (fromPosition, toPosition);

        // Verify if object is within attack radius
		if (distance < radius)
        {
            // Calculate if there isn't something in the way
            Vector3 direction = toPosition - fromPosition;
            Ray r = new Ray(transform.position, direction);

			int hitCount = Physics.RaycastNonAlloc(r, rc_hits, distance, losBlockMask);

			// if doesn't hit anything in its path, then it is in line of sight
			if (hitCount == 0) 
			{
				return true;
			}
        }

        return false;
    }

    protected void cancelAllMovement(bool cancelAttack=true) {
		// TODO
	}

    #endregion

    #region Enums

    /// <summary>
    /// Character current state.
    /// </summary>
    public enum State {
		Idle,
		Wandering,
		Moving,
		Following,
		Attacking,
		Building,
		Dying,
		Dead
	}
	public enum MovementMode {
		Walking,
		Running
	}

	#endregion
}