﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;

public class DefaultZombieController : DefaultCharacterController {

    #region Public Variables

    public float wanderRadius;
    public AudioClip audioSees;
    public AudioClip audioHit;
    public AudioClip audioDie;

    #endregion
    #region Private Variables

    private bool dead = false;
    private Transform enemy;
    private Vector3 direction;
    private int nextUpdate = 1;
    private bool canSee;
    private float atkDist = 1.3f; // After a looooot of measure, don't mess with this value, PLOX
    private bool initialTurn;                          // TODO

    #endregion

    #region Control Methods

    // Use this for initialization
    new void Start () {
		base.Start ();
        enemy = GameObject.Find("Player").transform;
        Wander();
        initialTurn = true;
    }

	// Update is called once per frame
	new void Update () {

	}

    void FixedUpdate() {
        //Change to moveTowards... i have to?
        if (!dead)
            moveTo(Direction());
        //If some soundTrigger gets the zombie
        //moveTo(soundPosition);
    }

    #endregion

    #region Combat Methods

    // TODO

    public void hit(float damage) {
        if (!dead) {
            AudioManager.Play_Clip (transform.position, audioHit, 0.2f);
            healthPoints.current -= damage;
            if (healthPoints.current <= 0) {
                dead = true;
                m_Animator.SetTrigger ("Die");
                AudioManager.Play_Clip (transform.position, audioDie, 0.2f);
            }
        }
    }

    #endregion
    #region AAI

    //Put a random point in the universe and keep walking, to a casual wandering, u know, like someone who wants nothing.
    private void Wander() {

        direction = UnityEngine.Random.insideUnitSphere * wanderRadius;
        direction.y = 0; //Make sure the zombie does not go to heaven nor hell
        direction += transform.position;

    }
    /***LIST
     * Bunch of if's 'mmmkay
     * 1st to event trigger, if no trigger, just wander 
     * Wander, if no LOS 'mmmmkay
     * If LOS, chase 'mmmkay
     * If in contact, KILL
     * Check if got Visual or Proximity contact 'mmmkay
     */
    private Vector3 Direction() {
        //Avoid repeated code
        Quaternion targetRotation = Quaternion.LookRotation(direction - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * 2);
        if (canSee) {
            //I Can seeee u ;3
            direction = enemy.position;
            if (Vector3.Distance(transform.position, enemy.position) < atkDist) {
                //ATTACK!
            }
            if (Vector3.Angle(-transform.forward, transform.position - enemy.position) > 25 && initialTurn) {
                initialTurn = false;
                return transform.localPosition * 1.001f;// just to move a bit
            }
            //Move To the player
            initialTurn = true;
        }
        //I can't see u ;__;
        if (Time.time >= nextUpdate) {
            nextUpdate = Mathf.FloorToInt(Time.time) + 2;
            direction = UnityEngine.Random.insideUnitSphere * wanderRadius;
            direction.y = 0; //Make sure the zombie does not go to heaven nor hell
            direction += transform.position;
        }
        return direction;
    }

    #region COLLIDERCHECK
    void OnTriggerEnter(Collider col) {
        if (col.name.Equals("Player")) {
            Vector3 zombie = transform.position;
            zombie.y += 1.5f;
            RaycastHit hit;
            Debug.DrawRay(zombie, enemy.position - transform.position);
            Physics.Raycast(zombie, enemy.position - transform.position, out hit);
            if (hit.rigidbody != null)
                if (hit.rigidbody.name.Equals ("Player")) {
                    if (canSee == false && !dead)
                        AudioManager.Play_Clip (transform.position, audioSees, 0.3f);
                    canSee = true;
                }
        }
        
    }
    // Still thinking in a better way, this works just fine ;3
    void OnTriggerStay(Collider col) {
        if (col.name.Equals("Player")) {
            Vector3 zombie = transform.position;
            zombie.y += 1.5f;
            RaycastHit hit;
            Debug.DrawRay(zombie, enemy.position - transform.position);
            Physics.Raycast(zombie, enemy.position - transform.position, out hit);
            if (hit.rigidbody != null)
                if (hit.rigidbody.name.Equals ("Player")) {
                    canSee = true;
                }
            
        }
    }
    void OnTriggerExit(Collider col) {
        if (canSee) {
            canSee = false;
            nextUpdate = Mathf.FloorToInt(Time.time) + 3;
        }
    }
    #endregion
    #endregion
}