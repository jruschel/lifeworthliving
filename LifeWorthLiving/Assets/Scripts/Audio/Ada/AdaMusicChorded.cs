﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AdaMusicLayer {
    public string name;
    [Range (0, 2)]
    public float volume = 1.0f;
    public AudioClip[] chords;
}

public class AdaMusicChorded : MonoBehaviour {

    [Header ("Settings")]
    public float bpm;
    public int beatsPerChord;
    public int chordCount;
    public int layerCount;

    [Header("Layers")]

    public AdaMusicLayer[] layers;

    [Header ("Playback")]
    public bool[] activeLayers;

    private int currentChord = 0;
    private bool finishNextChord = false;
    private bool finished = false;
    private double nextEventTime;
    private AudioSource[] audioSources;

	// Use this for initialization
	void Start () {
        audioSources = new AudioSource[layerCount * (chordCount + 1)];
        for (int layer = 0; layer < layerCount; layer++) {
            for (int chord = 0; chord < (chordCount + 1); chord++) {
                GameObject gObject = new GameObject ("Layer " + layer + "; Chord " + chord);
                gObject.transform.parent = transform;
                audioSources[layer * (chordCount+1) + chord] = gObject.AddComponent<AudioSource> ();
                audioSources[layer * (chordCount+1) + chord].clip = layers[layer].chords[chord];
                audioSources[layer * (chordCount+1) + chord].volume = layers[layer].volume;
            }
        }
        nextEventTime = AudioSettings.dspTime + 2.0F;
        currentChord = 0;
    }

    public void finish() {
        finishNextChord = true;
    }

    // Update is called once per frame
    void Update () {
        if (finished) return;

        double time = AudioSettings.dspTime;
        // before changing
        if (time + 1.0F > nextEventTime) {
            if (!finishNextChord) {
                currentChord = (currentChord + 1) % chordCount;
            } else {
                currentChord = chordCount;
                finished = true;
            }

            for (int layer = 0; layer < layerCount; layer++) {
                if (activeLayers[layer]) {
                    audioSources[layer * (chordCount + 1) + currentChord].PlayScheduled (nextEventTime);
                }
            }

            nextEventTime += 60.0F / bpm * beatsPerChord;
        }
        // right before changing
        if (time > nextEventTime) {
            for (int layer = 0; layer < layerCount; layer++) {
                if (activeLayers[layer]) {
                    audioSources[layer * (chordCount + 1) + currentChord].volume = layers[layer].volume;
                }
            }
        }
    }
}
