﻿using UnityEngine;
using System.Collections;

public class DefaultItemController : MonoBehaviour {

	[Header("Default")]
	public string itemName;
    public Color useButtonColor;
    public Color useButtonShadow;

	[Header("Animation")]
	public float rotationSpeed = 50;
	public float floatFrequency = 4;
	public float floatAmplitude = 0.1f;
	public Color glowBaseColor = new Color (1.0f * 0.5f, 0.9f * 0.5f, 0.05f * 0.5f);

    private bool pickupAble;

	protected Transform m_Transform;
	private Vector3 originalPosition;

	private bool isSelected = false;
	private Coroutine animationCoroutine = null;

    private GameObject buttonSpriteObject;

    private int n_renderers;
    private MeshRenderer[] m_Renderers;
    private Material[] defaultMaterials, glowMaterials;

	float anim_scaleFactor = 1;
	float glow_emission = 0;

	// Use this for initialization
	void Start () {
		m_Transform = transform;
		originalPosition = m_Transform.position;
        pickupAble = true;

        m_Renderers = GetComponentsInChildren<MeshRenderer> ();
        n_renderers = m_Renderers.Length;
        defaultMaterials = new Material[n_renderers];
        glowMaterials = new Material[n_renderers];
        for (int i = 0; i < n_renderers; i++) {
            defaultMaterials[i] = m_Renderers[i].sharedMaterial;
            glowMaterials[i] = new Material (defaultMaterials[i]);
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (pickupAble) {
            m_Transform.position = originalPosition + new Vector3 (
                0,
                Mathf.Sin (Time.time * floatFrequency) * floatAmplitude,
                0);
            m_Transform.RotateAround (m_Transform.position, Vector3.up, rotationSpeed * Time.deltaTime);
        }
	}

	public void selectStart() {
		if (isSelected == false && pickupAble) {
			isSelected = true;

            buttonSpriteObject = SimpleObjectsProvider.Request_ButtonSpriteX (useButtonColor, 1.1f, useButtonShadow);
            buttonSpriteObject.transform.parent = transform;
            buttonSpriteObject.transform.localPosition = new Vector3 (0, 1, 0);

            if (animationCoroutine != null)
				StopCoroutine (animationCoroutine);
			animationCoroutine = StartCoroutine (SelectionAnimation ());
		}
	}

	public void selectEnd() {
		if (isSelected) {
			isSelected = false;

            SimpleObjectsProvider.ReturnObject (buttonSpriteObject);

			if (animationCoroutine != null)
				StopCoroutine (animationCoroutine);
			animationCoroutine = StartCoroutine (SelectionEndAnimation ());
		}
	}

	IEnumerator SelectionAnimation() {
        for (int i = 0; i < n_renderers; i++)
		    m_Renderers[i].sharedMaterial = glowMaterials[i];

		while (true) {
			anim_scaleFactor = Mathf.Lerp (anim_scaleFactor, 1.25f, 0.2f);
			m_Transform.localScale = Vector3.one * anim_scaleFactor;

			glow_emission = Mathf.Lerp (glow_emission, Mathf.Min((Mathf.Sin (Time.time * 4) + 1.5f) * 0.5f, 1), 0.3f);

			Color glowColor = glowBaseColor * Mathf.LinearToGammaSpace (glow_emission);

            for (int i = 0; i < n_renderers; i++)
                glowMaterials[i].SetColor ("_EmissionColor", glowColor);

			yield return new WaitForFixedUpdate ();
		}
	}

	IEnumerator SelectionEndAnimation() {
		while (anim_scaleFactor > 1.005f && glow_emission > 0.005f) {
			anim_scaleFactor = Mathf.Lerp (anim_scaleFactor, 1, 0.2f);
			m_Transform.localScale = Vector3.one * anim_scaleFactor;

			glow_emission = Mathf.Lerp (glow_emission, 0, 0.2f);

			Color glowColor = glowBaseColor * Mathf.LinearToGammaSpace (glow_emission);

            for (int i = 0; i < n_renderers; i++)
                glowMaterials[i].SetColor ("_EmissionColor", glowColor);

            yield return new WaitForFixedUpdate ();
		}

		anim_scaleFactor = 1;
		glow_emission = 0;

		m_Transform.localScale = Vector3.one;

        for (int i = 0; i < n_renderers; i++)
            glowMaterials[i].SetColor ("_EmissionColor", glowBaseColor * 0);

        for (int i = 0; i < n_renderers; i++)
            m_Renderers[i].sharedMaterial = defaultMaterials[i];

        animationCoroutine = null;
    }

    IEnumerator PickUpAnimation(Transform target) {
        while (anim_scaleFactor > 0.001f) {
            anim_scaleFactor = Mathf.Lerp (anim_scaleFactor, 0, 0.12f);
            m_Transform.localScale = Vector3.one * anim_scaleFactor;
            m_Transform.transform.position = Vector3.MoveTowards (
                m_Transform.transform.position, 
                target.position + new Vector3(0, 0.8f, 0),
                0.2f);

            glow_emission = Mathf.Lerp (glow_emission, 0, 0.2f);

            Color glowColor = glowBaseColor * Mathf.LinearToGammaSpace (glow_emission);

            for (int i = 0; i < n_renderers; i++)
                glowMaterials[i].SetColor ("_EmissionColor", glowColor);

            yield return new WaitForFixedUpdate ();
        }

        anim_scaleFactor = 1;
        glow_emission = 0;

        m_Transform.localScale = Vector3.zero;

        for (int i = 0; i < n_renderers; i++)
            glowMaterials[i].SetColor ("_EmissionColor", glowBaseColor * 0);
        
        for (int i = 0; i < n_renderers; i++)
            m_Renderers[i].sharedMaterial = defaultMaterials[i];

        animationCoroutine = null;

        OnEndPickUpAnimation ();
    }

    private void OnEndPickUpAnimation() {
        // TODO Add to pool
        Destroy (gameObject);
    }

	// When object is pickedup
	public virtual void pickUp(PlayerController player) {
        if (pickupAble) {
            selectEnd ();
            pickupAble = false;
            animationCoroutine = StartCoroutine (PickUpAnimation (player.transform));
        }
	}

	public virtual float getBonus_Health() {
		return 0;
	}

	public virtual float getBonus_Food() {
		return 0;
	}

	public virtual Weapon getBonus_Weapon() {
		return null;
	}
}
