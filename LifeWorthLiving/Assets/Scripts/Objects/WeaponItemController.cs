﻿using UnityEngine;
using System.Collections;

public class WeaponItemController : DefaultItemController {

	[Header("Weapon")]
	public WeaponType weaponType;
    public AudioClip shootAudio;
    public AudioClip pickupAudio;
	public float fireRate;
	public float ammo = 0.5f;
    public float consumption = 0.02f;
    public float damageMultiplier = 1.0f;
	[Range(0,3)]public int buffKnockback = 0;
	[Range(0,3)]public int buffBleed = 0;
	public bool buffInfiniAmmo = false;

	public override void pickUp (PlayerController player) {
        AudioManager.Play_Clip (transform.position, pickupAudio, 0.4f);

		Weapon weapon = new Weapon (player);
		weapon.name = itemName;
		weapon.wType = weaponType;
        weapon.shootAudio = shootAudio;
        weapon.pickupAudio = pickupAudio;
        weapon.fireRate = fireRate;
		weapon.ammo = ammo;
        weapon.consumption = consumption;
        weapon.damageMultiplier = damageMultiplier;
		weapon.buffKnockback = buffKnockback;
		weapon.buffBleed = buffBleed;
		weapon.buffInfiniAmmo = buffInfiniAmmo;

        // TODO add player weapon (with info) to the players 'inventory'? swap? can only carry 2?
        if (player.giveWeapon (weapon)) {
            base.pickUp (player);
        }
	}
}
