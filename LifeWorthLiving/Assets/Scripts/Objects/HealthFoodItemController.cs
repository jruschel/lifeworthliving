﻿using UnityEngine;
using System.Collections;

public class HealthFoodItemController : DefaultItemController {

	[Header("Food")]
	public float foodValue = 0;

	[Header("Health")]
	public float hpValue = 0;

	public override void pickUp (PlayerController player) {
        player.healthPoints.giveHP (hpValue);
        player.healthPoints.giveFood (foodValue);

		base.pickUp (player);
	}

	public override float getBonus_Health() {
		return hpValue;
	}

	public override float getBonus_Food() {
		return foodValue;
	}
}
