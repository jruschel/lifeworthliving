﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;
using System;

public class TerrainBuilder {

	#region Private Variables

	private string name;
	private Material terrainMaterial;
	private ChunkMap gridMap;

    #endregion

    #if UNITY_EDITOR

    #region Control Methods

    public TerrainBuilder (string _name, Material _terrainMaterial, ChunkMap _gridMap) {
		name = _name;
		terrainMaterial = _terrainMaterial;
		gridMap = _gridMap;
	}

	#endregion

	#region Generation

	/*public void Generate(MapChunk[,] chunks, int chunksX, int chunksZ) {
        Mesh[,] chunkMeshes = new Mesh[chunksX, chunksZ];

        for (int i = 0; i < chunksX; i++)
        {
            for (int j = 0; j < chunksZ; j++)
            {
                chunkMeshes[i, j] = GenerateChunkMesh(
                chunks[i, j].startX, chunks[i, j].startZ, MapBuilder.CHUNK_SIZE, MapBuilder.CHUNK_SIZE);
            }
        }

        //chunkMeshes = fixNormalsOnChunkEdges(chunkMeshes, chunksX, chunksZ);

        for (int i = 0; i < chunksX; i++)
        {
            for (int j = 0; j < chunksZ; j++)
            {
                GameObject terrainMeshObject = new GameObject();
                terrainMeshObject.name = "Terrain Mesh";
                terrainMeshObject.transform.parent = chunks[i, j].gObject.transform;
                terrainMeshObject.transform.localPosition =
                	new Vector3(i * MapBuilder.CHUNK_SIZE, 0, j * MapBuilder.CHUNK_SIZE);
                terrainMeshObject.layer = LayerMask.NameToLayer("Terrain");

                MeshFilter terrainMeshFilter = terrainMeshObject.AddComponent<MeshFilter>();
                MeshRenderer terrainMeshRenderer = terrainMeshObject.AddComponent<MeshRenderer>();
				terrainMeshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.TwoSided;
                terrainMeshRenderer.sharedMaterial = terrainMaterial;

                terrainMeshFilter.sharedMesh = chunkMeshes[i, j];

                terrainMeshObject.AddComponent<MeshCollider> ();

				AssetDatabase.CreateAsset (terrainMeshFilter.sharedMesh, 
					"Assets/Resources/Terrains/" + name + "_" + i + "," + j + ".asset");
				AssetDatabase.SaveAssets ();
			}
		}
	}*/

	/// <summary>
	/// Generates the terrain chunk given as parameters.
	/// </summary>
	/// <returns>The generated mesh.</returns>
	/// <param name="startX">Start X position (inclusive).</param>
	/// <param name="startZ">Start Z position (inclusive).</param>
	/// <param name="sizeX">Chunk size in X axis (exclusive).</param>
	/// <param name="sizeZ">Chunk size in Z axis (exclusive).</param>
	public Mesh GenerateChunkMesh(int startX, int startZ, int sizeX, int sizeZ) {
		CombineInstance[] combineInstances = new CombineInstance[sizeX * sizeZ];
		Mesh chunkMesh = new Mesh();

		for (int i = 0; i < sizeX; i ++) {
			for (int j = 0; j < sizeZ; j ++) {
				// Generate only 1 mesh and combine
				Mesh mesh = new Mesh();
				Vector3[] verts = new Vector3[6];
				Vector2[] uvs = new Vector2[6];
				int[] tris;

				Color[] colors;
				Color c1 = gridMap.tileColors[(int)gridMap.tileMap[(startX+i) * gridMap.gridSizeX + (startZ+j)].ground];
				Color c2 = new Color (c1.r, c1.g, c1.b, c1.a);
				float obscurance = 1.0f - ((float)gridMap.tileMap [(startX + i) * gridMap.gridSizeX + (startZ + j)].obscurance *
				                   GridMap.OBSCURANCE_STEP);

				if (Mathf.Abs (
					    gridMap.vertexMap [((startX + i + 1) * (gridMap.gridSizeX + 1)) + startZ + j + 1].height -
					    gridMap.vertexMap [((startX + i) * (gridMap.gridSizeX + 1)) + startZ + j].height) <
				    Mathf.Abs (
					    gridMap.vertexMap [((startX + i) * (gridMap.gridSizeX + 1)) + startZ + j + 1].height -
					    gridMap.vertexMap [((startX + i + 1) * (gridMap.gridSizeX + 1)) + startZ + j].height)) {
					// 2 -- 5
					// |  /
					// | /
					// 4
					//      3
					//    / |
					//   /  |
					// 0 -- 1

					float height0 = gridMap.vertexMap [
						                ((startX + i) * (gridMap.gridSizeX + 1)) + (startZ + j)
					                ].height; // + Mathf.PerlinNoise((startX+i)*0.16f, (startZ+j)*0.2f)*0.9f;
					float height1 = gridMap.vertexMap [
						                ((startX + i + 1) * (gridMap.gridSizeX + 1)) + (startZ + j)
					                ].height; // + Mathf.PerlinNoise((startX+i+1)*0.16f, (startZ+j)*0.2f)*0.9f;
					float height2 = gridMap.vertexMap [
						                ((startX + i) * (gridMap.gridSizeX + 1)) + (startZ + j + 1)
					                ].height; // + Mathf.PerlinNoise((startX+i)*0.16f, (startZ+j+1)*0.2f)*0.9f;
					float height3 = gridMap.vertexMap [
						                ((startX + i + 1) * (gridMap.gridSizeX + 1)) + (startZ + j + 1)
					                ].height; // + Mathf.PerlinNoise((startX+i+1)*0.16f, (startZ+j+1)*0.2f)*0.9f;

					verts [0] = new Vector3((i), height0, (j));
					verts [1] = new Vector3((i+1), height1, (j));
					verts [2] = new Vector3((i), height2, (j+1));
					verts [3] = new Vector3((i+1), height3, (j+1));
					verts [4] = verts [0];
					verts [5] = verts [3];

					uvs [0] = new Vector2(0.0f, 1.0f);
					uvs [1] = new Vector2(1.0f, 1.0f);
					uvs [2] = new Vector2(0.0f, 0.0f);
					uvs [3] = new Vector2(1.0f, 0.0f);
					uvs [4] = uvs [0];
					uvs [5] = uvs [3];

					tris = new int[6] { 0, 3, 1, 5, 4, 2 };

					if (Mathf.Abs(height0 - height1) > GridMap.LEVEL_HEIGHT || 
						Mathf.Abs(height0 - height3) > GridMap.LEVEL_HEIGHT || 
						Mathf.Abs(height1 - height3) > GridMap.LEVEL_HEIGHT) {
						c1 = gridMap.tileColors [(int)GroundType.Dirt];
					}
					if (Mathf.Abs(height0 - height1) > 2*GridMap.LEVEL_HEIGHT || 
						Mathf.Abs(height0 - height3) > 2*GridMap.LEVEL_HEIGHT || 
						Mathf.Abs(height1 - height3) > 2*GridMap.LEVEL_HEIGHT) {
						c1 = gridMap.tileColors [(int)GroundType.Stone];
					}
					if (Mathf.Abs(height0 - height2) > GridMap.LEVEL_HEIGHT || 
						Mathf.Abs(height0 - height3) > GridMap.LEVEL_HEIGHT || 
						Mathf.Abs(height2 - height3) > GridMap.LEVEL_HEIGHT) {
						c2 = gridMap.tileColors [(int)GroundType.Dirt];
					}
					if (Mathf.Abs(height0 - height2) > 2*GridMap.LEVEL_HEIGHT || 
						Mathf.Abs(height0 - height3) > 2*GridMap.LEVEL_HEIGHT || 
						Mathf.Abs(height2 - height3) > 2*GridMap.LEVEL_HEIGHT) {
						c2 = gridMap.tileColors [(int)GroundType.Stone];
					}

					c1 = c1 * obscurance;
					c2 = c2 * obscurance;
					colors = new Color[6] { c1, c1, c2, c1, c2, c2 };
				}
				else
				{
					// 5 -- 3
					//  \   |
					//    \ |
					//      4
					// 2
					// | \
					// |  \
					// 0 -- 1

					float height0 = gridMap.vertexMap [
						                ((startX + i) * (gridMap.gridSizeX + 1)) + (startZ + j)
					                ].height; // + Mathf.PerlinNoise((startX+i)*0.16f, (startZ+j)*0.2f)*0.9f;
					float height1 = gridMap.vertexMap [
						                ((startX + i + 1) * (gridMap.gridSizeX + 1)) + (startZ + j)
					                ].height; // + Mathf.PerlinNoise((startX+i+1)*0.16f, (startZ+j)*0.2f)*0.9f;
					float height2 = gridMap.vertexMap [
						                ((startX + i) * (gridMap.gridSizeX + 1)) + (startZ + j + 1)
					                ].height; // + Mathf.PerlinNoise((startX+i)*0.16f, (startZ+j+1)*0.2f)*0.9f;
					float height3 = gridMap.vertexMap [
						                ((startX + i + 1) * (gridMap.gridSizeX + 1)) + (startZ + j + 1)
					                ].height; // + Mathf.PerlinNoise((startX+i+1)*0.16f, (startZ+j+1)*0.2f)*0.9f;

					verts [0] = new Vector3((i), height0, (j));
					verts [1] = new Vector3((i+1),height1, (j));
					verts [2] = new Vector3((i), height2, (j+1));
					verts [3] = new Vector3((i+1), height3, (j+1));
					verts [4] = verts [1];
					verts [5] = verts [2];

					uvs [0] = new Vector2(0.0f, 1.0f);
					uvs [1] = new Vector2(1.0f, 1.0f);
					uvs [2] = new Vector2(0.0f, 0.0f);
					uvs [3] = new Vector2(1.0f, 0.0f);
					uvs [4] = uvs [1];
					uvs [5] = uvs [2];

					tris = new int[6] { 0, 2, 1, 4, 5, 3 };

					if (Mathf.Abs(height0 - height1) > GridMap.LEVEL_HEIGHT || 
						Mathf.Abs(height0 - height2) > GridMap.LEVEL_HEIGHT || 
						Mathf.Abs(height1 - height2) > GridMap.LEVEL_HEIGHT) {
						c1 = gridMap.tileColors [(int)GroundType.Dirt];
					}
					if (Mathf.Abs(height0 - height1) > 2*GridMap.LEVEL_HEIGHT || 
						Mathf.Abs(height0 - height2) > 2*GridMap.LEVEL_HEIGHT || 
						Mathf.Abs(height1 - height2) > 2*GridMap.LEVEL_HEIGHT) {
						c1 = gridMap.tileColors [(int)GroundType.Stone];
					}
					if (Mathf.Abs(height1 - height2) > GridMap.LEVEL_HEIGHT || 
						Mathf.Abs(height1 - height3) > GridMap.LEVEL_HEIGHT || 
						Mathf.Abs(height2 - height3) > GridMap.LEVEL_HEIGHT) {
						c2 = gridMap.tileColors [(int)GroundType.Dirt];
					}
					if (Mathf.Abs(height1 - height2) > 2*GridMap.LEVEL_HEIGHT || 
						Mathf.Abs(height1 - height3) > 2*GridMap.LEVEL_HEIGHT || 
						Mathf.Abs(height2 - height3) > 2*GridMap.LEVEL_HEIGHT) {
						c2 = gridMap.tileColors [(int)GroundType.Stone];
					}

					c1 = c1 * obscurance;
					c2 = c2 * obscurance;
					colors = new Color[6] { c1, c1, c1, c2, c2, c2 };
				}

				// generate mesh
				mesh.vertices = verts;
				mesh.triangles = tris;
				mesh.uv = uvs;
				mesh.colors = colors;
				mesh.RecalculateNormals();

				// combine
				combineInstances [i * sizeX + j] = new CombineInstance();
				combineInstances [i * sizeX + j].mesh = mesh;
			}
		}

		chunkMesh.CombineMeshes (combineInstances, true, false);

		return chunkMesh;

        // init
		/*int[] tris = new int[sizeX * sizeZ * 2 * 3];    // for each tile, 2 triangles, 3 points 
		Vector3[] verts = new Vector3[(sizeX + 1) * (sizeZ + 1)];
        Vector2[] uvs = new Vector2[(sizeX + 1) * (sizeZ + 1)];
        Mesh chunkMesh = new Mesh();

        Color c = new Color (1,1,1,1);	// opaque white
		Color[] colors = new Color[(sizeX + 1) * (sizeZ + 1)];	// colors is per vertex

		// Configure default UV (per vertex tile)
		for (int i = 0; i < sizeX; i += 2) {
			for (int j = 0; j < sizeZ; j += 2) {
				// by default, every tile is covering the whole texture
				uvs [((i) * sizeX) + j + 1] = new Vector2 (0, 1);
				uvs [((i + 1) * sizeX) + j + 1] = new Vector2 (1, 1);
				uvs [((i) * sizeX) + j] = new Vector2 (0, 1);
				uvs [((i + 1) * sizeX) + j] = new Vector2 (1, 0);
			}
		}

        // Generate Triangle Indexes
        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeZ; j++)
            {
                if (Mathf.Abs(
					gridMap.vertexMap[((i + 1) * (gridMap.gridSizeX + 1)) + j + 1].height -
					gridMap.vertexMap[((i) * (gridMap.gridSizeX + 1)) + j].height) <
                    Mathf.Abs(
						gridMap.vertexMap[((i) * (gridMap.gridSizeX + 1)) + j + 1].height -
						gridMap.vertexMap[((i + 1) * (gridMap.gridSizeX + 1)) + j].height))
                {
                    // 0 -- 1
                    // |  /
                    // | /
                    // 2
                    tris[(((i * sizeX) + j) * 6) + 0] = ((i) * (sizeX + 1)) + j + 1;
                    tris[(((i * sizeX) + j) * 6) + 1] = ((i + 1) * (sizeX + 1)) + j + 1;
                    tris[(((i * sizeX) + j) * 6) + 2] = ((i) * (sizeX + 1)) + j;

                    //      4
                    //    / |
                    //   /  |
                    // 3 -- 5
                    tris[(((i * sizeX) + j) * 6) + 3] = ((i) * (sizeX + 1)) + j;
                    tris[(((i * sizeX) + j) * 6) + 4] = ((i + 1) * (sizeX + 1)) + j + 1;
                    tris[(((i * sizeX) + j) * 6) + 5] = ((i + 1) * (sizeX + 1)) + j;
                }
                else
                {
                    // 0 -- 1
                    //  \   |
                    //    \ |
                    //      2
                    tris[(((i * sizeX) + j) * 6) + 0] = ((i) * (sizeX + 1)) + j + 1;
                    tris[(((i * sizeX) + j) * 6) + 1] = ((i + 1) * (sizeX + 1)) + j + 1;
                    tris[(((i * sizeX) + j) * 6) + 2] = ((i + 1) * (sizeX + 1)) + j;

                    // 4
                    // | \
                    // |  \
                    // 3 -- 5
                    tris[(((i * sizeX) + j) * 6) + 3] = ((i) * (sizeX + 1)) + j;
                    tris[(((i * sizeX) + j) * 6) + 4] = ((i) * (sizeX + 1)) + j + 1;
                    tris[(((i * sizeX) + j) * 6) + 5] = ((i + 1) * (sizeX + 1)) + j;
                }

            }
        }

        // Configure Vertices Positions
        for (int i = 0; i < sizeX + 1; i++) {
			for (int j = 0; j < sizeZ + 1; j++) {
				//float rV = UnityEngine.Random.Range (0.92f, 1.0f);
				//colors [i * (sizeX + 1) + j] = gridMap.
				Color col = Color.white;
				if (startX + i < gridMap.gridSizeX && startZ + j < gridMap.gridSizeZ)
					col = gridMap.tileMap [
						((startX + i) * (gridMap.gridSizeX)) + (startZ + j)
					].color;
				colors [i * (sizeX + 1) + j] = col;
				verts [i * (sizeX + 1) + j] = new Vector3 (i, 
					gridMap.vertexMap[
						((startX+i) * (gridMap.gridSizeX+1)) + (startZ+j)
					].height, 
					j);
			}
		}

        // Configure vertices, triangles, uvs, colors
        chunkMesh.vertices = verts;
		chunkMesh.triangles = tris;
		chunkMesh.uv = uvs;
		chunkMesh.colors = colors;

        // Final mesh processing

        chunkMesh.RecalculateNormals();
        chunkMesh.Optimize();

		return chunkMesh;*/
	}

	public Mesh GenerateChunkWaterMesh(int startX, int startZ, int sizeX, int sizeZ, List<float> heightList) {
		List<CombineInstance> combineInstances = new List<CombineInstance> ();
		Mesh chunkMesh = new Mesh();

		for (int i = 0; i < sizeX; i ++) {
			for (int j = 0; j < sizeZ; j ++) {
				bool generateWater = false;
				float avg_water_height = 0;
				bool useHeightFromAvg = false;

				for (int a = i - 1; a <= i + 1; a++) {
					for (int b = j - 1; b <= j + 1; b++) {
						if (a >= 0 && b >= 0 && a < sizeX && b < sizeZ) {
							if (gridMap.tileMap [(startX + a) * gridMap.gridSizeX + (startZ + b)].waterLevel > 0) {

								float ttile_water_height = 0;

								ttile_water_height = ((gridMap.vertexMap 
									[(startX + a) * (gridMap.gridSizeX+1) + (startZ + b)].level - GridMap.LEVEL_N2) *
									GridMap.LEVEL_HEIGHT);

								ttile_water_height = (ttile_water_height + ((gridMap.vertexMap 
									[(startX + a) * (gridMap.gridSizeX+1) + (startZ + b+1)].level - GridMap.LEVEL_N2) *
									GridMap.LEVEL_HEIGHT)) * 0.5f;

								ttile_water_height = (ttile_water_height + ((gridMap.vertexMap 
									[(startX + a+1) * (gridMap.gridSizeX+1) + (startZ + b)].level - GridMap.LEVEL_N2) *
									GridMap.LEVEL_HEIGHT)) * 0.5f;

								ttile_water_height = (ttile_water_height + ((gridMap.vertexMap 
									[(startX + a+1) * (gridMap.gridSizeX+1) + (startZ + b+1)].level - GridMap.LEVEL_N2) *
									GridMap.LEVEL_HEIGHT)) * 0.5f;

								if (!generateWater)
									avg_water_height = ttile_water_height;
								else
									avg_water_height = (avg_water_height + (ttile_water_height)) * 0.5f;
								
								generateWater = true;
							} else {
								// if current tile is not water, if water is to be generated, will use the median
								if (a == i && b == j)
									useHeightFromAvg = true;
							}
						}
					}
				}

				// Generate only 1 mesh and combine, if water
				if (generateWater) {
					Mesh mesh = new Mesh ();
					Vector3[] verts = new Vector3[6];
					Vector2[] uvs = new Vector2[6];
					int[] tris;

					if (UnityEngine.Random.Range(0f, 1f) < 0.5f) {
						// 2 -- 5
						// |  /
						// | /
						// 4
						//      3
						//    / |
						//   /  |
						// 0 -- 1

						float height0 = (useHeightFromAvg)? avg_water_height :
							((gridMap.vertexMap [((startX + i) * (gridMap.gridSizeX + 1)) + (startZ + j)].level - GridMap.LEVEL_N2) * 
								GridMap.LEVEL_HEIGHT);
						float height1 = (useHeightFromAvg)? avg_water_height :
							((gridMap.vertexMap [((startX + i + 1) * (gridMap.gridSizeX + 1)) + (startZ + j)].level - GridMap.LEVEL_N2) * 
								GridMap.LEVEL_HEIGHT);
						float height2 = (useHeightFromAvg)? avg_water_height :
							((gridMap.vertexMap [((startX + i) * (gridMap.gridSizeX + 1)) + (startZ + j + 1)].level - GridMap.LEVEL_N2) * 
								GridMap.LEVEL_HEIGHT);
						float height3 = (useHeightFromAvg)? avg_water_height :
							((gridMap.vertexMap [((startX + i + 1) * (gridMap.gridSizeX + 1)) + (startZ + j + 1)].level - GridMap.LEVEL_N2) * 
								GridMap.LEVEL_HEIGHT);

						float height_l = -GridMap.LEVEL_WATER_STEP * 0.5f;

						verts [0] = new Vector3 ((i), height0+height_l, (j));
						verts [1] = new Vector3 ((i + 1), height1+height_l, (j));
						verts [2] = new Vector3 ((i), height2+height_l, (j + 1));
						verts [3] = new Vector3 ((i + 1), height3+height_l, (j + 1));
						verts [4] = verts [0];
						verts [5] = verts [3];

						uvs [0] = new Vector2 (0.0f, 1.0f);
						uvs [1] = new Vector2 (1.0f, 1.0f);
						uvs [2] = new Vector2 (0.0f, 0.0f);
						uvs [3] = new Vector2 (1.0f, 0.0f);
						uvs [4] = uvs [0];
						uvs [5] = uvs [3];

						tris = new int[6] { 0, 3, 1, 5, 4, 2 };

						for (int k = 0; k < 6; k++)
							heightList.Add (verts[k].y);
					} else {
						// 5 -- 3
						//  \   |
						//    \ |
						//      4
						// 2
						// | \
						// |  \
						// 0 -- 1

						float height0 = (useHeightFromAvg)? avg_water_height :
							((gridMap.vertexMap [((startX + i) * (gridMap.gridSizeX + 1)) + (startZ + j)].level - GridMap.LEVEL_N2) * 
								GridMap.LEVEL_HEIGHT);
						float height1 = (useHeightFromAvg)? avg_water_height :
							((gridMap.vertexMap [((startX + i + 1) * (gridMap.gridSizeX + 1)) + (startZ + j)].level - GridMap.LEVEL_N2) * 
								GridMap.LEVEL_HEIGHT);
						float height2 = (useHeightFromAvg)? avg_water_height :
							((gridMap.vertexMap [((startX + i) * (gridMap.gridSizeX + 1)) + (startZ + j + 1)].level - GridMap.LEVEL_N2) * 
								GridMap.LEVEL_HEIGHT);
						float height3 = (useHeightFromAvg)? avg_water_height :
							((gridMap.vertexMap [((startX + i + 1) * (gridMap.gridSizeX + 1)) + (startZ + j + 1)].level - GridMap.LEVEL_N2) * 
								GridMap.LEVEL_HEIGHT);
						float height_l = -GridMap.LEVEL_WATER_STEP * 0.5f;

						verts [0] = new Vector3 ((i), height0+height_l, (j));
						verts [1] = new Vector3 ((i + 1), height1+height_l, (j));
						verts [2] = new Vector3 ((i), height2+height_l, (j + 1));
						verts [3] = new Vector3 ((i + 1), height3+height_l, (j + 1));
						verts [4] = verts [1];
						verts [5] = verts [2];

						uvs [0] = new Vector2 (0.0f, 1.0f);
						uvs [1] = new Vector2 (1.0f, 1.0f);
						uvs [2] = new Vector2 (0.0f, 0.0f);
						uvs [3] = new Vector2 (1.0f, 0.0f);
						uvs [4] = uvs [1];
						uvs [5] = uvs [2];

						tris = new int[6] { 0, 2, 1, 4, 5, 3 };
						for (int k = 0; k < 6; k++)
							heightList.Add (verts[k].y);
					}

					// generate mesh
					mesh.vertices = verts;
					mesh.triangles = tris;
					mesh.uv = uvs;
					mesh.RecalculateNormals ();

					// combines
					CombineInstance combineInstance = new CombineInstance ();
					combineInstance.mesh = mesh;
					combineInstances.Add (combineInstance);
				}
			}
		}

		chunkMesh.CombineMeshes (combineInstances.ToArray(), true, false);

		return chunkMesh;
	}

    /// <summary>
    /// Fixes the normals in each of the chunk edges (which were bugged and reported on #ISSUE001).
    /// Problem was caused because the triangle faces that were in other chunks were not computed when calculating the normals.
    /// This method recalculates every vertex normal at the edges of any group of Meshes passed as parameters.
    /// </summary>
    /// <returns>The fixed Meshes.</returns>
    /// <param name="chunkMeshes">Originally generated meshes.</param>
    /// <param name="chunksX">Number of chunks on the X axis.</param>
    /// <param name="chunksZ">Number of chunks in the Z axis.</param>
    private Mesh[,] fixNormalsOnChunkEdges(Mesh[,] chunkMeshes, int chunksX, int chunksZ)
    {
        // For each chunk
        for (int i = 0; i < chunksX; i++)
        {
            for (int j = 0; j < chunksZ; j++)
            {
                // If there is a chunk on its right side
                if (i < (chunksX - 1))
                {
                    //recalculate normals for its right edge vertixes and for the other chunk left edge vertixes
                    Vector3[] chunkANormals = chunkMeshes[i, j].normals;
                    Vector3[] chunkBNormals = chunkMeshes[i + 1, j].normals;

					for (int x = 0; x < GridMap.CHUNK_SIZE; x++)
                    {
						int aIndex = (GridMap.CHUNK_SIZE) * (GridMap.CHUNK_SIZE + 1) + x;
                        int bIndex = x;

                        Vector3 resultantNormal = (chunkANormals[aIndex] + chunkBNormals[bIndex]) * 0.5f;

                        chunkANormals[aIndex] = resultantNormal;
                        chunkBNormals[bIndex] = resultantNormal;
                    }
                    chunkMeshes[i, j].normals = chunkANormals;
                    chunkMeshes[i + 1, j].normals = chunkBNormals;
                }
                
                // If there is a chunk right below it
                if (j < (chunksZ - 1))
                {
                    //recalculate normals for its down edge vertixes and for the other chunk up edge vertixes
                    Vector3[] chunkANormals = chunkMeshes[i, j].normals;
                    Vector3[] chunkBNormals = chunkMeshes[i, j + 1].normals;

					for (int z = 0; z < GridMap.CHUNK_SIZE; z++)
                    {
						int aIndex = z * (GridMap.CHUNK_SIZE + 1) + GridMap.CHUNK_SIZE;
						int bIndex = z * (GridMap.CHUNK_SIZE + 1);

                        Vector3 resultantNormal = (chunkANormals[aIndex] + chunkBNormals[bIndex]) * 0.5f;

                        chunkANormals[aIndex] = resultantNormal;
                        chunkBNormals[bIndex] = resultantNormal;

                    }
                    chunkMeshes[i, j].normals = chunkANormals;
                    chunkMeshes[i, j + 1].normals = chunkBNormals;
                }
            }
        }

        return chunkMeshes;
    }

    #endregion

    #endif
}
