﻿using UnityEngine;
using System.Collections;

/* Map Controller Class
 * Handles all high-level Map-related stuff.
 * A map is defined as:
	* The Terrain
	* The Tilemap
	* The Structures
 */
using System.Diagnostics;


public class MapController : MonoBehaviour {

	[System.Serializable]
	public struct ChunkInstance {
		public GameObject gObject;
		public Coroutine loadCoroutine;	// if null: loaded or not loaded. if not null, loading
		public Coroutine unloadCoroutine;	// if null: loaded or not loaded. if not null, unloading
		public bool loaded;		// only loaded when coroutine done
	}

	#region Public Variables

	[Header("Map Settings")]
	public Material terrainMaterial;
	public Material waterMaterial;
	public bool previewMapGizmos = false;

	[HideInInspector] public GridMap gridMap;
	[HideInInspector] public ChunkMap chunkMap;	// all map

	[HideInInspector] public ChunkInstance[,] chunkObjects;

	[Header("Dynamic Chunk Loading")]
	public Transform master;
	public int radius;
	public float baseWaitTime = 0.4f;
	public float randomWaitTime = 0.2f;
	public Vector2i currentMasterChunk;

	#endregion

	#region Private Variables

	private bool isInit = false;

	private MapPathfinder mapPathfinder;

	#endregion

	#region Control Methods

	void Awake() {
		//gridMap = Resources.Load("Terrains/" + name + "_grid.map"); //Gridmap.LoadFromFile ("Assets/Resources/Terrains/" + name + "_grid.map");
	}

	// Use this for initialization
	void Start () {
		init ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	#endregion

	#region Generation Methods

	public void confirmDynamicObjectCreation(GameObject _gameObject) {
		// TODO get some information as to what kind of object it is?
		//  could be useful to save stuff
		_gameObject.transform.parent = transform;
	}

	#endregion

	#region Manager Methods

	public void init() {
		if (!isInit) {
			isInit = true;
			//UnityEngine.Debug.Log ("Initing...");
			//Stopwatch stopWatch = new Stopwatch ();
			//stopWatch.Start ();

			// load grid map info from file
			gridMap = GridMap.LoadFromFile ("Assets/Resources/");

			// create a HUGE chunk map
			chunkMap = new ChunkMap (gridMap.sizeX * GridMap.CHUNK_SIZE, 
				gridMap.sizeZ * GridMap.CHUNK_SIZE);

			chunkObjects = new ChunkInstance[gridMap.sizeX, gridMap.sizeZ];

			loadBasic ();

			//stopWatch.Stop ();
			//UnityEngine.Debug.Log ("Inited! Ellapsed time: " + stopWatch.ElapsedMilliseconds + "ms");

			mapPathfinder = GetComponent<MapPathfinder> ();
			mapPathfinder.init (this);

            // TODO make sure that it doesn't load everything

			// load current chunk (immediately)
			currentMasterChunk.a = Mathf.FloorToInt ((float)master.position.x / (float)GridMap.CHUNK_SIZE);
			currentMasterChunk.b = Mathf.FloorToInt ((float)master.position.z / (float)GridMap.CHUNK_SIZE);

			for (int a = -radius; a <= radius; a++) {
				for (int b = -radius; b <= radius; b++) {
					int chunkX = currentMasterChunk.a + a;
					int chunkZ = currentMasterChunk.b + b;

					if (chunkX < 0 || chunkZ < 0 || chunkX >= gridMap.sizeX || chunkZ >= gridMap.sizeZ)
						continue;

					loadChunkGridImmediate (chunkX, chunkZ);
				}
			}
		}
	}

	void FixedUpdate() {
		currentMasterChunk.a = Mathf.FloorToInt ((float)master.position.x / (float)GridMap.CHUNK_SIZE);
		currentMasterChunk.b = Mathf.FloorToInt ((float)master.position.z / (float)GridMap.CHUNK_SIZE);

		for (int a = -radius-1; a <= radius+1; a++) {
			for (int b = -radius-1; b <= radius+1; b++) {
				int chunkX = currentMasterChunk.a + a;
				int chunkZ = currentMasterChunk.b + b;

				if (chunkX < 0 || chunkZ < 0 || chunkX >= gridMap.sizeX || chunkZ >= gridMap.sizeZ)
					continue;

				// chunks that must be activated
				if (a >= -radius && a <= radius && b >= -radius && b <= radius) {
					if (!chunkObjects[chunkX, chunkZ].loaded) {
						if (chunkObjects[chunkX, chunkZ].loadCoroutine == null) {
							if (chunkObjects [chunkX, chunkZ].unloadCoroutine != null) 
								StopCoroutine (chunkObjects [chunkX, chunkZ].unloadCoroutine);
							chunkObjects [chunkX, chunkZ].unloadCoroutine = null;
							chunkObjects [chunkX, chunkZ].loadCoroutine = StartCoroutine (loadChunkGridAsync(chunkX, chunkZ));
						}
					}
				} 
				// chunks that should be deactivated
				else {
					if (chunkObjects [chunkX, chunkZ].loaded) {
						if (chunkObjects [chunkX, chunkZ].unloadCoroutine == null) {
							if (chunkObjects [chunkX, chunkZ].loadCoroutine != null) 
								StopCoroutine (chunkObjects [chunkX, chunkZ].loadCoroutine);
							chunkObjects [chunkX, chunkZ].loadCoroutine = null;
							chunkObjects [chunkX, chunkZ].unloadCoroutine = StartCoroutine (unloadChunkGridAsync(chunkX, chunkZ));
						}
					}
				}

			}
		}
	}

	#endregion

	#region Chunk Loading and Unloading

	// load basic info and gameobjects for all chunks
	private void loadBasic() {
		for (int i = 0; i < gridMap.sizeX; i++) {
			for (int j = 0; j < gridMap.sizeZ; j++) {
				gridMap.chunkMaps[i, j] = ChunkMap.LoadFromFile (
					"Assets/Resources/Chunks/chunk_" + i + "_" + j + ".asset");

				for (int x = 0; x < GridMap.CHUNK_SIZE+1; x++) {
					for (int z = 0; z < GridMap.CHUNK_SIZE+1; z++) {
						chunkMap.vertexMap [(x+(i*GridMap.CHUNK_SIZE)) * ((gridMap.sizeX * GridMap.CHUNK_SIZE)+1) + (z+(j*GridMap.CHUNK_SIZE))].copyFrom (
							gridMap.chunkMaps[i, j].vertexMap [x * (GridMap.CHUNK_SIZE+1) + z]);

						if (x < GridMap.CHUNK_SIZE && z < GridMap.CHUNK_SIZE) {
							chunkMap.tileMap [(x+(i*GridMap.CHUNK_SIZE)) * (gridMap.sizeX * GridMap.CHUNK_SIZE) + (z+(j*GridMap.CHUNK_SIZE))].copyFrom (
								gridMap.chunkMaps[i, j].tileMap [x * GridMap.CHUNK_SIZE + z]);
						}
					}
				}

				// Initialize chunk instance
				chunkObjects [i, j].gObject = new GameObject ();
				chunkObjects [i, j].gObject.name = "Chunk " + i + ", " + j;
				chunkObjects [i, j].gObject.transform.parent = transform;
				chunkObjects [i, j].gObject.transform.localPosition = new Vector3 (i * GridMap.CHUNK_SIZE, 0, j * GridMap.CHUNK_SIZE);
				chunkObjects [i, j].loadCoroutine = null;
				chunkObjects [i, j].loaded = false;
			}
		}
	}

	private void loadChunkGridImmediate(int i, int j) {
		// load terrain
		GameObject terrainObject = new GameObject ();
		terrainObject.name = "Terrain";
		terrainObject.transform.parent = chunkObjects [i, j].gObject.transform;
		terrainObject.transform.localPosition = Vector3.zero;
		terrainObject.layer = LayerMask.NameToLayer("Terrain");

		MeshFilter meshFilter = terrainObject.AddComponent<MeshFilter> ();
		meshFilter.sharedMesh = Resources.Load<Mesh> ("Terrain/chunk_" + i + "_" + j);

		MeshRenderer meshRenderer = terrainObject.AddComponent<MeshRenderer> ();
		meshRenderer.sharedMaterial = terrainMaterial;
		meshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.TwoSided;

		MeshCollider meshCollider = terrainObject.AddComponent<MeshCollider> ();

		// load procedural content
		for (int a = 0; a < GridMap.PROCEDURAL_OBJECTS_COUNT; a++) {
			GameObject proceduralObject = Instantiate(Resources.Load<GameObject> ("Procedural/chunk_" + i + "_" + j + "_" + a));
			proceduralObject.transform.parent = chunkObjects [i, j].gObject.transform;
			proceduralObject.transform.localPosition = Vector3.zero;
		}

		GameObject dynamicObjects = Instantiate(Resources.Load<GameObject> ("Dynamic/chunk_" + i + "_" + j));
		dynamicObjects.name = "Dynamic objects";
		dynamicObjects.transform.parent = chunkObjects [i, j].gObject.transform;
		dynamicObjects.transform.localPosition = Vector3.zero;

		// load water
		GameObject waterTerrainObject = new GameObject ();
		waterTerrainObject.name = "Water";
		waterTerrainObject.transform.parent = chunkObjects [i, j].gObject.transform;
		waterTerrainObject.transform.localPosition = Vector3.zero;
		waterTerrainObject.layer = LayerMask.NameToLayer("Water");

		MeshFilter waterMeshFilter = waterTerrainObject.AddComponent<MeshFilter> ();
		waterMeshFilter.sharedMesh = Resources.Load<Mesh> ("Terrain/chunk_" + i + "_" + j + "_water");

		MeshRenderer waterMeshRenderer = waterTerrainObject.AddComponent<MeshRenderer> ();
		waterMeshRenderer.sharedMaterial = waterMaterial;
		waterMeshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.TwoSided;

		//MeshCollider waterMeshCollider = waterTerrainObject.AddComponent<MeshCollider> ();
		//waterMeshCollider.isTrigger = true;

		Water waterScript = waterTerrainObject.AddComponent<Water> ();
		waterScript.waveHeight = 0.1f;
		waterScript.waveLength = 0.05489128f;
		waterScript.vertHeights = gridMap.chunkMaps[i, j].waterMeshHeights;

		chunkObjects [i, j].loaded = true;
		chunkObjects [i, j].loadCoroutine = null;
	}

	private IEnumerator loadChunkGridAsync(int i, int j) {
		ResourceRequest request;

		// load terrain
		GameObject terrainObject = new GameObject ();
		terrainObject.name = "Terrain";
		terrainObject.transform.parent = chunkObjects [i, j].gObject.transform;
		terrainObject.transform.localPosition = Vector3.zero;
		terrainObject.layer = LayerMask.NameToLayer("Terrain");

		yield return new WaitForSecondsRealtime ((baseWaitTime + Random.Range(0, randomWaitTime)) * 0.5f);

		MeshFilter meshFilter = terrainObject.AddComponent<MeshFilter> ();
		meshFilter.sharedMesh = Resources.Load<Mesh> ("Terrain/chunk_" + i + "_" + j);

		yield return new WaitForSecondsRealtime (baseWaitTime + Random.Range(0, randomWaitTime));

		MeshRenderer meshRenderer = terrainObject.AddComponent<MeshRenderer> ();
		meshRenderer.sharedMaterial = terrainMaterial;
		meshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.TwoSided;

		MeshCollider meshCollider = terrainObject.AddComponent<MeshCollider> ();

		yield return new WaitForSecondsRealtime (baseWaitTime + Random.Range(0, randomWaitTime));

		// load procedural content
		for (int a = 0; a < GridMap.PROCEDURAL_OBJECTS_COUNT; a++) {
			request = Resources.LoadAsync<GameObject> ("Procedural/chunk_" + i + "_" + j + "_" + a);

			while (!request.isDone) {
				yield return new WaitForSecondsRealtime ((baseWaitTime + Random.Range(0, randomWaitTime)) * 0.5f);
			}

			GameObject proceduralObject = Instantiate(request.asset as GameObject);
			proceduralObject.transform.parent = chunkObjects [i, j].gObject.transform;
			proceduralObject.transform.localPosition = Vector3.zero;

			yield return new WaitForSecondsRealtime ((baseWaitTime + Random.Range(0, randomWaitTime)) * 0.5f);
		}

		// dynamic
		request = Resources.LoadAsync<GameObject> ("Dynamic/chunk_" + i + "_" + j);

		while (!request.isDone) {
			yield return new WaitForSecondsRealtime ((baseWaitTime + Random.Range(0, randomWaitTime)) * 0.5f);
		}

		GameObject dynamicObjects = Instantiate(request.asset as GameObject);
		dynamicObjects.name = "Dynamic objects";
		dynamicObjects.transform.parent = chunkObjects [i, j].gObject.transform;
		dynamicObjects.transform.localPosition = Vector3.zero;

		yield return new WaitForSecondsRealtime (baseWaitTime + Random.Range(0, randomWaitTime));

		// load water
		GameObject waterTerrainObject = new GameObject ();
		waterTerrainObject.name = "Water";
		waterTerrainObject.transform.parent = chunkObjects [i, j].gObject.transform;
		waterTerrainObject.transform.localPosition = Vector3.zero;
		waterTerrainObject.layer = LayerMask.NameToLayer("Water");

		yield return new WaitForSecondsRealtime ((baseWaitTime + Random.Range(0, randomWaitTime)) * 0.5f);

		MeshFilter waterMeshFilter = waterTerrainObject.AddComponent<MeshFilter> ();
		waterMeshFilter.sharedMesh = Resources.Load<Mesh> ("Terrain/chunk_" + i + "_" + j + "_water");

		yield return new WaitForSecondsRealtime (baseWaitTime + Random.Range(-randomWaitTime, randomWaitTime));

		MeshRenderer waterMeshRenderer = waterTerrainObject.AddComponent<MeshRenderer> ();
		waterMeshRenderer.sharedMaterial = waterMaterial;
		waterMeshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.TwoSided;

		//MeshCollider waterMeshCollider = waterTerrainObject.AddComponent<MeshCollider> ();
		//waterMeshCollider.isTrigger = true;

		Water waterScript = waterTerrainObject.AddComponent<Water> ();
		waterScript.waveHeight = 0.1f;
		waterScript.waveLength = 0.001f;
		waterScript.vertHeights = gridMap.chunkMaps[i, j].waterMeshHeights;

		chunkObjects [i, j].loaded = true;
		chunkObjects [i, j].loadCoroutine = null;
	}

	private IEnumerator unloadChunkGridAsync(int i, int j) {
		while (chunkObjects[i, j].gObject.transform.childCount > 0) {
			Destroy (chunkObjects[i, j].gObject.transform.GetChild(0).gameObject);

			yield return new WaitForSecondsRealtime ((baseWaitTime + Random.Range(0, randomWaitTime)) * 0.5f);
		}

		chunkObjects [i, j].loaded = false;
		chunkObjects [i, j].unloadCoroutine = null;
	}

	#endregion

	void OnDrawGizmos() {
		if (gridMap != null && previewMapGizmos) 
			for (int i = 0; i < 2*GridMap.CHUNK_SIZE; i++) {
				for (int j = 0; j < 2*GridMap.CHUNK_SIZE; j++) {
					if (chunkMap.tileMap[i * chunkMap.gridSizeX + j].isWalkable)
						Gizmos.color = Color.green;
					else
						Gizmos.color = Color.red;
					Gizmos.DrawCube (
						new Vector3(i+0.5f, 
							chunkMap.GetMedianHeightOnTile(i, j)+0.5f, 
							j+0.5f),
						new Vector3(0.5f, 0.5f, 0.5f));
				}
			}
	}
}
