﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;

[System.Serializable]
public class GridMap {
	public const int CHUNK_SIZE = 64;
	public const float TILE_SIZE = 1;

	public const int MAX_AREAS = 32;

	public const int LEVEL_N = 40;
	public const int LEVEL_N2 = 20;
	public const float LEVEL_HEIGHT = 0.4f;

	public const int LEVEL_WATER_MAX = 4;
	public const float LEVEL_WATER_STEP = 1.0f;

	public const byte MAX_OBSCURANCE = 16;
	public const float OBSCURANCE_STEP = 1.0f / (float)MAX_OBSCURANCE;

	public const int PROCEDURAL_OBJECTS_COUNT = 8;

	[SerializeField] public string folder;

	[SerializeField] public int sizeX, sizeZ;	// in number of chunks
	[System.NonSerialized] public ChunkMap[,] chunkMaps;	// used in editor only

	[SerializeField] public Area[] areas;
	public string[] areaNames {
		get {
			string[] names = new string[MAX_AREAS];
			for (int i = 0; i < MAX_AREAS; i++)
				names [i] = areas [i].name;
			return names;
		}
	}

	public GridMap(string _folder, int _sizeX, int _sizeZ) {
		folder = _folder;
		sizeX = _sizeX;
		sizeZ = _sizeZ;

		chunkMaps = new ChunkMap[sizeX, sizeZ];

		areas = new Area[MAX_AREAS];
		for (int i = 0; i < MAX_AREAS; i++)
			areas [i].Init ();
	}

	public static GridMap LoadFromFile(string _folder) {
		if (File.Exists(_folder + "gmap.asset")) {
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (_folder + "gmap.asset", FileMode.Open);
			GridMap gm = (GridMap)bf.Deserialize (file);
			gm.folder = _folder;
			file.Close ();
			gm.chunkMaps = new ChunkMap[gm.sizeX, gm.sizeZ];
			return gm;
		} else {
			return null;
		}
	}

	public void SaveToFile() {
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (folder + "gmap.asset");
		bf.Serialize (file, this);
		file.Close ();
	}

	public Tile getTile(int x, int z) {
		int chunkX = Mathf.FloorToInt ((float)x / (float)CHUNK_SIZE);
		int chunkZ = Mathf.FloorToInt ((float)z / (float)CHUNK_SIZE);
		int inChunkX = x % CHUNK_SIZE;
		int inChunkZ = z % CHUNK_SIZE;

		//Debug.Log (string.Format("x: {0}, z: {1} == chunkX: {2}, chunkZ: {3}, inX: {4}, inZ: {5}", x, z, chunkX, chunkZ, inChunkX, inChunkZ));

		return chunkMaps [chunkX, chunkZ].tileMap [inChunkX * CHUNK_SIZE + inChunkZ];
	}
}

/// <summary>
/// Gridmap Class.
/// This class contains the information and methods for handling a GridMap.
/// A GridMap defines tiles (ground type, object placement, etc), and the heightmap.
/// </summary>
[System.Serializable]
public class ChunkMap {
	public Tile[] tileMap;
	public TerrainVertex[] vertexMap;
	public int gridSizeX, gridSizeZ;		// plz plz plz plz plzz don`t change this outside :)
	public float[] waterMeshHeights;

	[System.NonSerialized] public Color[] tileColors;

	public ChunkMap(int _gridSizeX, int _gridSizeZ) {
		gridSizeX = _gridSizeX;
		gridSizeZ = _gridSizeZ;

		tileMap = new Tile[gridSizeX * gridSizeZ];
		vertexMap = new TerrainVertex[(gridSizeX + 1) * (gridSizeZ + 1)];

		for (int i = 0; i < gridSizeX; i++) {
			for (int j = 0; j < gridSizeZ ; j++) {
				tileMap [i * gridSizeX + j] = new Tile (i, j);
			}
		}
		for (int i = 0; i < gridSizeX + 1; i++) {
			for (int j = 0; j < gridSizeZ + 1; j++) {
				vertexMap [i * (gridSizeX + 1) + j].height = 0;
				vertexMap [i * (gridSizeX + 1) + j].level = GridMap.LEVEL_N2;
			}
		}
	}

	public static ChunkMap LoadFromFile(string fileName) {
		if (File.Exists(fileName)) {
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (fileName, FileMode.Open);
			ChunkMap gm = (ChunkMap)bf.Deserialize (file);
			file.Close ();
			return gm;
		} else {
			return null;
		}
	}

	public void SaveToFile(string fileName) {
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (fileName);
		bf.Serialize (file, this);
		file.Close ();
	}

	// Basic random function
	public void InitAsPerlinNoise(float scale, float weight) {
		for (int i = 0; i < gridSizeX + 1; i++) {
			for (int j = 0; j < gridSizeZ + 1; j++) {
				vertexMap[i * (gridSizeX + 1) + j].height = Mathf.PerlinNoise (i * scale, j * scale) * weight;
			}
		}
	}

	public void calculateVertexHeights(GridMap gridMap, Vector2i offset) {
		for (int i = 0; i < gridSizeX + 1; i++) {
			for (int j = 0; j < gridSizeZ + 1; j++) {
				float perlinValue = 0;
				float perlinScale = 0;
				float perlinWeight = 0;

				for (int a = -1; a <= 0; a++) {
					for (int b = -1; b <= 0; b++) {
						int posX = i + a + (offset.a * GridMap.CHUNK_SIZE);
						int posZ = j + b + (offset.b * GridMap.CHUNK_SIZE);

						if (posX < 0 || posZ < 0 || posX >= (gridMap.sizeX*gridSizeX) || posZ >= (gridMap.sizeZ*gridSizeZ)) 
							continue;

						Tile tile = gridMap.getTile (posX, posZ);

						if (a == -1 && b == -1) {
							perlinScale = (gridMap.areas [tile.areaIndex].perlinScale);
							perlinWeight = (gridMap.areas [tile.areaIndex].perlinWeight);
						} else {
							perlinScale = (perlinScale + gridMap.areas [tile.areaIndex].perlinScale) * 0.5f;
							perlinWeight = (perlinWeight + gridMap.areas [tile.areaIndex].perlinWeight) * 0.5f;
						}
					}
				}

				perlinValue = ((Mathf.PerlinNoise ((i + (offset.a*GridMap.CHUNK_SIZE)) * perlinScale, 
					(j + (offset.b*GridMap.CHUNK_SIZE)) * perlinScale) - 0.5f) * 2 * perlinWeight);


				vertexMap [i * (gridSizeX + 1) + j].height = 
					((vertexMap [i * (gridSizeX + 1) + j].level-GridMap.LEVEL_N2) * GridMap.LEVEL_HEIGHT)+
					perlinValue;
			}
		}

		for (int i = 0; i < gridSizeX; i++) {
			for (int j = 0; j < gridSizeZ; j++) {
				if (tileMap[i * (gridSizeX) + j].waterLevel > 0) {
					vertexMap [(i) * (gridSizeX + 1) + (j)].height -= tileMap [i * (gridSizeX) + j].waterLevel * 
						GridMap.LEVEL_WATER_STEP * ((i == 0? 0.75f : 0.25f) + (j == 0? 0.75f : 0.25f));
					vertexMap [(i+1) * (gridSizeX + 1) + (j)].height -= tileMap [i * (gridSizeX) + j].waterLevel * 
						GridMap.LEVEL_WATER_STEP * ((i == gridSizeX-1? 0.75f : 0.25f) + (j == 0? 0.75f : 0.25f));
					vertexMap [(i) * (gridSizeX + 1) + (j+1)].height -= tileMap [i * (gridSizeX) + j].waterLevel * 
						GridMap.LEVEL_WATER_STEP * ((i == 0? 0.75f : 0.25f) + (j == gridSizeZ-1? 0.75f : 0.25f));
					vertexMap [(i+1) * (gridSizeX + 1) + (j+1)].height -= tileMap [i * (gridSizeX) + j].waterLevel * 
						GridMap.LEVEL_WATER_STEP * ((i == gridSizeX-1? 0.75f : 0.25f) + (j == gridSizeZ-1? 0.75f : 0.25f));
				}
			}
		}
	}

	public void resetAllTilesToEmpty() {
		for (int i = 0; i < gridSizeX ; i++) {
			for (int j = 0; j < gridSizeZ; j++) {
				tileMap [i * (gridSizeX) + j].isEmpty = true;
			}
		}
	}

	public List<Tile> GetNeighbours(Tile node) {
		List<Tile> neighbours = new List<Tile>();

		for (int x = -1; x <= 1; x++) {
			for (int z = -1; z <= 1; z++) {
				if (x == 0 && z == 0)
					continue;

				int checkX = node.gridPosition.a + x;
				int checkZ = node.gridPosition.b + z;

				if (checkX >= 0 && checkX < gridSizeX && checkZ >= 0 && checkZ < gridSizeZ) {
					neighbours.Add(tileMap[checkX * gridSizeX + checkZ]);
				}
			}
		}

		return neighbours;
	}

	/// <summary>
	/// Gets the tile position from the given world position.
	/// </summary>
	/// <returns>The tile position from world position.</returns>
	/// <param name="position">The world position.</param>
	public Vector2i getTilePositionFromWorldPosition(Vector3 position) {
		return new Vector2i (Mathf.RoundToInt(position.x-0.5f), Mathf.RoundToInt(position.z-0.5f));
	}

	public Tile getTileFromWorldPosition(Vector3 position) {
		return tileMap[Mathf.RoundToInt(position.x-0.5f) * gridSizeX + Mathf.RoundToInt(position.z-0.5f)];
	}

	public Tile getTileFromGridPosition(Vector2i position) {
		return tileMap[position.a * gridSizeX + position.b];
	}

	public Vector3 getWorldPositionFromTile(Vector2i position) {
		return new Vector3 (
			position.a+0.5f,
			GetMedianHeightOnTile(position.a, position.b),
			position.b+0.5f
			);
	}

	/// <summary>
	/// Gets the highest height on the given area.
	/// </summary>
	/// <returns>The highest height.</returns>
	/// <param name="startX">Start x.</param>
	/// <param name="startZ">Start z.</param>
	/// <param name="sizeX">Size x.</param>
	/// <param name="sizeZ">Size z.</param>
	public float getMaxHeight(int startX, int startZ, int sizeX, int sizeZ) {
		float maxHeight = float.MinValue;
		for (int i = startX; i < startX + sizeX; i++) {
			for (int j = startZ; j < startZ + sizeZ; j++) {
				if (maxHeight < vertexMap [i * (gridSizeX + 1) + j].height)
					maxHeight = vertexMap [i * (gridSizeX + 1) + j].height;
			}
		}
		return maxHeight;
	}

	/// <summary>
	/// Gets the lowest height on the given area.
	/// </summary>
	/// <returns>The lowest height.</returns>
	/// <param name="startX">Start x.</param>
	/// <param name="startZ">Start z.</param>
	/// <param name="sizeX">Size x.</param>
	/// <param name="sizeZ">Size z.</param>
	public float getMinHeight(int startX, int startZ, int sizeX, int sizeZ) {
		float minHeight = float.MaxValue;
		for (int i = startX; i < startX + sizeX; i++) {
			for (int j = startZ; j < startZ + sizeZ; j++) {
				if (minHeight > vertexMap [i * (gridSizeX + 1) + j].height)
					minHeight = vertexMap [i * (gridSizeX + 1) + j].height;
			}
		}
		return minHeight;
	}

	/// <summary>
	/// Gets the median height of the given tile.
	/// </summary>
	/// <returns>The median height on tile.</returns>
	/// <param name="tileX">Tile x position.</param>
	/// <param name="tileZ">Tile z position.</param>
	public float GetMedianHeightOnTile(int tileX, int tileZ) {
		return (
			vertexMap[((tileX) * (gridSizeX+1)) + (tileZ)].height +
			vertexMap[((tileX) * (gridSizeX+1)) + (tileZ+1)].height +
			vertexMap[((tileX+1) * (gridSizeX+1)) + (tileZ)].height +
			vertexMap[((tileX+1) * (gridSizeX+1)) + (tileZ+1)].height
		) * 0.25f;
	}

	/// <summary>
	/// Gets the exact height on this position (considering weights inside the tile).
	/// </summary>
	/// <returns>The exact height.</returns>
	/// <param name="position">World position (Y is not considered).</param>
	public float GetExactHeight(Vector3 position) {
		// TODO actually make the calculation
		Vector2i gridPos = getTilePositionFromWorldPosition(position);
		return GetMedianHeightOnTile(gridPos.a, gridPos.b) + 0.04f;
	}

	public float getMedianLevelOnTile(int tileX, int tileZ) {
		return ((
			vertexMap[((tileX) * (gridSizeX+1)) + (tileZ)].level +
			vertexMap[((tileX) * (gridSizeX+1)) + (tileZ+1)].level +
			vertexMap[((tileX+1) * (gridSizeX+1)) + (tileZ)].level +
			vertexMap[((tileX+1) * (gridSizeX+1)) + (tileZ+1)].level
		) * 0.25f) - GridMap.LEVEL_N2;
	}

	/// <summary>
	/// Determines whether the given area is clear (eg for be built on).
	/// </summary>
	/// <returns><c>true</c> if this area is clear; otherwise, <c>false</c>.</returns>
	/// <param name="startX">Start x.</param>
	/// <param name="startZ">Start z.</param>
	/// <param name="sizeX">Size x.</param>
	/// <param name="sizeZ">Size z.</param>
	public bool IsAreaClear(int startX, int startZ, int sizeX, int sizeZ) {
		int i = startX, j = startZ;
		while (i < startX + sizeX) {
			j = startZ;
			while (j < startZ + sizeZ) {
				if (!tileMap [i * gridSizeX + j].isWalkable) {
					return false;
				}
				j++;
			}
			i++;
		}
		return true;
	}

	/// <summary>
	/// Configures the given area as having a construction built. Assumes the area is buildable.
	/// </summary>
	/// <param name="startX">Start x.</param>
	/// <param name="startZ">Start z.</param>
	/// <param name="sizeX">Size x.</param>
	/// <param name="sizeZ">Size z.</param>
	public void BuildOnArea(int startX, int startZ, int sizeX, int sizeZ) {
		for (int i = startX; i < startX + sizeX; i++) {
			for (int j = startZ; j < startZ + sizeZ; j++) {
				tileMap [i * gridSizeX + j].builtOn ();
			}
		}
	}
}

/// <summary>
/// Tile structure.
/// A tile is any single cell in the terrain.
/// </summary>
[System.Serializable]
public class Tile : IHeapItem<Tile> {
	public bool isEmpty;	// no large objects that prevent this tile from being walkable (eg tree, rock, etc)
	public bool isBlocked;	// if tile is blocked by another source (eg by definition, or dynamic object)
	public bool isWalkable {
		get {
			return (isEmpty && !isBlocked);
		}
	}

	public GroundType ground;
	public byte areaIndex;
	public byte obscurance;

	public Vector2i gridPosition;

	public byte waterLevel;

	// temporary values - used on a single path finding call (only 1 is made at a time)
	[System.NonSerialized] public int gCost, hCost;
	[System.NonSerialized] public Tile parent;
	[System.NonSerialized] private int heapIndex;

	public Tile(int x, int z) {
		gridPosition.a = x;
		gridPosition.b = z;
		isEmpty = true;
		isBlocked = false;
	}

	public void copyFrom(Tile tile) {
		isEmpty = tile.isEmpty;
		isBlocked = tile.isBlocked;
		ground = tile.ground;
		areaIndex = tile.areaIndex;
		obscurance = tile.obscurance;
	}

	public void builtOn() {
		isEmpty = false;
	}

	public int fCost {
		get {
			return gCost + hCost;
		}
	}

	public int HeapIndex {
		get {
			return heapIndex;
		}
		set {
			heapIndex = value;
		}
	}

	public int CompareTo(Tile tileToCompare) {
		int compare = fCost.CompareTo(tileToCompare.fCost);
		if (compare == 0) {
			compare = hCost.CompareTo(tileToCompare.hCost);
		}
		return -compare;
	}
}

[System.Serializable]
public struct TerrainVertex {
	public float height;
	public byte level;

	public void copyFrom(TerrainVertex terrainVertex) {
		height = terrainVertex.height;
		level = terrainVertex.level;
	}
}

[System.Serializable]
public struct Area {
	[SerializeField] public string name;
	[SerializeField] public float perlinScale, perlinWeight;
	[SerializeField] public float[] color_c;
	[SerializeField] public SpawnerDef[] spawners;

	public void Init() {
		name = "Unnamed area";
		perlinScale = 0.2f;
		perlinWeight = 0;

		color_c = new float[3];
		color_c [0] = Random.Range (0.0f, 1.0f);
		color_c [1] = Random.Range (0.0f, 1.0f);
		color_c [2] = Random.Range (0.0f, 1.0f);
	}

	public Color color {
		get {
			return new Color (color_c [0], color_c [1], color_c [2]);
		}
		set {
			color_c [0] = value.r;
			color_c [1] = value.g;
			color_c [2] = value.b;
		}
	}

	[System.Serializable]
	public struct SpawnerDef {
		public const string DEFAULT_SPAWNER_LOCATION = "Assets/Prefabs/Map/Spawners/";

		// Name of the spawner prefab that must exist on the folder DEFAULT_SPAWNER_LOCATION
		[SerializeField] public string spawner;
		[Range (0, 100)] [SerializeField] public float distribution;  // in percentage

		// NOT saved on file, this is the cached prefab reference to the spawner, used to spawn stuff
		public Spawner spawnerPrefab {
			get {
                #if UNITY_EDITOR
                if (_spawnerPrefab == null) {
					GameObject gObject = AssetDatabase.LoadAssetAtPath<GameObject> (DEFAULT_SPAWNER_LOCATION + spawner + ".prefab");
					if (gObject != null)
						_spawnerPrefab = gObject.GetComponent<Spawner> ();
					else
						Debug.LogError ("Spawner not found: '" + spawner + ".prefab' at " + DEFAULT_SPAWNER_LOCATION);
				}
				return _spawnerPrefab;
                #endif
                return null;
            }
		}
		[System.NonSerialized] private Spawner _spawnerPrefab;

		public void DoSpawn(ChunkMap chunkMap, int x, int z, ChunkModeler chunkModeler) {
			spawnerPrefab.DoSpawn (chunkMap, x, z, chunkModeler, distribution * 0.01f);
		}
	}
}

/// <summary>
/// Ground type enum.
/// </summary>
public enum GroundType {
	Grass=0,
	Dirt=1,
	Sand=2,
	Stone=3
}
