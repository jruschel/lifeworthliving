﻿using UnityEngine;

/// <summary>
/// Construction interface - anything that can be built.
/// </summary>
public interface IConstruction {
	/// <summary>
	/// The size that this construction occupies.
	/// </summary>
	/// <returns>The size.</returns>
	Vector2i GetSize();

	/// <summary>
	/// The cost for building this construction.
	/// </summary>
	/// <returns>The build cost.</returns>
	ResourcesCount GetBuildCost();

	/// <summary>
	/// Sets this construction as preview mode.
	/// </summary>
	void SetToPreview();

	/// <summary>
	/// Sets this construction to built mode.
	/// </summary>
	void SetToBuilt();
}