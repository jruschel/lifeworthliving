﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[RequireComponent(typeof(MapController))]
public class MapPathfinder : MonoBehaviour {

	Queue<PathRequest> pathRequestQueue = new Queue<PathRequest>();
	PathRequest currentPathRequest;
	bool isProcessingPath;

	Heap<Tile> tempOpenSet;
	HashSet<Tile> tempClosedSet;

	MapController mapController;

	static MapPathfinder Instance;

	void Awake() {
		Instance = this;
	}

	public void init (MapController _mapController) {
		mapController = _mapController;

		tempOpenSet = new Heap<Tile>(mapController.gridMap.sizeX * mapController.gridMap.sizeZ * 
			GridMap.CHUNK_SIZE * GridMap.CHUNK_SIZE);
		tempClosedSet = new HashSet<Tile>();
	}

	#region Path finding

	private void StartFindPath(Vector2i startPos, Vector2i targetPos) {
		StartCoroutine(FindPath(startPos,targetPos));
	}

	IEnumerator FindPath(Vector2i startPos, Vector2i targetPos) {

		Vector2i[] waypoints = new Vector2i[0];
		bool pathSuccess = false;

		Tile startNode = mapController.chunkMap.getTileFromGridPosition (startPos);
		Tile targetNode = mapController.chunkMap.getTileFromGridPosition(targetPos);


		if (startNode.isWalkable && targetNode.isWalkable) {
			tempOpenSet.Clear ();
			tempClosedSet.Clear ();
			tempOpenSet.Add(startNode);

			while (tempOpenSet.Count > 0) {
				Tile currentNode = tempOpenSet.RemoveFirst();
				tempClosedSet.Add(currentNode);

				if (currentNode == targetNode) {
					pathSuccess = true;
					break;
				}

				foreach (Tile neighbour in mapController.chunkMap.GetNeighbours(currentNode)) {
					if (!neighbour.isWalkable || tempClosedSet.Contains(neighbour) ||
						Mathf.Abs(mapController.chunkMap.GetMedianHeightOnTile(currentNode.gridPosition.a, currentNode.gridPosition.b) - 
							mapController.chunkMap.GetMedianHeightOnTile(neighbour.gridPosition.a, neighbour.gridPosition.b)) > 0.1f) {
						continue;
					}

					int newMovementCostToNeighbour = currentNode.gCost + GetDistance(currentNode, neighbour);
					if (newMovementCostToNeighbour < neighbour.gCost || !tempOpenSet.Contains(neighbour)) {
						neighbour.gCost = newMovementCostToNeighbour;
						neighbour.hCost = GetDistance(neighbour, targetNode);
						neighbour.parent = currentNode;

						if (!tempOpenSet.Contains (neighbour)) {
							tempOpenSet.Add (neighbour);
						}
					}
				}
			}
		}
		yield return null;
		if (pathSuccess) {
			waypoints = RetracePath(startNode, targetNode);
		}
		FinishedProcessingPath(waypoints, pathSuccess);

	}

	Vector2i[] RetracePath(Tile startNode, Tile endNode) {
		List<Tile> path = new List<Tile>();
		Tile currentNode = endNode;

		while (currentNode != startNode) {
			path.Add(currentNode);
			currentNode = currentNode.parent;
		}
		Vector2i[] waypoints = SimplifyPath(path);
		Array.Reverse(waypoints);
		return waypoints;

	}

	Vector2i[] SimplifyPath(List<Tile> path) {
		List<Vector2i> waypoints = new List<Vector2i>();
		Vector2 directionOld = Vector2.zero;

		for (int i = 1; i < path.Count; i ++) {
			Vector2 directionNew = new Vector2(path[i-1].gridPosition.a - path[i].gridPosition.a,
				path[i-1].gridPosition.b - path[i].gridPosition.b);
			if (directionNew != directionOld) {
				waypoints.Add(path[i].gridPosition);
			}
			directionOld = directionNew;
		}
		return waypoints.ToArray();
	}

	int GetDistance(Tile nodeA, Tile nodeB) {
		int dstX = Mathf.Abs(nodeA.gridPosition.a - nodeB.gridPosition.a);
		int dstY = Mathf.Abs(nodeA.gridPosition.b - nodeB.gridPosition.b);

		if (dstX > dstY)
			return 14*dstY + 10* (dstX-dstY);
		return 14*dstX + 10 * (dstY-dstX);
	}

	#endregion

	#region Request Manager

	public static void RequestPath(Vector2i pathStart, Vector2i pathEnd, Action<Vector2i[], bool> callback) {
		PathRequest newRequest = new PathRequest(pathStart,pathEnd,callback);
		Instance.pathRequestQueue.Enqueue(newRequest);
		Instance.TryProcessNext();
	}

	void TryProcessNext() {
		if (!isProcessingPath && pathRequestQueue.Count > 0) {
			currentPathRequest = pathRequestQueue.Dequeue();
			isProcessingPath = true;
			StartFindPath(currentPathRequest.pathStart, currentPathRequest.pathEnd);
		}
	}

	void FinishedProcessingPath(Vector2i[] path, bool success) {
		currentPathRequest.callback(path,success);
		isProcessingPath = false;
		TryProcessNext();
	}

	struct PathRequest {
		public Vector2i pathStart;
		public Vector2i pathEnd;
		public Action<Vector2i[], bool> callback;

		public PathRequest(Vector2i _start, Vector2i _end, Action<Vector2i[], bool> _callback) {
			pathStart = _start;
			pathEnd = _end;
			callback = _callback;
		}
	}

	#endregion
}
