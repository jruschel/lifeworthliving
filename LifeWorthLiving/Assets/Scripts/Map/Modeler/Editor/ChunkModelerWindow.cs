﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class ChunkModelerWindow : EditorWindow {
	public static ChunkModelerWindow Instance;

	[SerializeField] MapModeler mapDeModeler;
	[SerializeField] ChunkModeler chunkModeler;
	[SerializeField] bool isValid = false;

	public void Init(MapModeler _mapDeModeler) {
		mapDeModeler = _mapDeModeler;
		this.titleContent.text = "Modeling";
	}

	void OnGUI() {
		if (isValid && mapDeModeler != null) {
			EditorGUILayout.BeginVertical ();
			EditorGUILayout.LabelField ("How to use", EditorStyles.boldLabel);
			EditorGUILayout.LabelField ("Use 'A' to trigger painting.");
			EditorGUILayout.LabelField ("Use 'Z' to undo.");
			EditorGUILayout.LabelField ("Now editing " + chunkModeler.name + ".");
			EditorGUILayout.EndVertical ();

			EditorGUILayout.Separator ();

			EditorGUILayout.BeginVertical ();
			EditorGUILayout.LabelField ("Brush, Pen and View", EditorStyles.boldLabel);
			mapDeModeler.modelMode = (MapModeler.ModelMode)EditorGUILayout.EnumPopup ("Model Mode", mapDeModeler.modelMode);
			mapDeModeler.brushRadius = EditorGUILayout.IntSlider ("Brush Radius", mapDeModeler.brushRadius, 1, 32);
			EditorGUILayout.EndVertical ();

			EditorGUILayout.Space ();

			EditorGUILayout.BeginVertical ();
			EditorGUILayout.LabelField ("Apply Tools", EditorStyles.boldLabel);

			// Level
			EditorGUILayout.BeginVertical ();
			EditorGUILayout.BeginHorizontal ();
			mapDeModeler.paintLevel = EditorGUILayout.Foldout (mapDeModeler.paintLevel, "Level");
			EditorGUILayout.Space ();
			mapDeModeler.paintLevel = EditorGUILayout.Toggle (mapDeModeler.paintLevel);
			EditorGUILayout.EndHorizontal ();
			if (mapDeModeler.paintLevel) {
				mapDeModeler.paintLevelValue = (byte)(EditorGUILayout.IntSlider ("Value", mapDeModeler.paintLevelValue-GridMap.LEVEL_N2, -GridMap.LEVEL_N2, GridMap.LEVEL_N2)+GridMap.LEVEL_N2);
			}
			EditorGUILayout.EndVertical ();

			// Obstruction
			EditorGUILayout.BeginVertical ();
			EditorGUILayout.BeginHorizontal ();
			mapDeModeler.paintObstruction = EditorGUILayout.Foldout (mapDeModeler.paintObstruction, "Obstruction");
			EditorGUILayout.Space ();
			mapDeModeler.paintObstruction = EditorGUILayout.Toggle (mapDeModeler.paintObstruction);
			EditorGUILayout.EndHorizontal ();
			if (mapDeModeler.paintObstruction) {
				
				mapDeModeler.paintObstructionValue = EditorGUILayout.Popup ("Obstruct",
					(mapDeModeler.paintObstructionValue)?0:1, mapDeModeler.paintObstructionOptions) == 0;
			}
			EditorGUILayout.EndVertical ();

			// Ground (tile color)
			EditorGUILayout.BeginVertical ();
			EditorGUILayout.BeginHorizontal ();
			mapDeModeler.paintGround = EditorGUILayout.Foldout (mapDeModeler.paintGround, "Ground");
			EditorGUILayout.Space ();
			mapDeModeler.paintGround = EditorGUILayout.Toggle (mapDeModeler.paintGround);
			EditorGUILayout.EndHorizontal ();
			if (mapDeModeler.paintGround) {
				mapDeModeler.paintGroundType = (GroundType)EditorGUILayout.EnumPopup ("Ground Type", mapDeModeler.paintGroundType);
			}
			EditorGUILayout.EndVertical ();

			// Area
			EditorGUILayout.BeginVertical ();
			EditorGUILayout.BeginHorizontal ();
			mapDeModeler.paintArea = EditorGUILayout.Foldout (mapDeModeler.paintArea, "Area");
			EditorGUILayout.Space ();
			mapDeModeler.paintArea = EditorGUILayout.Toggle (mapDeModeler.paintArea);
			EditorGUILayout.EndHorizontal ();
			if (mapDeModeler.paintArea) {
				mapDeModeler.paintAreaIndex = (byte)EditorGUILayout.Popup("Value", mapDeModeler.paintAreaIndex, mapDeModeler.gridMap.areaNames);
			}
			EditorGUILayout.EndVertical ();

			// Water
			EditorGUILayout.BeginVertical ();
			EditorGUILayout.BeginHorizontal ();
			mapDeModeler.paintWater = EditorGUILayout.Foldout (mapDeModeler.paintWater, "Water");
			EditorGUILayout.Space ();
			mapDeModeler.paintWater = EditorGUILayout.Toggle (mapDeModeler.paintWater);
			EditorGUILayout.EndHorizontal ();
			if (mapDeModeler.paintWater) {
				mapDeModeler.paintWaterLevel = (byte)EditorGUILayout.IntSlider ("Level", mapDeModeler.paintWaterLevel, 0, GridMap.LEVEL_WATER_MAX);
			}
			EditorGUILayout.EndVertical ();

			// Obsurance
			EditorGUILayout.BeginVertical ();
			EditorGUILayout.BeginHorizontal ();
			mapDeModeler.paintObscurance = EditorGUILayout.Foldout (mapDeModeler.paintObscurance, "Obscurance");
			EditorGUILayout.Space ();
			mapDeModeler.paintObscurance = EditorGUILayout.Toggle (mapDeModeler.paintObscurance);
			EditorGUILayout.EndHorizontal ();
			if (mapDeModeler.paintObscurance) {
				mapDeModeler.paintObscuranceValue = (byte)EditorGUILayout.IntSlider ("Value", mapDeModeler.paintObscuranceValue, 0, GridMap.MAX_OBSCURANCE);
			}
			EditorGUILayout.EndVertical ();

			EditorGUILayout.EndVertical ();

			EditorGUILayout.Space ();

			EditorGUILayout.BeginVertical ();
			EditorGUILayout.LabelField ("Filter", EditorStyles.boldLabel);

			// Area
			EditorGUILayout.BeginVertical ();
			EditorGUILayout.BeginHorizontal ();
			mapDeModeler.filterByArea = EditorGUILayout.Foldout (mapDeModeler.filterByArea, "by Area");
			EditorGUILayout.Space ();
			mapDeModeler.filterByArea = EditorGUILayout.Toggle (mapDeModeler.filterByArea);
			EditorGUILayout.EndHorizontal ();
			if (mapDeModeler.filterByArea) {
				EditorGUILayout.BeginHorizontal ();
				mapDeModeler.filterByAreaMode = (MapModeler.FilterMode)EditorGUILayout.EnumPopup (mapDeModeler.filterByAreaMode);
				mapDeModeler.filterByAreaValue = (byte)EditorGUILayout.Popup(mapDeModeler.filterByAreaValue, mapDeModeler.gridMap.areaNames);
				EditorGUILayout.EndHorizontal ();
			}
			EditorGUILayout.EndVertical ();

			EditorGUILayout.EndVertical ();

			EditorGUILayout.Separator ();

			EditorGUILayout.BeginVertical ();

			EditorGUILayout.LabelField ("View Modes", EditorStyles.boldLabel);
			mapDeModeler.viewLevels = EditorGUILayout.Toggle ("Levels", mapDeModeler.viewLevels);
			mapDeModeler.viewObstruction = EditorGUILayout.Toggle ("Obstruction", mapDeModeler.viewObstruction);
			mapDeModeler.viewTileColors = EditorGUILayout.Toggle ("Tile Colors", mapDeModeler.viewTileColors);
			mapDeModeler.viewAreas = EditorGUILayout.Toggle ("Areas", mapDeModeler.viewAreas);
			mapDeModeler.viewWater = EditorGUILayout.Toggle ("Water", mapDeModeler.viewWater);
			mapDeModeler.viewObscurance = EditorGUILayout.Toggle ("Obscurance", mapDeModeler.viewObscurance);
			EditorGUILayout.BeginHorizontal ();
			if (GUILayout.Button("Refresh Chunk")) {
				chunkModeler.updateAllPreviewTexture ();
			}
			if (GUILayout.Button("Refresh ALL")) {
				chunkModeler.mapDeModeler.updateAllPreviewTextures ();
			}
			EditorGUILayout.EndHorizontal ();
			EditorGUILayout.EndVertical ();
		} else {
			EditorGUILayout.BeginVertical ();
			EditorGUILayout.LabelField ("No selected chunk!", EditorStyles.boldLabel);
			EditorGUILayout.LabelField ("Select a chunk to edit.");
			EditorGUILayout.EndVertical ();
		}
	}

	public static void EnableTo(ChunkModeler _chunkModeler) {
		if (Instance != null) {
			Instance.chunkModeler = _chunkModeler;
			Instance.isValid = true;
			Instance.Repaint ();
		}
	}

	public static void Disable() {
		if (Instance != null) {
			Instance.chunkModeler = null;
			Instance.isValid = false;
			Instance.Repaint ();
		}
	}

	void OnEnable() {
		Instance = this;
	}

	void OnDisable() {
		if (Instance == this)
			Instance = null;
	}
}
