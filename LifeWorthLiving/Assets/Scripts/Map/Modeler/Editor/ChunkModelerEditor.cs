﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor (typeof (ChunkModeler))]
[CanEditMultipleObjects]
public class ChunkModelerEditor : Editor {
	[SerializeField] ChunkModeler chunkModeler;
	[SerializeField] Tool lastTool;

	public void OnEnable() {
		chunkModeler = (ChunkModeler)target;

		ChunkModelerWindow.EnableTo (chunkModeler);

		lastTool = Tools.current;
		Tools.current = Tool.None;
	}

	public void OnDisable() {
		ChunkModelerWindow.Disable ();

		Tools.current = lastTool;
	}

	public override void OnInspectorGUI () {
		GUILayout.BeginVertical ();
		GUILayout.TextArea ("To use: \n" +
			"Use 'Open Modeling Window' when fresh starting the scene, and make sure that the window correctly updates the currently selected chunk. \n" +
			"'Save Chunk Grid' will save the grid map (Resouces/Chunks). \n" +
			"'Build and Save Terrain Mesh' will generate and save the terrain mesh (Resources/Terrain). \n" +
			"'Generate and Save Procedural Objects' will generate procedural content and save as prefab (Resources/Procedural). \n" +
			"'Save Dynamic Objects' will save the GameObject 'Dynamic objects' as a prefab (Resources/Dynamic). Use this to place custom stuff. \n" +
			"'Build Save All' will do all of the actions above. \n" +
			"'Load All' will load from Resources the Grid, Terrain, Procedural and Dynamic objects.");
		GUILayout.EndVertical ();

		GUILayout.BeginVertical ();
		if (targets.Length == 1) {
			if (GUILayout.Button ("Open Modeling Window")) {
				ChunkModelerWindow toolsWindow = (ChunkModelerWindow)EditorWindow.GetWindow (typeof(ChunkModelerWindow));
				toolsWindow.Init (chunkModeler.mapDeModeler);
				ChunkModelerWindow.EnableTo (chunkModeler);
			}
		}
		if (GUILayout.Button("Save Chunk Grid")) {
			foreach (Object o in targets) {
				(o as ChunkModeler).saveGrid ();
			}
		}
		if (GUILayout.Button("Build and Save Terrain Mesh")) {
			foreach (Object o in targets) {
				(o as ChunkModeler).buildSaveTerrainMesh ();
			}
		}
		GUILayout.BeginHorizontal ();
		GUILayout.Label ("Procedural Objects");
		if (GUILayout.Button("Generate")) {
			foreach (Object o in targets) {
				(o as ChunkModeler).generateProceduralObjects ();
				(o as ChunkModeler).saveGrid ();
			}
		}
		if (GUILayout.Button("Save")) {
			foreach (Object o in targets) {
				(o as ChunkModeler).saveProceduralObjects ();
				(o as ChunkModeler).saveGrid ();
			}
		}
		if (GUILayout.Button("Reset")) {
			foreach (Object o in targets) {
				(o as ChunkModeler).resetProceduralObjects ();
				(o as ChunkModeler).saveGrid ();
			}
		}
		GUILayout.EndHorizontal ();

		/*if (GUILayout.Button("Generate and Save Procedural Objects")) {
			chunkModeler.generateSaveProceduralObjects ();
			foreach (Object o in targets) {
				(o as ChunkModeler).saveGrid ();
			}
		}*/

		if (GUILayout.Button("Save Dynamic Objects")) {
			foreach (Object o in targets) {
				(o as ChunkModeler).saveDynamicObjects ();
			}
		}
		GUILayout.BeginHorizontal ();
		if (GUILayout.Button("Load All")) {
			if (EditorUtility.DisplayDialog ("Loading All", "Loading the chunk will overwrite any changes made. Continue?", "Yes", "No")) {
				foreach (Object o in targets) {
					(o as ChunkModeler).loadAll ();
				}
			}
		}
		if (GUILayout.Button("Build Save All")) {
			foreach (Object o in targets) {
				(o as ChunkModeler).buildSaveAll ();
			}
		}
		GUILayout.EndHorizontal ();
		GUILayout.BeginHorizontal ();
		if (GUILayout.Button("Enable Edit")) {
			foreach (Object o in targets) {
				(o as ChunkModeler).enableEdit ();
			}
		}
		if (GUILayout.Button("Disable Edit")) {
			foreach (Object o in targets) {
				(o as ChunkModeler).disableEdit ();
			}
		}
		GUILayout.EndHorizontal ();
		GUILayout.EndVertical ();
	}

	void OnSceneGUI() {
		// draw brush circle
		switch (chunkModeler.mapDeModeler.modelMode) {
		case MapModeler.ModelMode.HeightMap:
			UnityEditor.Handles.DrawWireDisc (
				new Vector3 (
					chunkModeler.mapDeModeler.currentMouseTile.a + chunkModeler.transform.localPosition.x, 
					0,
					//mapModeler.gridMap.GetMedianHeightOnTile (mapModeler.currentMouseTile.a, mapModeler.currentMouseTile.b), 
					chunkModeler.mapDeModeler.currentMouseTile.b + chunkModeler.transform.localPosition.z), 
				Vector3.up, chunkModeler.mapDeModeler.brushRadius);
			break;
		case MapModeler.ModelMode.TileMap:
		case MapModeler.ModelMode.TiledHeightMap:
			UnityEditor.Handles.DrawWireDisc (
				new Vector3 (
					chunkModeler.mapDeModeler.currentMouseTile.a+0.5f + chunkModeler.transform.localPosition.x, 
					0,
					//mapModeler.gridMap.GetMedianHeightOnTile (mapModeler.currentMouseTile.a, mapModeler.currentMouseTile.b), 
					chunkModeler.mapDeModeler.currentMouseTile.b+0.5f + chunkModeler.transform.localPosition.z), 
				Vector3.up, chunkModeler.mapDeModeler.brushRadius);
			break;
		}

		// Information on the current tile/vertex
		UnityEditor.Handles.color = Color.white;
		if (chunkModeler.mapDeModeler.modelMode == MapModeler.ModelMode.TileMap || chunkModeler.mapDeModeler.modelMode == MapModeler.ModelMode.TiledHeightMap) {
			if (chunkModeler.mapDeModeler.currentMouseTile.a >= 0 && chunkModeler.mapDeModeler.currentMouseTile.b >= 0 &&
			   chunkModeler.mapDeModeler.currentMouseTile.a < GridMap.CHUNK_SIZE && chunkModeler.mapDeModeler.currentMouseTile.b < GridMap.CHUNK_SIZE) {
				UnityEditor.Handles.BeginGUI ();
				GUILayout.BeginArea (new Rect (
					5, 5, 150, 400
				));

				GUILayout.BeginVertical (EditorStyles.helpBox);

				GUILayout.Label (chunkModeler.mapDeModeler.currentMouseTile.a + ", " + chunkModeler.mapDeModeler.currentMouseTile.b);

				if (chunkModeler.mapDeModeler.viewLevels)
					GUILayout.Label (" Level : " + chunkModeler.chunkMap.getMedianLevelOnTile (chunkModeler.mapDeModeler.currentMouseTile.a, chunkModeler.mapDeModeler.currentMouseTile.b));

				if (chunkModeler.mapDeModeler.viewObstruction)
					GUILayout.Label (" Walk : " + chunkModeler.chunkMap.tileMap [chunkModeler.mapDeModeler.currentMouseTile.a * GridMap.CHUNK_SIZE + chunkModeler.mapDeModeler.currentMouseTile.b].isWalkable);

				if (chunkModeler.mapDeModeler.viewTileColors)
					GUILayout.Label (" Tile : " + chunkModeler.chunkMap.tileMap [chunkModeler.mapDeModeler.currentMouseTile.a * GridMap.CHUNK_SIZE + chunkModeler.mapDeModeler.currentMouseTile.b].ground);

				if (chunkModeler.mapDeModeler.viewAreas)
					GUILayout.Label (" Area : " + chunkModeler.mapDeModeler.gridMap.areas [
						chunkModeler.chunkMap.tileMap [(chunkModeler.mapDeModeler.currentMouseTile.a) * GridMap.CHUNK_SIZE + (chunkModeler.mapDeModeler.currentMouseTile.b)].areaIndex].name);

				if (chunkModeler.mapDeModeler.viewWater)
					GUILayout.Label (" Water : " + chunkModeler.chunkMap.tileMap [chunkModeler.mapDeModeler.currentMouseTile.a * GridMap.CHUNK_SIZE + chunkModeler.mapDeModeler.currentMouseTile.b].waterLevel);

				if (chunkModeler.mapDeModeler.viewObscurance)
					GUILayout.Label (" Obsc : " + chunkModeler.chunkMap.tileMap [chunkModeler.mapDeModeler.currentMouseTile.a * GridMap.CHUNK_SIZE + chunkModeler.mapDeModeler.currentMouseTile.b].obscurance);

				GUILayout.EndVertical ();

				GUILayout.EndArea ();
				UnityEditor.Handles.EndGUI ();
			}
		}
		if (chunkModeler.mapDeModeler.modelMode == MapModeler.ModelMode.HeightMap) {
			if (chunkModeler.mapDeModeler.currentMouseTile.a >= 0 && chunkModeler.mapDeModeler.currentMouseTile.b >= 0 &&
				chunkModeler.mapDeModeler.currentMouseTile.a <= GridMap.CHUNK_SIZE && chunkModeler.mapDeModeler.currentMouseTile.b <= GridMap.CHUNK_SIZE) {
				UnityEditor.Handles.BeginGUI ();
				GUILayout.BeginArea (new Rect (
					5, 5, 150, 400
				));

				GUILayout.BeginVertical (EditorStyles.helpBox);

				GUILayout.Label (chunkModeler.mapDeModeler.currentMouseTile.a + ", " + chunkModeler.mapDeModeler.currentMouseTile.b);

				GUILayout.Label (" Level : " + (chunkModeler.chunkMap.vertexMap [chunkModeler.mapDeModeler.currentMouseTile.a * (GridMap.CHUNK_SIZE+1) + chunkModeler.mapDeModeler.currentMouseTile.b].level-GridMap.LEVEL_N2));

				GUILayout.EndVertical ();

				GUILayout.EndArea ();
				UnityEditor.Handles.EndGUI ();
			}
		}

		// input
		Event e = Event.current;
		if (e.isMouse && e.button != 2) {
			chunkModeler.mapDeModeler.currentMousePosition = e.mousePosition;

			float dist;
			Plane xz = new Plane (new Vector3 (0, 1, 0), 0f);
			Ray ray = HandleUtility.GUIPointToWorldRay (chunkModeler.mapDeModeler.currentMousePosition);
			xz.Raycast (ray, out dist);
			Vector3 pos = ray.GetPoint (dist);
			pos.x -= chunkModeler.transform.localPosition.x;
			pos.z -= chunkModeler.transform.localPosition.z;
			switch (chunkModeler.mapDeModeler.modelMode) {
			case MapModeler.ModelMode.HeightMap:
				chunkModeler.mapDeModeler.currentMouseTile.a = Mathf.RoundToInt (pos.x / GridMap.TILE_SIZE);
				chunkModeler.mapDeModeler.currentMouseTile.b = Mathf.RoundToInt (pos.z / GridMap.TILE_SIZE);
				break;
			case MapModeler.ModelMode.TileMap:
			case MapModeler.ModelMode.TiledHeightMap:
				chunkModeler.mapDeModeler.currentMouseTile.a = Mathf.RoundToInt ((pos.x - (GridMap.TILE_SIZE * 0.5f)) / GridMap.TILE_SIZE);
				chunkModeler.mapDeModeler.currentMouseTile.b = Mathf.RoundToInt ((pos.z - (GridMap.TILE_SIZE * 0.5f)) / GridMap.TILE_SIZE);
				break;
			}

			if (chunkModeler.mapDeModeler.doToolApply) {
				chunkModeler.applyCurrentTool ();
			}
		}

		if (e.isKey) {
			switch(e.keyCode){
			case KeyCode.A:
				if (e.type == EventType.KeyDown || e.type == EventType.keyDown) {
					chunkModeler.mapDeModeler.doToolApply = true;
					chunkModeler.applyCurrentTool ();
				}
				if (e.type == EventType.KeyUp || e.type == EventType.keyUp) {
					chunkModeler.mapDeModeler.doToolApply = false;
				}
				Event.current.Use ();
				break;
			case KeyCode.Z:
				if (e.type == EventType.KeyUp || e.type == EventType.keyUp) {
					// TODO
					Debug.LogError ("Undo not implemented.");
				}
				Event.current.Use ();
				break;
			}
		}
	}
}
