﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor (typeof (MapModeler))]
public class MapModelerEditor : Editor {
	MapModeler mapDeModeler;

	public void OnEnable() {
		mapDeModeler = (MapModeler)target;
	}

	public override void OnInspectorGUI() {
		GUILayout.BeginVertical ();
		GUILayout.TextArea ("To use: \n" +
			"Use 'Load Chunk Grids' to load basic chunk info (gridMap) from Resources, and 'Save Chunk Grids' to save the gridmap on Resources (Resorces/Chunks). \n" +
			"'Build and Save All' will rebuild the terrain, place procedural content, create dynamic prefabs, and save this information on Resources. After this, the files on Resources will be ready to be loaded in-game. \n" +
			"'Disable Edit' will disable edit mode on all chunks, making the scene lighter and better to visualize.");
		GUILayout.EndVertical ();

		GUILayout.BeginVertical ();
		GUILayout.Label ("Map Building", EditorStyles.boldLabel);
		mapDeModeler.chunkDimension = EditorGUILayout.IntField ("Chunk Dimension", mapDeModeler.chunkDimension);
		GUILayout.BeginHorizontal ();
		if (GUILayout.Button("Create Blank")) {
			if (EditorUtility.DisplayDialog ("Regenerating Map", "Creating a blank map will erase everything. Continue?", "Yes", "No")) {
				mapDeModeler.createBlank ();
			}
		}
		if (GUILayout.Button("Clear")) {
			if (EditorUtility.DisplayDialog ("Clearing Map", "Clearing the map will erase everything. Continue?", "Yes", "No")) {
				mapDeModeler.clear ();
			}
		}
		GUILayout.EndHorizontal ();

		GUILayout.Space (8);

		GUILayout.BeginHorizontal ();
		if (GUILayout.Button("Load Meta Grid")) {
			if (EditorUtility.DisplayDialog ("Loading Meta Grid", "Loading the map will overwrite any loaded map. Continue?", "Yes", "No")) {
				mapDeModeler.loadMetaGrid ();
			}
		}
		if (GUILayout.Button("Save Meta Grid")) {
			if (EditorUtility.DisplayDialog ("Saving Meta Grid", "This will overwrite previous saves. Continue?", "Yes", "No")) {
				mapDeModeler.saveMetaGrid ();
			}
		}
		GUILayout.EndHorizontal ();
		GUILayout.BeginHorizontal ();
		if (GUILayout.Button("Load Chunk Grids")) {
			if (EditorUtility.DisplayDialog ("Loading Grids", "Loading the map will overwrite any loaded map. Continue?", "Yes", "No")) {
				mapDeModeler.loadGrid ();
			}
		}
		if (GUILayout.Button("Save Chunk Grids")) {
			if (EditorUtility.DisplayDialog ("Saving Grids", "This will overwrite previous saves. Continue?", "Yes", "No")) {
				mapDeModeler.saveGrid ();
			}
		}
		GUILayout.EndHorizontal ();
		GUILayout.BeginHorizontal ();
		if (GUILayout.Button("Load All")) {
			if (EditorUtility.DisplayDialog ("Loading all chunks", "Loading the map will erase any changes made and take a while. Continue?", "Yes", "No")) {
				mapDeModeler.loadAll ();
			}
		}
		if (GUILayout.Button("Build and Save All")) {
			if (EditorUtility.DisplayDialog ("Builing all chunks", "Building the map will erase previous build and take a while. Continue?", "Yes", "No")) {
				mapDeModeler.buildSaveAll ();
			}
		}
		GUILayout.EndHorizontal ();

		GUILayout.Space (8);

		if (GUILayout.Button("Open Modeling Window")) {
			ChunkModelerWindow toolsWindow = (ChunkModelerWindow) EditorWindow.GetWindow (typeof(ChunkModelerWindow));
			toolsWindow.Init (mapDeModeler);
		}
		GUILayout.BeginHorizontal ();
		if (GUILayout.Button("Enable Edit")) {
			if (EditorUtility.DisplayDialog ("Enabling Edit", "This will make all chunks editable. Continue?", "Yes", "No")) {
				mapDeModeler.enableAllEdit ();
			}
		}
		if (GUILayout.Button("Disable Edit")) {
			if (EditorUtility.DisplayDialog ("Disabling Edit", "This will make all chunks non-editable. Continue?", "Yes", "No")) {
				mapDeModeler.disableAllEdit ();
			}
		}
		GUILayout.EndHorizontal ();
		GUILayout.EndVertical ();




        GUILayout.Space (8);

        GUILayout.BeginVertical ();
        GUILayout.Label ("Perlin Layers", EditorStyles.boldLabel);
        serializedObject.Update ();
        EditorGUIUtility.LookLikeInspector ();
        SerializedProperty spLayers = serializedObject.FindProperty ("perlinLayers");
        EditorGUI.BeginChangeCheck ();
        EditorGUILayout.PropertyField (spLayers, true);
        if (EditorGUI.EndChangeCheck ())
            serializedObject.ApplyModifiedProperties ();
        EditorGUIUtility.LookLikeControls ();
        if (GUILayout.Button ("Apply Perlin Layers")) {
            if (EditorUtility.DisplayDialog ("Applying Perlin Layers", "This action will reset all height data. Continue?", "Yes", "No")) {
                mapDeModeler.applyPerlinLayers ();
            }
        }
        GUILayout.EndVertical ();




        GUILayout.Space (8);

		GUILayout.BeginVertical ();
		GUILayout.Label ("Terrain Settings", EditorStyles.boldLabel);

		// Tile Colors
		GUILayout.BeginHorizontal ();
		serializedObject.Update();
		EditorGUIUtility.LookLikeInspector();
		SerializedProperty spTileColors = serializedObject.FindProperty ("tileColors");
		EditorGUI.BeginChangeCheck();
		EditorGUILayout.PropertyField(spTileColors, true);
		if(EditorGUI.EndChangeCheck())
			serializedObject.ApplyModifiedProperties();
		EditorGUIUtility.LookLikeControls();
		GUILayout.EndHorizontal ();

		// Materials
		GUILayout.BeginHorizontal ();
		serializedObject.Update();
		EditorGUIUtility.LookLikeInspector();
		SerializedProperty spMaterial = serializedObject.FindProperty ("terrainMaterial");
		EditorGUI.BeginChangeCheck();
		EditorGUILayout.PropertyField(spMaterial, true);
		if(EditorGUI.EndChangeCheck())
			serializedObject.ApplyModifiedProperties();
		EditorGUIUtility.LookLikeControls();
		GUILayout.EndHorizontal ();

		GUILayout.BeginHorizontal ();
		serializedObject.Update();
		EditorGUIUtility.LookLikeInspector();
		SerializedProperty spWaterMaterial = serializedObject.FindProperty ("waterMaterial");
		EditorGUI.BeginChangeCheck();
		EditorGUILayout.PropertyField(spWaterMaterial, true);
		if(EditorGUI.EndChangeCheck())
			serializedObject.ApplyModifiedProperties();
		EditorGUIUtility.LookLikeControls();
		GUILayout.EndHorizontal ();

		GUILayout.EndVertical ();

		GUILayout.Space (8);

		GUILayout.BeginVertical ();
		GUILayout.Label ("Areas", EditorStyles.boldLabel);
		serializedObject.Update();
		EditorGUIUtility.LookLikeInspector();
		SerializedProperty spAreas = serializedObject.FindProperty ("gridMap.areas");
		EditorGUI.BeginChangeCheck();
		EditorGUILayout.PropertyField(spAreas, true);
		if(EditorGUI.EndChangeCheck())
			serializedObject.ApplyModifiedProperties();
		EditorGUIUtility.LookLikeControls();
		GUILayout.EndVertical ();
	}
}
