﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Spawner : MonoBehaviour {

    [Header("Elements")]
	public GameObject[] gPrefabs;
	
    [Header("Obstruction")]
    public bool onlyOnEmptyTiles = true;
    public bool obstructsTile = true;
    [Range (1, 32)] public int maxObstructionCheckArea = 2;
    [Range (0, 1)] public float elementTileRadius = 0.8f;
    public LayerMask obstructionCollisionMask;

    [Header("Scaling")]
    public Vector3 minScale = Vector3.one;
    public Vector3 maxScale = Vector3.one;

    [Header("Rotation")]
    public Vector3 minRotate = Vector3.zero;
    public Vector3 maxRotate = Vector3.zero;

    [Header("Jittering")]
	public Vector3 tileJitter = Vector3.zero;

    // Spawn
	public void DoSpawn (ChunkMap chunkMap, int x, int z, ChunkModeler chunkModeler, float distribution) {
		bool canBuild = true;
	
		if (onlyOnEmptyTiles) {
			if (!chunkMap.tileMap [x * GridMap.CHUNK_SIZE + z].isWalkable)
				canBuild = false;
		}

		// continue only if each element wont obstruct the tile, or if the tile is free
		if (canBuild) {
			// check distribution
			if (Random.Range (0.0f, 1.0f) < distribution) {
                // instantiate random gameobject and transform it based on parameters
                GameObject gObject = Instantiate (gPrefabs[Random.Range (0, gPrefabs.Length)]);
#if UNITY_EDITOR
                gObject.transform.parent = chunkModeler.generateNewProceduralObject ();
#endif
                gObject.transform.localPosition =
                new Vector3 (
                        (x + GridMap.TILE_SIZE * 0.5f) + Random.Range (-tileJitter.x, tileJitter.x),
                        (chunkMap.GetMedianHeightOnTile (x, z)) + Random.Range (-tileJitter.y, tileJitter.y),
                        (z + GridMap.TILE_SIZE * 0.5f) + Random.Range (-tileJitter.z, tileJitter.z));
                gObject.transform.localScale = Vector3.Lerp (minScale, maxScale, Random.Range (0f, 1f));
                gObject.transform.Rotate (Vector3.Lerp (minRotate, maxRotate, Random.Range (0f, 1f)));

                // if must obstruct tile, check radius
                if (obstructsTile) {
                    Collider[] collisions = new Collider[1];
                    for (int cx = x - maxObstructionCheckArea; cx <= x + maxObstructionCheckArea; cx++) {
                        for (int cz = z - maxObstructionCheckArea; cz <= z + maxObstructionCheckArea; cz++) {
                            if (cx >= 0 && cz >= 0 && cx < chunkMap.gridSizeX && cz < chunkMap.gridSizeZ) {
                                if (Physics.OverlapCapsuleNonAlloc (
                                    new Vector3 (
                                        ((chunkModeler.position.a * GridMap.CHUNK_SIZE) + cx + GridMap.TILE_SIZE * 0.5f),
                                        (chunkMap.GetMedianHeightOnTile (cx, cz)) - 0.5f,
                                        ((chunkModeler.position.b * GridMap.CHUNK_SIZE) + cz + GridMap.TILE_SIZE * 0.5f)),
                                    new Vector3 (
                                        ((chunkModeler.position.a * GridMap.CHUNK_SIZE) + cx + GridMap.TILE_SIZE * 0.5f),
                                        (chunkMap.GetMedianHeightOnTile (cx, cz)) + 100f,
                                        ((chunkModeler.position.b * GridMap.CHUNK_SIZE) + cz + GridMap.TILE_SIZE * 0.5f)),
                                    elementTileRadius * GridMap.TILE_SIZE * 0.5f,
                                    collisions,
                                    obstructionCollisionMask) > 0) {

                                    chunkMap.tileMap[cx * GridMap.CHUNK_SIZE + cz].isEmpty = false;

                                }
                            }
                        }
                    }
                }

				// TODO merge child object to this object?
				//	repositioning is still not working
				/*
				GameObject mainChild = gObject.transform.GetChild (0).gameObject;

				Mesh mesh = mainChild.GetComponent<MeshFilter>().sharedMesh;
				MeshRenderer childMeshRenderer = mainChild.GetComponent<MeshRenderer> ();

				gObject.AddComponent<MeshFilter> ().sharedMesh = mesh;
				MeshRenderer meshRenderer = gObject.AddComponent<MeshRenderer> ();
				meshRenderer.sharedMaterials = childMeshRenderer.sharedMaterials;
				meshRenderer.shadowCastingMode = childMeshRenderer.shadowCastingMode;
				meshRenderer.receiveShadows = childMeshRenderer.receiveShadows;

				gObject.transform.position += mainChild.transform.localPosition;
				DestroyImmediate (mainChild);
				*/
			}
		}
	}
}
