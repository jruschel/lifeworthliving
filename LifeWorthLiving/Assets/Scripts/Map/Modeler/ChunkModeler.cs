﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System;
using System.Collections.Generic;

[ExecuteInEditMode]
public class ChunkModeler : MonoBehaviour {

	// Defs
	[SerializeField] public ChunkMap chunkMap;
	[SerializeField] public MapModeler mapDeModeler;
	[SerializeField] public Vector2i position;	// what index is this chunk

	// Preview
	[SerializeField] private Mesh previewMesh;
	[SerializeField] private MeshRenderer previewMeshRenderer;
	[SerializeField] private MeshFilter previewMeshFilter;
	[SerializeField] private Texture2D previewTexture = null;
	[SerializeField] bool isEditEnabled=false;

	// Built
	[SerializeField] private GameObject builtTerrainObject, builtWaterObject;
	[SerializeField] private Mesh builtTerrainMesh, builtWaterMesh;

	// Procedural and Dynamic objects
	[SerializeField] private SerialIndexedArray<GameObject> gObjectsProcedural;
	[SerializeField] private GameObject gObjectDynamic;

    #if UNITY_EDITOR

    #region Initing, Enabling, Disabling, Building, Saving, Loading

    public void init(MapModeler _mapDeModeler, Vector2i _position) {
		mapDeModeler = _mapDeModeler;
		position = _position;

		// create grid
		chunkMap = new ChunkMap (GridMap.CHUNK_SIZE, GridMap.CHUNK_SIZE);
		chunkMap.tileColors = mapDeModeler.tileColors;
		mapDeModeler.gridMap.chunkMaps [position.a, position.b] = chunkMap;

		gObjectDynamic = new GameObject ();
		gObjectDynamic.name = "Dynamic objects";
		gObjectDynamic.transform.parent = transform;
		gObjectDynamic.transform.localPosition = Vector3.zero;

		GameObject[] gobjs = new GameObject[GridMap.PROCEDURAL_OBJECTS_COUNT];
		for (int i = 0; i < GridMap.PROCEDURAL_OBJECTS_COUNT; i++) {
			gobjs[i] = new GameObject ();
			gobjs[i].name = "ProceduralObjects " + i;
			gobjs[i].transform.parent = transform;
			gobjs[i].transform.localPosition = Vector3.zero;
		}
		gObjectsProcedural = new SerialIndexedArray<GameObject> (gobjs);

		enableEdit ();
	}

	public void initFromLoad(MapModeler _mapDeModeler, bool _loadAll, Vector2i _position) {
		mapDeModeler = _mapDeModeler;
		position = _position;

		// create grid - only if not loading all (loadAll() will load the gridMap)
		if (!_loadAll) {
			chunkMap = ChunkMap.LoadFromFile ("Assets/Resources/Chunks/" + name + ".asset");
			chunkMap.tileColors = mapDeModeler.tileColors;
			mapDeModeler.gridMap.chunkMaps [position.a, position.b] = chunkMap;

			gObjectDynamic = new GameObject ();
			gObjectDynamic.name = "Dynamic objects";
			gObjectDynamic.transform.parent = transform;
			gObjectDynamic.transform.localPosition = Vector3.zero;

			GameObject[] gobjs = new GameObject[GridMap.PROCEDURAL_OBJECTS_COUNT];
			for (int i = 0; i < GridMap.PROCEDURAL_OBJECTS_COUNT; i++) {
				gobjs[i] = new GameObject ();
				gobjs[i].name = "ProceduralObjects " + i;
				gobjs[i].transform.parent = transform;
				gobjs[i].transform.localPosition = Vector3.zero;
			}
			gObjectsProcedural = new SerialIndexedArray<GameObject> (gobjs);
		} else {
			// load all
			loadAll ();
		}

		enableEdit ();
	}

	public void disableEdit() {
		if (isEditEnabled) {
			isEditEnabled = false;

			if (previewMesh != null)
				DestroyImmediate (previewMesh);
			if (previewMeshRenderer != null)
				previewMeshRenderer.enabled = false;
			if (previewTexture != null)
				DestroyImmediate (previewTexture);
			if (previewMeshFilter != null)
				previewMeshFilter.sharedMesh = null;
			
			this.enabled = false;
		}

		if (builtTerrainObject != null)
			builtTerrainObject.SetActive (true);
		if (builtWaterObject != null)
			builtWaterObject.SetActive (true);
		if (gObjectsProcedural != null)
			foreach (GameObject g in gObjectsProcedural.getAllObjects())
				g.SetActive (true);
		if (gObjectDynamic != null)
			gObjectDynamic.SetActive (true);
	}

	public void enableEdit() {
		if (!isEditEnabled) {
			isEditEnabled = true;

			// create preview mesh
			if (previewMeshFilter == null)
				previewMeshFilter = gameObject.AddComponent<MeshFilter> ();
			previewMesh = new Mesh ();
			previewMesh.vertices = new Vector3[4] { 
				new Vector3 (0, 0, 0),
				new Vector3 (GridMap.CHUNK_SIZE, 0, 0),
				new Vector3 (0, 0, GridMap.CHUNK_SIZE),
				new Vector3 (GridMap.CHUNK_SIZE, 0, GridMap.CHUNK_SIZE)
			};
			previewMesh.triangles = new int[6] {
				0, 2, 3, 0, 3, 1
			};
			previewMesh.uv = new Vector2[4] {
				new Vector2 (0, 0),
				new Vector2 (1, 0),
				new Vector2 (0, 1),
				new Vector2 (1, 1),
			};
			previewMesh.RecalculateNormals ();
			previewMeshFilter.sharedMesh = previewMesh;

			if (previewMeshRenderer == null) {
				previewMeshRenderer = gameObject.AddComponent<MeshRenderer> ();
				previewMeshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
				previewMeshRenderer.receiveShadows = false;
			}
			EditorUtility.SetSelectedWireframeHidden (previewMeshRenderer, true);
			previewMeshRenderer.sharedMaterial = new Material (mapDeModeler.terrainMaterial);

			initPreviewTexture ();

			updateAllPreviewTexture ();

			this.enabled = true;
		}

		previewMeshRenderer.enabled = true;
		if (builtTerrainObject != null)
			builtTerrainObject.SetActive (false);
		if (builtWaterObject != null)
			builtWaterObject.SetActive (false);
		if (gObjectsProcedural != null)
			foreach (GameObject g in gObjectsProcedural.getAllObjects())
				g.SetActive (false);
		if (gObjectDynamic != null)
			gObjectDynamic.SetActive (false);
	}

	public void initPreviewTexture() {
		if (previewTexture == null) {
			previewTexture = new Texture2D (GridMap.CHUNK_SIZE, GridMap.CHUNK_SIZE, TextureFormat.RGB24, false, true);
			previewTexture.filterMode = UnityEngine.FilterMode.Point;
			previewTexture.wrapMode = TextureWrapMode.Clamp;
			previewMeshRenderer.sharedMaterial.mainTexture = previewTexture;
		}
	}

	public void destroyChunk() {
		DestroyImmediate (gameObject);
	}

	public void buildSaveTerrainMesh() {
		// destroy old
		if (builtTerrainObject != null)
			DestroyImmediate (builtTerrainObject);
		if (builtWaterObject != null)
			DestroyImmediate (builtWaterObject);

		// recalculate heights
		chunkMap.calculateVertexHeights (mapDeModeler.gridMap, position);

		// terrain mesh
		TerrainBuilder terrainBuilder = new TerrainBuilder (name, mapDeModeler.terrainMaterial, chunkMap);
		builtTerrainMesh = terrainBuilder.GenerateChunkMesh (0, 0, GridMap.CHUNK_SIZE, GridMap.CHUNK_SIZE);

		builtTerrainObject = new GameObject ();
		builtTerrainObject.name = "Terrain";
		builtTerrainObject.transform.parent = transform;
		builtTerrainObject.transform.localPosition = Vector3.zero;
		builtTerrainObject.layer = LayerMask.NameToLayer("Terrain");

		MeshFilter meshFilter = builtTerrainObject.AddComponent<MeshFilter> ();
		meshFilter.sharedMesh = builtTerrainMesh;

		MeshRenderer meshRenderer = builtTerrainObject.AddComponent<MeshRenderer> ();
		meshRenderer.sharedMaterial = mapDeModeler.terrainMaterial;
		meshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.TwoSided;

		MeshCollider meshCollider = builtTerrainObject.AddComponent<MeshCollider> ();

		// save terrain mesh
		AssetDatabase.CreateAsset (builtTerrainMesh, 
			"Assets/Resources/Terrain/" + name + ".asset");
		AssetDatabase.SaveAssets ();

		// water mesh
		List<float> heightList = new List<float> ();
		builtWaterMesh = terrainBuilder.GenerateChunkWaterMesh (0, 0, GridMap.CHUNK_SIZE, GridMap.CHUNK_SIZE, 
			heightList);
		chunkMap.waterMeshHeights = heightList.ToArray ();

		builtWaterObject = new GameObject ();
		builtWaterObject.name = "Water";
		builtWaterObject.transform.parent = transform;
		builtWaterObject.transform.localPosition = Vector3.zero;
		builtWaterObject.layer = LayerMask.NameToLayer("Water");

		MeshFilter meshFilterWater = builtWaterObject.AddComponent<MeshFilter> ();
		meshFilterWater.sharedMesh = builtWaterMesh;

		MeshRenderer meshRendererWater = builtWaterObject.AddComponent<MeshRenderer> ();
		meshRendererWater.sharedMaterial = mapDeModeler.waterMaterial;
		meshRendererWater.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

		Water waterScript = builtWaterObject.AddComponent<Water> ();
		waterScript.waveHeight = 0.1f;
		waterScript.waveLength = 0.001f;
		waterScript.vertHeights = chunkMap.waterMeshHeights;

		//MeshCollider meshColliderWater = builtWaterObject.AddComponent<MeshCollider> ();

		// save the grid
		saveGrid ();

		// save water mesh
		AssetDatabase.CreateAsset (builtWaterMesh, 
			"Assets/Resources/Terrain/" + name + "_water.asset");
		AssetDatabase.SaveAssets ();

		if (previewMeshRenderer != null)
			previewMeshRenderer.enabled = false;
	}

	public void saveGrid() {
		chunkMap.SaveToFile ("Assets/Resources/Chunks/" + name + ".asset");
	}

	public void generateProceduralObjects () {
		if (gObjectsProcedural != null)
			foreach (GameObject g in gObjectsProcedural.getAllObjects())
				DestroyImmediate (g);
		
		GameObject[] gobjs = new GameObject[GridMap.PROCEDURAL_OBJECTS_COUNT];
		for (int i = 0; i < GridMap.PROCEDURAL_OBJECTS_COUNT; i++) {
			gobjs[i] = new GameObject ();
			gobjs[i].name = "ProceduralObjects " + i;
			gobjs[i].transform.parent = transform;
			gobjs[i].transform.localPosition = Vector3.zero;
		}
		gObjectsProcedural = new SerialIndexedArray<GameObject> (gobjs);

		chunkMap.resetAllTilesToEmpty ();

		for (int i = 0; i < GridMap.CHUNK_SIZE; i++) {
			for (int j = 0; j < GridMap.CHUNK_SIZE; j++) {
				for (int s = 0; s < mapDeModeler.gridMap.areas[
					chunkMap.tileMap[i * GridMap.CHUNK_SIZE + j].areaIndex].spawners.Length; s++) {
					mapDeModeler.gridMap.areas [
						chunkMap.tileMap [i * GridMap.CHUNK_SIZE + j].areaIndex].spawners [s].DoSpawn (
							chunkMap, i, j, this);
				}
			}
		}

		updateAllPreviewTexture ();
	}
	public void saveProceduralObjects () {
		GameObject[] procs = gObjectsProcedural.getAllObjects ();
		for (int i = 0; i < GridMap.PROCEDURAL_OBJECTS_COUNT; i++) {
			PrefabUtility.CreatePrefab ("Assets/Resources/Procedural/" + name + "_" + i + ".prefab",
				procs[i]);
		}

		updateAllPreviewTexture ();
	}
	public void resetProceduralObjects() {
		if (gObjectsProcedural != null)
			foreach (GameObject g in gObjectsProcedural.getAllObjects())
				DestroyImmediate (g);

		GameObject[] gobjs = new GameObject[GridMap.PROCEDURAL_OBJECTS_COUNT];
		for (int i = 0; i < GridMap.PROCEDURAL_OBJECTS_COUNT; i++) {
			gobjs[i] = new GameObject ();
			gobjs[i].name = "ProceduralObjects " + i;
			gobjs[i].transform.parent = transform;
			gobjs[i].transform.localPosition = Vector3.zero;
		}
		gObjectsProcedural = new SerialIndexedArray<GameObject> (gobjs);

		chunkMap.resetAllTilesToEmpty ();

		GameObject[] procs = gObjectsProcedural.getAllObjects ();
		for (int i = 0; i < GridMap.PROCEDURAL_OBJECTS_COUNT; i++) {
			PrefabUtility.CreatePrefab ("Assets/Resources/Procedural/" + name + "_" + i + ".prefab",
				procs[i]);
		}

		updateAllPreviewTexture ();
	}
	public Transform generateNewProceduralObject() {
		return gObjectsProcedural.getNext().transform;
	}

	public void saveDynamicObjects () {
		PrefabUtility.CreatePrefab ("Assets/Resources/Dynamic/" + name + ".prefab",
			gObjectDynamic);
	}

	public void buildSaveAll() {
		saveGrid ();
		buildSaveTerrainMesh ();
		generateProceduralObjects ();
		saveProceduralObjects ();
		saveDynamicObjects ();
	}

	public void loadAll() {
		UnityEngine.Object loadedObject;

		chunkMap = ChunkMap.LoadFromFile ("Assets/Resources/Chunks/" + name + ".asset");
		chunkMap.tileColors = mapDeModeler.tileColors;
		mapDeModeler.gridMap.chunkMaps [position.a, position.b] = chunkMap;

		// try to load terrain mesh - if fail, do nothing
		if (builtTerrainObject != null)
			DestroyImmediate (builtTerrainObject);
		if (builtWaterObject != null)
			DestroyImmediate (builtWaterObject);

		// load terrain mesh
		loadedObject = Resources.Load("Terrain/" + name);
		if (loadedObject != null) {
			builtTerrainObject = new GameObject ();
			builtTerrainObject.name = "Terrain";
			builtTerrainObject.transform.parent = transform;
			builtTerrainObject.transform.localPosition = Vector3.zero;
			builtTerrainObject.layer = LayerMask.NameToLayer ("Terrain");

			MeshFilter meshFilter = builtTerrainObject.AddComponent<MeshFilter> ();
			builtTerrainMesh = loadedObject as Mesh;
			meshFilter.sharedMesh = builtTerrainMesh;

			MeshRenderer meshRenderer = builtTerrainObject.AddComponent<MeshRenderer> ();
			meshRenderer.sharedMaterial = mapDeModeler.terrainMaterial;
			meshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.TwoSided;

			MeshCollider meshCollider = builtTerrainObject.AddComponent<MeshCollider> ();

			if (previewMeshRenderer)
				previewMeshRenderer.enabled = false;
		}

		// load water mesh
		loadedObject = Resources.Load("Terrain/" + name + "_water");
		if (loadedObject != null) {
			builtWaterObject = new GameObject ();
			builtWaterObject.name = "Water";
			builtWaterObject.transform.parent = transform;
			builtWaterObject.transform.localPosition = Vector3.zero;
			builtWaterObject.layer = LayerMask.NameToLayer ("Water");

			MeshFilter meshFilterWater = builtWaterObject.AddComponent<MeshFilter> ();
			builtWaterMesh = loadedObject as Mesh;
			meshFilterWater.sharedMesh = builtWaterMesh;

			MeshRenderer meshRendererWater = builtWaterObject.AddComponent<MeshRenderer> ();
			meshRendererWater.sharedMaterial = mapDeModeler.waterMaterial;
			meshRendererWater.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

			Water waterScript = builtWaterObject.AddComponent<Water> ();
			waterScript.waveHeight = 0.1f;
			waterScript.waveLength = 0.05489128f;

			//MeshCollider meshColliderWater = builtWaterObject.AddComponent<MeshCollider> ();
		}

		// try to load procedural objects - if fail, create empty
		if (gObjectsProcedural != null)
			foreach (GameObject g in gObjectsProcedural.getAllObjects())
				DestroyImmediate (g);

		GameObject[] gobjs = new GameObject[GridMap.PROCEDURAL_OBJECTS_COUNT];
		for (int i = 0; i < GridMap.PROCEDURAL_OBJECTS_COUNT; i++) {
			loadedObject = Resources.Load("Procedural/" + name + "_" + i);
			if (loadedObject != null) {
				gobjs[i] = Instantiate (loadedObject as GameObject);
			} else {
				gobjs[i] = new GameObject ();
			}
			gobjs[i].name = "ProceduralObjects " + i;
			gobjs[i].transform.parent = transform;
			gobjs[i].transform.localPosition = Vector3.zero;
		}
		gObjectsProcedural = new SerialIndexedArray<GameObject> (gobjs);

		// try to load dynamic objects - if fail, create empty
		if (gObjectDynamic != null)
			DestroyImmediate (gObjectDynamic);
		
		loadedObject = Resources.Load("Dynamic/" + name);
		if (loadedObject != null) {
			gObjectDynamic = Instantiate (loadedObject as GameObject);
		} else {
			gObjectDynamic = new GameObject ();
		}
		gObjectDynamic.name = "Dynamic objects";
		gObjectDynamic.transform.parent = transform;
		gObjectDynamic.transform.localPosition = Vector3.zero;

		updateAllPreviewTexture ();
	}

	#endregion

	#region Preview

	public void updateAllPreviewTexture() {
		updatePreviewTexture (0, 0, GridMap.CHUNK_SIZE, GridMap.CHUNK_SIZE);
	}
	private void updatePreviewTexture(int centerX, int centerZ, int sizeX, int sizeZ) {
		initPreviewTexture ();

		for (int i = centerX-sizeX; i <= centerX+sizeX; i++) {
			for (int j = centerZ-sizeZ; j <= centerZ+sizeZ; j++) {
				if (i >= 0 && j >= 0 && i < GridMap.CHUNK_SIZE && j < GridMap.CHUNK_SIZE) {
					Color c = new Color(0.5f, 0.5f, 0.5f);
					bool isCInit = false;

					if (mapDeModeler.viewTileColors) {
						c = mapDeModeler.tileColors [
							(int)chunkMap.tileMap [(i) * chunkMap.gridSizeX + (j)].ground];
						isCInit = true;
					}
					if (mapDeModeler.viewAreas) {
						if (isCInit) {
							c = Color.Lerp (c, mapDeModeler.gridMap.areas[
								chunkMap.tileMap [(i) * chunkMap.gridSizeX + (j)].areaIndex].color,
								0.5f);
						} else {
							c = mapDeModeler.gridMap.areas[
								chunkMap.tileMap [(i) * chunkMap.gridSizeX + (j)].areaIndex].color;
						}
					}
					if (mapDeModeler.viewLevels) {
						float v = ((chunkMap.getMedianLevelOnTile (i,j) / GridMap.LEVEL_N) + 1) * 0.5f;
						c.r *= v;
						c.g *= v;
						c.b *= v;
					}
					if (mapDeModeler.viewWater) {
						if (chunkMap.tileMap [(i) * chunkMap.gridSizeX + (j)].waterLevel > 0) {
							c = Color.Lerp (c, Color.blue, (float)chunkMap.tileMap [(i) * chunkMap.gridSizeX + (j)].waterLevel/GridMap.LEVEL_WATER_MAX);
						}
					}
					if (mapDeModeler.viewObstruction) {
						if (!chunkMap.tileMap [(i) * chunkMap.gridSizeX + (j)].isWalkable) {
							c = Color.Lerp (c, Color.red, 0.4f);
						}
					}

					if (mapDeModeler.viewObscurance) {
						c = c * 
							(1 - (chunkMap.tileMap [(i) * chunkMap.gridSizeX + (j)].obscurance * GridMap.OBSCURANCE_STEP));
					}

					previewTexture.SetPixel (i, j, c);
				}
			}
		}
		previewTexture.Apply ();
	}

	void OnDrawGizmosSelected() {
		Gizmos.color = new Color (1, 1, 1, 0.8f);
		Gizmos.DrawLine (transform.position, transform.position + new Vector3(GridMap.CHUNK_SIZE, 0, 0));
		Gizmos.DrawLine (transform.position + new Vector3(GridMap.CHUNK_SIZE, 0, 0),
			transform.position + new Vector3(GridMap.CHUNK_SIZE, 0, GridMap.CHUNK_SIZE));
		Gizmos.DrawLine (transform.position, transform.position + new Vector3(0, 0, GridMap.CHUNK_SIZE));
		Gizmos.DrawLine (transform.position + new Vector3(0, 0, GridMap.CHUNK_SIZE),
			transform.position + new Vector3(GridMap.CHUNK_SIZE, 0, GridMap.CHUNK_SIZE));

		Vector3 xOf = new Vector3 (1, 0, 0);
		Vector3 zOf = new Vector3 (0, 0, 1);
		Gizmos.DrawLine (transform.position+zOf, transform.position + new Vector3(GridMap.CHUNK_SIZE, 0, 0) + zOf);
		Gizmos.DrawLine (transform.position + new Vector3(GridMap.CHUNK_SIZE, 0, 0) - xOf,
			transform.position + new Vector3(GridMap.CHUNK_SIZE, 0, GridMap.CHUNK_SIZE) - xOf);
		Gizmos.DrawLine (transform.position+xOf, transform.position + new Vector3(0, 0, GridMap.CHUNK_SIZE) + xOf);
		Gizmos.DrawLine (transform.position + new Vector3(0, 0, GridMap.CHUNK_SIZE) - zOf,
			transform.position + new Vector3(GridMap.CHUNK_SIZE, 0, GridMap.CHUNK_SIZE) - zOf);
	}

	#endregion

	#region Brush Tools

	public void applyCurrentTool() {
		int i, j;

		Undo.RecordObject (this, "Applied brush");

		// Tile-oriented
		if (mapDeModeler.modelMode == MapModeler.ModelMode.TileMap) {
			for (i = mapDeModeler.currentMouseTile.a - mapDeModeler.brushRadius; i <= mapDeModeler.currentMouseTile.a + mapDeModeler.brushRadius; i++) {
				for (j = mapDeModeler.currentMouseTile.b - mapDeModeler.brushRadius; j <= mapDeModeler.currentMouseTile.b + mapDeModeler.brushRadius; j++) {
					if (i >= 0 && j >= 0 && i < GridMap.CHUNK_SIZE && j < GridMap.CHUNK_SIZE) {
						if (((i - mapDeModeler.currentMouseTile.a) * (i - mapDeModeler.currentMouseTile.a)) +
							((j - mapDeModeler.currentMouseTile.b) * (j - mapDeModeler.currentMouseTile.b)) <
							(mapDeModeler.brushRadius) * (mapDeModeler.brushRadius)) {

							if (mapDeModeler.filterByArea) {
								if (mapDeModeler.filterByAreaMode == MapModeler.FilterMode.Equals) {
									if (chunkMap.tileMap [i * (GridMap.CHUNK_SIZE) + j].areaIndex != mapDeModeler.filterByAreaValue)
										continue;
								}
								if (mapDeModeler.filterByAreaMode == MapModeler.FilterMode.NotEquals) {
									if (chunkMap.tileMap [i * (GridMap.CHUNK_SIZE) + j].areaIndex == mapDeModeler.filterByAreaValue)
										continue;
								}
							}

							// height
							if (mapDeModeler.paintLevel) {
								chunkMap.vertexMap [i * (GridMap.CHUNK_SIZE + 1) + j].level = mapDeModeler.paintLevelValue;
								chunkMap.vertexMap [(i) * (GridMap.CHUNK_SIZE + 1) + (j + 1)].level = mapDeModeler.paintLevelValue;
								chunkMap.vertexMap [(i + 1) * (GridMap.CHUNK_SIZE + 1) + (j)].level = mapDeModeler.paintLevelValue;
								chunkMap.vertexMap [(i + 1) * (GridMap.CHUNK_SIZE + 1) + (j + 1)].level = mapDeModeler.paintLevelValue;
							}

							// obstruction
							if (mapDeModeler.paintObstruction) {
								chunkMap.tileMap [i * (GridMap.CHUNK_SIZE) + j].isBlocked = mapDeModeler.paintObstructionValue;
							}

							// ground
							if (mapDeModeler.paintGround) {
								chunkMap.tileMap [i * (GridMap.CHUNK_SIZE) + j].ground = mapDeModeler.paintGroundType;
							}

							// area
							if (mapDeModeler.paintArea) {
								chunkMap.tileMap [i * (GridMap.CHUNK_SIZE) + j].areaIndex = mapDeModeler.paintAreaIndex;
							}

							// water
							if (mapDeModeler.paintWater) {
								chunkMap.tileMap [i * (GridMap.CHUNK_SIZE) + j].waterLevel = mapDeModeler.paintWaterLevel;
							}

							// obscurance
							if (mapDeModeler.paintObscurance) {
								chunkMap.tileMap [i * (GridMap.CHUNK_SIZE) + j].obscurance = mapDeModeler.paintObscuranceValue;
							}
						}
					} 
				}
			}
		}

		// Tile-oriented + height
		if (mapDeModeler.modelMode == MapModeler.ModelMode.TiledHeightMap) {
			for (i = mapDeModeler.currentMouseTile.a - mapDeModeler.brushRadius; i <= mapDeModeler.currentMouseTile.a + mapDeModeler.brushRadius; i++) {
				for (j = mapDeModeler.currentMouseTile.b - mapDeModeler.brushRadius; j <= mapDeModeler.currentMouseTile.b + mapDeModeler.brushRadius; j++) {
					if (i >= 0 && j >= 0 && i < GridMap.CHUNK_SIZE && j < GridMap.CHUNK_SIZE) {
						if (mapDeModeler.filterByArea) {
							if (mapDeModeler.filterByAreaMode == MapModeler.FilterMode.Equals) {
								if (chunkMap.tileMap [i * (GridMap.CHUNK_SIZE) + j].areaIndex != mapDeModeler.filterByAreaValue)
									continue;
							}
							if (mapDeModeler.filterByAreaMode == MapModeler.FilterMode.NotEquals) {
								if (chunkMap.tileMap [i * (GridMap.CHUNK_SIZE) + j].areaIndex == mapDeModeler.filterByAreaValue)
									continue;
							}
						}

						if (((i - mapDeModeler.currentMouseTile.a) * (i - mapDeModeler.currentMouseTile.a)) +
						    ((j - mapDeModeler.currentMouseTile.b) * (j - mapDeModeler.currentMouseTile.b)) <
						    (mapDeModeler.brushRadius) * (mapDeModeler.brushRadius)) {

							// height
							if (mapDeModeler.paintLevel) {
								chunkMap.vertexMap [i * (GridMap.CHUNK_SIZE + 1) + j].level = mapDeModeler.paintLevelValue;
								chunkMap.vertexMap [(i) * (GridMap.CHUNK_SIZE + 1) + (j + 1)].level = mapDeModeler.paintLevelValue;
								chunkMap.vertexMap [(i + 1) * (GridMap.CHUNK_SIZE + 1) + (j)].level = mapDeModeler.paintLevelValue;
								chunkMap.vertexMap [(i + 1) * (GridMap.CHUNK_SIZE + 1) + (j + 1)].level = mapDeModeler.paintLevelValue;
							}
						}

						if (((i - mapDeModeler.currentMouseTile.a) * (i - mapDeModeler.currentMouseTile.a)) +
							((j - mapDeModeler.currentMouseTile.b) * (j - mapDeModeler.currentMouseTile.b)) <
							(mapDeModeler.brushRadius+0.4f) * (mapDeModeler.brushRadius+0.4f)) {

							// ground
							if (mapDeModeler.paintGround) {
								chunkMap.tileMap [i * (GridMap.CHUNK_SIZE) + j].ground = mapDeModeler.paintGroundType;
							}

							// obstruction
							if (mapDeModeler.paintObstruction) {
								chunkMap.tileMap [i * (GridMap.CHUNK_SIZE) + j].isBlocked = mapDeModeler.paintObstructionValue;
							}

							// area
							if (mapDeModeler.paintArea) {
								chunkMap.tileMap [i * (GridMap.CHUNK_SIZE) + j].areaIndex = mapDeModeler.paintAreaIndex;
							}

							// water
							if (mapDeModeler.paintWater) {
								chunkMap.tileMap [i * (GridMap.CHUNK_SIZE) + j].waterLevel = mapDeModeler.paintWaterLevel;
							}

							// obscurance
							if (mapDeModeler.paintObscurance) {
								chunkMap.tileMap [i * (GridMap.CHUNK_SIZE) + j].obscurance = mapDeModeler.paintObscuranceValue;
							}
						}
					} 
				}
			}
		}

		// Vertex-oriented
		if (mapDeModeler.modelMode == MapModeler.ModelMode.HeightMap) {
			for (i = mapDeModeler.currentMouseTile.a - mapDeModeler.brushRadius; i < mapDeModeler.currentMouseTile.a + mapDeModeler.brushRadius; i++) {
				for (j = mapDeModeler.currentMouseTile.b - mapDeModeler.brushRadius; j < mapDeModeler.currentMouseTile.b + mapDeModeler.brushRadius; j++) {
					if (i >= 0 && j >= 0 && i < GridMap.CHUNK_SIZE+1 && j < GridMap.CHUNK_SIZE+1) {
						if (((i - mapDeModeler.currentMouseTile.a) * (i - mapDeModeler.currentMouseTile.a)) + 
							((j - mapDeModeler.currentMouseTile.b) * (j - mapDeModeler.currentMouseTile.b)) < 
							mapDeModeler.brushRadius * mapDeModeler.brushRadius) 
						{
							// TODO this filter is weird - will do tile checks on vertex-oriented calculation
							if (mapDeModeler.filterByArea) {
								if (mapDeModeler.filterByAreaMode == MapModeler.FilterMode.Equals) {
									if (chunkMap.tileMap [i * (GridMap.CHUNK_SIZE) + j].areaIndex != mapDeModeler.filterByAreaValue)
										continue;
								}
								if (mapDeModeler.filterByAreaMode == MapModeler.FilterMode.NotEquals) {
									if (chunkMap.tileMap [i * (GridMap.CHUNK_SIZE) + j].areaIndex == mapDeModeler.filterByAreaValue)
										continue;
								}
							}

							// height
							if (mapDeModeler.paintLevel) {
								chunkMap.vertexMap [i * (GridMap.CHUNK_SIZE+1) + j].level = mapDeModeler.paintLevelValue;
							}

							if (i < GridMap.CHUNK_SIZE && j < GridMap.CHUNK_SIZE) {
								// ground
								if (mapDeModeler.paintGround) {
									chunkMap.tileMap [i * (GridMap.CHUNK_SIZE) + j].ground = mapDeModeler.paintGroundType;
								}
								// obstruction
								if (mapDeModeler.paintObstruction) {
									chunkMap.tileMap [i * (GridMap.CHUNK_SIZE) + j].isBlocked = mapDeModeler.paintObstructionValue;
								}
								// area
								if (mapDeModeler.paintArea) {
									chunkMap.tileMap [i * (GridMap.CHUNK_SIZE) + j].areaIndex = mapDeModeler.paintAreaIndex;
								}

								// water
								if (mapDeModeler.paintWater) {
									chunkMap.tileMap [i * (GridMap.CHUNK_SIZE) + j].waterLevel = mapDeModeler.paintWaterLevel;
								}

								// obscurance
								if (mapDeModeler.paintObscurance) {
									chunkMap.tileMap [i * (GridMap.CHUNK_SIZE) + j].obscurance = mapDeModeler.paintObscuranceValue;
								}
							}
						}
					} 
				}
			}
		}

		updatePreviewTexture (mapDeModeler.currentMouseTile.a, mapDeModeler.currentMouseTile.b, 
			mapDeModeler.brushRadius, mapDeModeler.brushRadius);
	}

    #endregion

    #endif
}
