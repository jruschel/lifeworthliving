﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class MapModeler : MonoBehaviour {

    #if UNITY_EDITOR

    #region Misc Variables

    [SerializeField] public GridMap gridMap;

	[SerializeField] public int chunkDimension;
	[SerializeField] private ChunkModeler[,] chunkModels;
	[SerializeField] private bool isInit = false;

	[SerializeField] public Color[] tileColors;
	[SerializeField] public Material terrainMaterial;
	[SerializeField] public Material waterMaterial;

	#endregion

	#region Modeling API

	public void createBlank() {
		Undo.RecordObject (this, "New Map");
		EditorUtility.SetDirty (this);

		clear ();

		gridMap = new GridMap ("Assets/Resources/", chunkDimension, chunkDimension);

		chunkModels = new ChunkModeler[chunkDimension, chunkDimension];
		for (int i = 0; i < chunkDimension; i++) {
			for (int j = 0; j < chunkDimension; j++) {
				GameObject gObject = new GameObject ();
				gObject.name = "chunk_" + i + "_" + j;
				gObject.transform.parent = transform;
				gObject.transform.localPosition = new Vector3 (
					i * GridMap.CHUNK_SIZE,
					0,
					j * GridMap.CHUNK_SIZE);

				chunkModels [i, j] = gObject.AddComponent<ChunkModeler> ();
				chunkModels [i, j].init(this, new Vector2i(i, j));
			}
		}

		isInit = true;
	}

	public void destroyAll() {
		if (isInit) {
			if (chunkModels != null)
				for (int i = 0; i < chunkModels.GetLength(0); i++) {
					for (int j = 0; j < chunkModels.GetLength(1); j++) {
						if (chunkModels [i, j] != null)
							chunkModels [i, j].destroyChunk ();
					}
				}
			chunkModels = null;
		}
	}

	public void saveGrid() {
		for (int i = 0; i < chunkDimension; i++) {
			for (int j = 0; j < chunkDimension; j++) {
				chunkModels [i, j].saveGrid ();
			}
		}
	}

	public void loadGrid() {
		Undo.RecordObject (this, "Load Map");
		EditorUtility.SetDirty (this);

		clear ();

		gridMap = GridMap.LoadFromFile ("Assets/Resources/");
		if (gridMap == null) {
			gridMap = new GridMap ("Assets/Resources/", chunkDimension, chunkDimension);
		}

		chunkModels = new ChunkModeler[chunkDimension, chunkDimension];
		for (int i = 0; i < chunkDimension; i++) {
			for (int j = 0; j < chunkDimension; j++) {
				GameObject gObject = new GameObject ();
				gObject.name = "chunk_" + i + "_" + j;
				gObject.transform.parent = transform;
				gObject.transform.localPosition = new Vector3 (
					i * GridMap.CHUNK_SIZE,
					0,
					j * GridMap.CHUNK_SIZE);

				chunkModels [i, j] = gObject.AddComponent<ChunkModeler> ();
				chunkModels [i, j].initFromLoad(this, false, new Vector2i(i, j));
			}
		}

		isInit = true;
	}

	public void loadMetaGrid() {
		GridMap temp = GridMap.LoadFromFile ("Assets/Resources/");
		if (temp == null) {
			EditorUtility.DisplayDialog ("Error loading meta grid", "The grid map file was not found!", "OK :(");
		}
		gridMap = temp;
	}

	public void saveMetaGrid() {
		gridMap.SaveToFile ();
	}

	public void clear() {
		destroyAll ();
		List<GameObject> gObjectList = new List<GameObject> ();
		foreach (Transform t in transform) {
			gObjectList.Add (t.gameObject);
		}
		foreach (GameObject g in gObjectList)
			DestroyImmediate (g);
	}

	public void buildSaveAll() {
		for (int i = 0; i < chunkDimension; i++) {
			for (int j = 0; j < chunkDimension; j++) {
				chunkModels [i, j].buildSaveAll ();
			}
		}
	}

	public void loadAll() {
		Undo.RecordObject (this, "Load Map");
		EditorUtility.SetDirty (this);

		clear ();

		gridMap = GridMap.LoadFromFile ("Assets/Resources/");
		if (gridMap == null) {
			gridMap = new GridMap ("Assets/Resources/", chunkDimension, chunkDimension);
		}

		chunkModels = new ChunkModeler[chunkDimension, chunkDimension];
		for (int i = 0; i < chunkDimension; i++) {
			for (int j = 0; j < chunkDimension; j++) {
				GameObject gObject = new GameObject ();
				gObject.name = "chunk_" + i + "_" + j;
				gObject.transform.parent = transform;
				gObject.transform.localPosition = new Vector3 (
					i * GridMap.CHUNK_SIZE,
					0,
					j * GridMap.CHUNK_SIZE);

				chunkModels [i, j] = gObject.AddComponent<ChunkModeler> ();
				chunkModels [i, j].initFromLoad(this, true, new Vector2i(i, j));
			}
		}

		isInit = true;
	}

	public void disableAllEdit() {
		for (int i = 0; i < chunkDimension; i++) {
			for (int j = 0; j < chunkDimension; j++) {
				chunkModels [i, j].disableEdit ();
			}
		}
	}

	public void enableAllEdit() {
		for (int i = 0; i < chunkDimension; i++) {
			for (int j = 0; j < chunkDimension; j++) {
				chunkModels [i, j].enableEdit ();
			}
		}
	}

	public void updateAllPreviewTextures() {
		for (int i = 0; i < chunkDimension; i++) {
			for (int j = 0; j < chunkDimension; j++) {
				chunkModels [i, j].updateAllPreviewTexture ();
			}
		}
	}

    #endregion

    [System.Serializable]
    public struct PerlinLayer {
        [SerializeField] public float scaleX, scaleZ;
        [SerializeField] public float weight;
        [SerializeField] public float threshold;

        public float evaluate(float x, float z) {
            float value = ((Mathf.PerlinNoise (x * scaleX, z * scaleZ) - 0.5f) * 2.0f) * weight;
            return (threshold > 0) ? ((value > threshold)? value : 0) : ((threshold > 0) ? ((value < threshold) ? value : 0) : value);
        }
    }

    [SerializeField]
    public PerlinLayer[] perlinLayers;

    public void applyPerlinLayers() {
        for (int i = 0; i < chunkDimension; i++) {
            for (int j = 0; j < chunkDimension; j++) {

                for (int rx = 0; rx <= GridMap.CHUNK_SIZE; rx ++) {
                    for (int rz = 0; rz <= GridMap.CHUNK_SIZE; rz++) {

                        int ax = (i * GridMap.CHUNK_SIZE) + rx;
                        int az = (j * GridMap.CHUNK_SIZE) + rz;

                        float value = 0;
                        for (int p = 0; p < perlinLayers.Length; p++) {
                            value += perlinLayers[p].evaluate (ax, az);
                        }

                        float dx = ax - (chunkDimension * GridMap.CHUNK_SIZE * 0.5f);
                        float dz = az - (chunkDimension * GridMap.CHUNK_SIZE * 0.5f);
                        float hvalue = Mathf.Sqrt (dx * dx + dz * dz) / (chunkDimension * GridMap.CHUNK_SIZE * 0.5f);
                        if (hvalue < 0.94f) {
                            hvalue *= 0.2f;

                            value = Mathf.Lerp (value, -1, hvalue);

                            chunkModels[i, j].chunkMap.vertexMap[(rx * (GridMap.CHUNK_SIZE + 1)) + rz].level =
                                (byte) (GridMap.LEVEL_N2 + (value * GridMap.LEVEL_N2));
                        } else {
                            if (rx < GridMap.CHUNK_SIZE && rz < GridMap.CHUNK_SIZE) {
                                chunkModels[i, j].chunkMap.tileMap[rx * GridMap.CHUNK_SIZE + rz].waterLevel = 2;
                                chunkModels[i, j].chunkMap.tileMap[rx * GridMap.CHUNK_SIZE + rz].ground = GroundType.Dirt;
                            }
                            chunkModels[i, j].chunkMap.vertexMap[(rx * (GridMap.CHUNK_SIZE + 1)) + rz].level =
                                (byte) (GridMap.LEVEL_N2 / 2);
                        }

                        

                    }
                }

                /*for (int rx = 0; rx < GridMap.CHUNK_SIZE; rx++) {
                    for (int rz = 0; rz < GridMap.CHUNK_SIZE; rz++) {
                        if (chunkModels[i, j].chunkMap.vertexMap[(rx * (GridMap.CHUNK_SIZE + 1)) + rz].level < GridMap.LEVEL_N2) {
                            chunkModels[i, j].chunkMap.tileMap[rx * GridMap.CHUNK_SIZE + rz].waterLevel = 2;
                            chunkModels[i, j].chunkMap.tileMap[rx * GridMap.CHUNK_SIZE + rz].ground = GroundType.Dirt;
                        }
                    }
                }*/

                chunkModels[i, j].updateAllPreviewTexture ();

            }
        }
    }

    #endif

    #region Chunk Modeling (brushes and filters)

    // View and modeling
    [SerializeField] public bool viewTileColors=true, viewLevels=false, 
		viewAreas=false, viewObstruction=true, viewWater=true, viewObscurance=true;
	[SerializeField] public ModelMode modelMode;

	// Filters
	// Area
	[SerializeField] public bool filterByArea = false;
	[SerializeField] public FilterMode filterByAreaMode;
	[SerializeField] public byte filterByAreaValue;

	// Brushes
	// Mouse
	[SerializeField] public Vector2i currentMouseTile;
	[SerializeField] public Vector2 currentMousePosition;
	// Brush
	[SerializeField] public bool doToolApply = false;
	[SerializeField] public int brushRadius;
	// Level
	[SerializeField] public bool paintLevel;
	[SerializeField] public byte paintLevelValue;
	// Obstruction
	[SerializeField] public bool paintObstruction;
	[SerializeField] public bool paintObstructionValue;
	[SerializeField] public string[] paintObstructionOptions = new string[2] { "Blocked", "Unblocked" };
	// Ground
	[SerializeField] public bool paintGround;
	[SerializeField] public GroundType paintGroundType;
	// Area
	[SerializeField] public bool paintArea;
	[SerializeField] public byte paintAreaIndex;
	// Water
	[SerializeField] public bool paintWater;
	[SerializeField] public byte paintWaterLevel;
	// Obscurance
	[SerializeField] public bool paintObscurance;
	[SerializeField] public byte paintObscuranceValue;

	public enum ModelMode {
		TileMap,
		TiledHeightMap,
		HeightMap
	}
	public enum FilterMode {
		Equals,
		NotEquals
	}

	#endregion
}
