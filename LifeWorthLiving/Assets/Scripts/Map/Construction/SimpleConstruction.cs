﻿using UnityEngine;
using System.Collections;

/// <summary>
/// A simple construction - minimal object that can be constructed.
/// </summary>
public class SimpleConstruction : MonoBehaviour, IConstruction {

	#region Public Variables

	public int sizeX, sizeZ;
	public int constructionCost, foodCost;

	#endregion

	#region IConstruction

	public Vector2i GetSize() {
		return new Vector2i (sizeX, sizeZ);
	}

	public ResourcesCount GetBuildCost() {
		return new ResourcesCount (constructionCost, foodCost);
	}

	public void SetToPreview() {
		GetComponentInChildren<Collider> ().enabled = false;
	}

	public void SetToBuilt() {
		GetComponentInChildren<Collider> ().enabled = true;
	}

	#endregion
}
