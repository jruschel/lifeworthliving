﻿using UnityEngine;
using System.Collections;

public static class GameSettings {

	/* Joystick mapping:
	 * 	A: Button0
	 * 	B: Button1
	 * 	X: Button2
	 * 	Y: Button3
	 * 	LB: Button4
	 * 	RB: Button5
	 * 	Back: Button6
	 * 	Start: Button7
	 * 
	 */

	public static KeyCode Key_Walk = KeyCode.LeftShift;

	public static KeyCode Key_OpenMap = KeyCode.Tab;
	public static KeyCode Joy_OpenMap = KeyCode.JoystickButton3;

	public static KeyCode Key_Escape = KeyCode.Escape;
	public static KeyCode Joy_Escape = KeyCode.JoystickButton7;

    public static KeyCode Key_PickUp = KeyCode.E;
    public static KeyCode Joy_PickUp = KeyCode.JoystickButton2;

    public static KeyCode Key_Jump = KeyCode.Space;
    public static KeyCode Joy_Jump = KeyCode.JoystickButton0;

    public static KeyCode Key_Shoot = KeyCode.LeftControl;
    public static KeyCode Joy_Shoot = KeyCode.JoystickButton5;

    public static KeyCode Key_WeaponSwitch = KeyCode.Q;
    public static KeyCode Joy_WeaponSwitch = KeyCode.JoystickButton1;

    public static bool IsInput_OpenMap {
		get 
		{
			return Input.GetKeyDown (Key_OpenMap) || Input.GetKeyDown (Joy_OpenMap);
		}
	}

	public static bool IsInput_Escape {
		get 
		{
			return Input.GetKeyDown (Key_Escape) || Input.GetKeyDown (Joy_Escape);
		}
	}

    public static bool IsInput_PickUp {
        get {
            return Input.GetKeyDown (Key_PickUp) || Input.GetKeyDown (Joy_PickUp);
        }
    }

    public static bool IsInput_Jump {
        get {
            return Input.GetKeyDown (Key_Jump) || Input.GetKeyDown (Joy_Jump);
        }
    }

    public static bool IsInput_Shoot {
        get {
            return Input.GetKeyDown (Key_Shoot) || Input.GetKeyDown (Joy_Shoot);
        }
    }

    public static bool IsInput_Shooting {
        get {
            return Input.GetKey (Key_Shoot) || Input.GetKey (Joy_Shoot);
        }
    }

    public static bool IsInput_WeaponSwitch {
        get {
            return Input.GetKeyDown (Key_WeaponSwitch) || Input.GetKeyDown (Joy_WeaponSwitch);
        }
    }

    public static bool IsInput_WeaponSwitching {
        get {
            return Input.GetKey (Key_WeaponSwitch) || Input.GetKey (Joy_WeaponSwitch);
        }
    }
}
