﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Weapon {
	public string name = "Dangerous Weapon";
	public WeaponType wType = WeaponType.Knife;
    public AudioClip shootAudio, pickupAudio;
	public float fireRate;
	public float ammo = 0;
    public float consumption = 0.02f;

	public float damageMultiplier = 1.0f;
	public int buffKnockback = 0;
	public int buffBleed = 0;
	public bool buffInfiniAmmo = false;

	private MonoBehaviour parent;

	private bool firing = false;
	private Coroutine fireCoroutine = null;

	public Weapon(MonoBehaviour _parent) {
		parent = _parent;
	}

    public void giveAmmo(float ammoBonus) {
        ammo = Mathf.Min (1, ammo + ammoBonus);
    }

	// Process a single fire of the weapon.
	//	Used by automatic fire, but can also be used as event callback (from animation, for instance).
	//	In this case, set animation speed to this weapon's fireRate.
	public float fireSingle(Vector3 position, Vector3 direction) {

        if(shootAudio != null)
            AudioManager.Play_Clip (position, shootAudio, (wType==WeaponType.Shotgun)?0.1f : 0.5f);

        RaycastHit[] hits = Physics.RaycastAll (position, direction, 30);
        for (int i = 0; i < hits.Length; i++) {
            if (hits[i].collider.tag == "Enemy") {
                DefaultZombieController zombie = hits[i].collider.gameObject.GetComponent<DefaultZombieController> ();
                zombie.hit (damageMultiplier);
            }
        }

        ammo -= consumption;
        WeaponHUDController.UpdateAmmo (ammo);

        return fireRate;
    }

	// For automatic firing (start and end), with no reggards for animation.
	public void automaticFireStart(Transform transform, Vector3 direction) {
		firing = true;

		if (fireCoroutine != null)
			parent.StopCoroutine (fireCoroutine);

		fireCoroutine = parent.StartCoroutine (AutomaticFire(transform, direction));
	}
	public void automaticFireEnd() {
		if (fireCoroutine != null)
			parent.StopCoroutine (fireCoroutine);
		fireCoroutine = null;

		firing = false;
	}

	private IEnumerator AutomaticFire(Transform transform, Vector3 direction) {
		fireSingle (transform.position, direction);

		yield return new WaitForSeconds (fireRate);
	}
}