﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadSceneOnClick : MonoBehaviour {

	public void LoadByIndex(int sceneIndex)
    {
        float fadeTime = GameObject.Find("Camera").GetComponent<Fading>().beginFade(1);
        GameObject.Find ("MainMenuMusic").GetComponent<AdaMusicChorded> ().finish ();
        StartCoroutine (DoSomething(fadeTime, sceneIndex));
        
    }

    IEnumerator DoSomething(float fadeTime, int sceneIndex)
    {
        yield return new WaitForSeconds(8);
        SceneManager.LoadScene(sceneIndex);
    }
}
