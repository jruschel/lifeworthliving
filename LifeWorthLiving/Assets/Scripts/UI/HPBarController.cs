﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class HPBarController : MonoBehaviour {

	#region Public Variables

	[Range(0,1)]public float hpLevel = 1;
	[Range(0,1)]public float poisonLevel = 0;
	[Range(0,1)]public float ppLevel = 1;
	public float alpha = 4;
	public Transform target;
	public Vector3 target_offset;

	public Transform hpMid, hpRightCap;
	public Transform poisonRightCap, poisonMid, poisonLeftCap;
	public Transform cameraTransform;

	#endregion

	#region Private Variables

	private float hpMidMinScale, hpMidMaxScale;
	private float hpRightCapMinX, hpRightCapMaxX;
	private float poisonRightCapMinX, poisonRightCapMaxX;
	private float poisonMidMinScale, poisonMidMaxScale;
	private float poisonMidMinX, poisonMidMaxX;
	private float poisonLeftCapMinX, poisonLeftCapMaxX;

	private Transform m_Transform;

	#endregion

	// Use this for initialization
	void Start () {
		m_Transform = GetComponent<Transform>();

		hpMidMinScale = 0;
		hpMidMaxScale = 1;

		hpRightCapMinX = -0.91f;
		hpRightCapMaxX = 0;

		poisonRightCapMinX = -0.91f;
		poisonRightCapMaxX = 0;

		poisonMidMinScale = 0;
		poisonMidMaxScale = 1;

		poisonMidMinX = 0;
		poisonMidMaxX = 0.18f;

		poisonLeftCapMinX = 0.91f;
		poisonLeftCapMaxX = 0;
	}
	
	// Update is called once per frame
	void Update () {
		// reposition
		transform.position = target.position + target_offset;

		// billboarding
		transform.LookAt(cameraTransform.position, -Vector3.up);

		// update scales and positions to match the current info
		float alphaScaled;
		alphaScaled = alpha * Time.deltaTime;
		#if UNITY_EDITOR
		alphaScaled = alpha * 0.15f;
		#endif

		hpMid.localScale = Vector3.Lerp(hpMid.localScale, new Vector3 (Mathf.Lerp(hpMidMinScale, hpMidMaxScale, hpLevel), 1, 1), alphaScaled);
		hpRightCap.localPosition = Vector3.Lerp(hpRightCap.localPosition, new Vector3 (Mathf.Lerp(hpRightCapMinX, hpRightCapMaxX, hpLevel), 0, 0), alphaScaled);

		poisonRightCap.localPosition = Vector3.Lerp(poisonRightCap.localPosition, new Vector3 (Mathf.Lerp(poisonRightCapMinX, poisonRightCapMaxX, ppLevel), 0, 0), alphaScaled);

		poisonMid.localScale = Vector3.Lerp(poisonMid.localScale, new Vector3 (Mathf.Lerp(poisonMidMinScale, poisonMidMaxScale, ppLevel-(1-poisonLevel)), 1, 1), alphaScaled);
		poisonMid.localPosition = Vector3.Lerp(poisonMid.localPosition, new Vector3 (Mathf.Lerp(poisonMidMinX, poisonMidMaxX, ppLevel), 0, 0), alphaScaled);

		poisonLeftCap.localPosition = Vector3.Lerp(poisonLeftCap.localPosition, new Vector3 (Mathf.Lerp(poisonLeftCapMinX, poisonLeftCapMaxX, poisonLevel), 0, 0), alphaScaled);
	}
}
