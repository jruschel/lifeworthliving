﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponHUDController : MonoBehaviour {

    public GameObject[] weapon_pistols, weapon_shotguns;
    public Animator[] animators;
    public GameObject fliper;

    public float flipSpeed = 1;

    private int current = 0;
    private float currentRotation = 0;
    private float targetRotation = 180;

    private WeaponType[] info_weapon;
    private float[] info_ammo;

    private bool isFlipping = false;
    private Coroutine flipCoroutine = null;

    float animationStep = 0;

    private static WeaponHUDController Instance;

	// Use this for initialization
	void Start () {
        Instance = this;

        current = 0;
        info_weapon = new WeaponType[2] { WeaponType.None, WeaponType.None };
        info_ammo = new float[2] { 1, 1 };
    }
	
    IEnumerator FlipAnimation() {
        isFlipping = true;

        current = (current + 1) % 2;

        while (animationStep < 0.995f) {

            fliper.transform.localRotation = Quaternion.Euler (
                0, Mathf.Lerp (currentRotation, targetRotation, animationStep), 0);

            if (animationStep > 0.8f) {
                int other = (current + 1) % 2;
                Instance.weapon_pistols[other].SetActive (false);
                Instance.weapon_shotguns[other].SetActive (false);
            }

            animationStep = Mathf.Lerp (animationStep, 1, flipSpeed * Time.deltaTime);
            yield return 0;
        }

        fliper.transform.localRotation = Quaternion.Euler (0, targetRotation, 0);

        if (current == 0) {
            currentRotation = 0;
            targetRotation = 180;
        }
        if (current == 1) {
            currentRotation = -180;
            targetRotation = 0;
        }

        animationStep = 0;
        flipCoroutine = null;

        isFlipping = false;
    }

    public static void Flip(WeaponType weaponType, float ammo) {
       if (Instance.isFlipping) {
            Instance.StartCoroutine (Instance.WaitForFlip(weaponType, ammo));
            return;
        }

        int newCurrent = (Instance.current + 1) % 2;
        Instance.info_ammo[newCurrent] = ammo;
        Instance.info_weapon[newCurrent] = weaponType;
        Instance.animators[newCurrent].SetFloat ("Blend", ammo);

        switch (weaponType) {
            case WeaponType.None:
                Instance.weapon_pistols[newCurrent].SetActive (false);
                Instance.weapon_shotguns[newCurrent].SetActive (false);
                break;
            case WeaponType.Pistol:
                Instance.weapon_pistols[newCurrent].SetActive (true);
                Instance.weapon_shotguns[newCurrent].SetActive (false);
                break;
            case WeaponType.Shotgun:
                Instance.weapon_pistols[newCurrent].SetActive (false);
                Instance.weapon_shotguns[newCurrent].SetActive (true);
                break;
        }

        Instance.flipCoroutine = Instance.StartCoroutine (Instance.FlipAnimation ());
    }

    IEnumerator WaitForFlip (WeaponType weaponType, float ammo) {
        while (isFlipping) {
            yield return 0;
        }

        Flip (weaponType, ammo);
    }

    public static void UpdateAmmo(float ammo) {
        Instance.info_ammo[Instance.current] = ammo;
        Instance.animators[Instance.current].SetFloat ("Blend", ammo);
    }
}
