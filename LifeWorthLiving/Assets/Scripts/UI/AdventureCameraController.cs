﻿using UnityEngine;
using System.Collections;

public class AdventureCameraController : MonoBehaviour {

	[HideInInspector] public Vector3 v_Forward, v_Right;

	[Header("Target")]
	public Transform target;

	[Header("Minimap")]
	public Transform minimapCamera;
	public Camera minimapViewCamera;
	private Rect mvcOriginalRect;
	private bool fullMinimap = false;
	private Coroutine fullMinimapToggleCoroutine;
	public Vector3 minimapOffset = new Vector3(0, 30, 0);

	[Header("Compass")]
	public Transform compassNeedle;
	public float needleWobbleAmount, needleWobbleFrequency;

	[Header("Movement")]
	[Range(0,1)] public float rotationSmoothAlpha = 0.26f;
	public float rotationSpeed = 100;
	private float rotationSpeedAngle = 0;

	Vector3 lastTargetPosition;

	Transform m_Transform;

	// Use this for initialization
	void Start () {
		m_Transform = transform;
		lastTargetPosition = target.position;

		mvcOriginalRect = minimapViewCamera.rect;
	}
	
	// Update is called once per frame
	void Update () {
		m_Transform.position = m_Transform.position + (target.position - lastTargetPosition);
		lastTargetPosition = target.position;

		float currentRotation = Input.GetAxis ("Mouse X");
		if (Mathf.Abs (currentRotation) > 0.1f) {
			rotationSpeedAngle = Mathf.Lerp (rotationSpeedAngle, currentRotation, rotationSmoothAlpha);
		} else {
			rotationSpeedAngle = Mathf.Lerp (rotationSpeedAngle, 0, rotationSmoothAlpha);
		}

		float rotation = rotationSpeedAngle * rotationSpeed * Time.deltaTime;

		m_Transform.RotateAround (target.position, Vector3.up, rotation);

		compassNeedle.RotateAround (compassNeedle.transform.position, Vector3.up, -rotation +
			Mathf.Sin(needleWobbleFrequency * Time.time) * needleWobbleAmount);
		compassNeedle.RotateAround (compassNeedle.transform.position, compassNeedle.forward, 
			Mathf.Sin(needleWobbleFrequency * Time.time) * needleWobbleAmount * 0.6f);
		minimapCamera.position = target.position + minimapOffset;
		minimapCamera.RotateAround (minimapCamera.position, Vector3.up, rotation);

		v_Forward = m_Transform.forward;
		v_Forward.y = 0;
		v_Forward.Normalize ();
		v_Right = m_Transform.right;
		v_Right.y = 0;
		v_Right.Normalize ();

		if (GameSettings.IsInput_OpenMap) {
			toggleFullMinimap ();
		}

		// Code for listening to any key
		/*if (Input.anyKey) {
			foreach (KeyCode kcode in System.Enum.GetValues(typeof(KeyCode))) {
				if (Input.GetKeyDown(kcode)) {
					Debug.Log (kcode);
				}
			}
		}*/
	}

	public void toggleFullMinimap() {
		if (fullMinimapToggleCoroutine != null)
			StopCoroutine (fullMinimapToggleCoroutine);
		fullMinimap = !fullMinimap;
		fullMinimapToggleCoroutine = StartCoroutine (AnimateToggleFullMinimap());
	}

	private IEnumerator AnimateToggleFullMinimap(float alpha = 0.15f) {
		Rect finalRect = (fullMinimap)? new Rect(0, 0, 1, 1) : mvcOriginalRect ;
		Rect currentRect = minimapViewCamera.rect;

		while (true) {
			currentRect.x = Mathf.Lerp (currentRect.x, finalRect.x, alpha);
			currentRect.y = Mathf.Lerp (currentRect.y, finalRect.y, alpha);
			currentRect.width = Mathf.Lerp (currentRect.width, finalRect.width, alpha);
			currentRect.height = Mathf.Lerp (currentRect.height, finalRect.height, alpha);

			minimapViewCamera.rect = currentRect;

			if (Mathf.Abs (currentRect.x - finalRect.x) < 0.005f && Mathf.Abs (currentRect.y - finalRect.y) < 0.005f &&
			    Mathf.Abs (currentRect.width - finalRect.width) < 0.005f && Mathf.Abs (currentRect.height - finalRect.height) < 0.005f)
				break;

			yield return new WaitForFixedUpdate ();
		}

		minimapViewCamera.rect = finalRect;
	}
}
