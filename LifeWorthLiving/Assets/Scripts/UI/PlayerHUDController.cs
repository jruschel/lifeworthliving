﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHUDController : MonoBehaviour {

    private static PlayerHUDController Instance;

    public GameObject objectHP, objectHunger;

	// Use this for initialization
	void Start () {
        Instance = this;

    }

    public static void SetHP(float value) {
        Instance.objectHP.transform.localScale = new Vector3 (value, 0.2f, 1);
    }

    public static void SetHunger(float value) {
        Instance.objectHunger.transform.localScale = new Vector3 (value, 0.2f, 1);
    }

}
