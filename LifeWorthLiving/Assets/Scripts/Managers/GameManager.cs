﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

/// <summary>
/// The legendary Game Manager - the ultimate singleton.
/// </summary>
public class GameManager : MonoBehaviour {

	#region Public Variables

	public GameState gameState = GameState.FirstLoad;		// don't change this :)

    public Camera mainCamera;
	public MapController mapController;

    #endregion

    #region Private Variables

    private Transform m_Transform;

	#endregion

	#region Control Methods

	void Awake() {
		Instance = this;
	}

	// Use this for initialization
	void Start () {
		m_Transform = transform;

		loadGame ();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            SceneManager.LoadScene (0);
        }

		switch (gameState) {

		//	MENU
		case GameState.MainMenu:
			break;

		// PLAYING
		case GameState.Playing:

		
		// PAUSED
		case GameState.Paused:
			break;
		}
	}

	#endregion

	#region Game Flow

	protected void loadGame() {
		// ...
		gameState = GameState.Playing;	// TODO: change to MainMenu
	}

	protected void startGame() {
		// ...
		gameState = GameState.Playing;
	}

	protected void pauseGame() {
		// ...
		gameState = GameState.Paused;
	}
	protected void resumeGame() {
		// ...
		gameState = GameState.Playing;
	}

	#endregion

	#region Private Methods

	#endregion

	#region Singleton

	public static GameManager Instance;

	#endregion
}