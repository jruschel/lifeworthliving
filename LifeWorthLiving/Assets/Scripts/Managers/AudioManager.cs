﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

	#region Constants

	public const int AUDIO_SOURCE_POOL_SIZE = 32;		// max number of clips being played at the same time

	#endregion

	#region Public Variables

	[Header("Steps Audio Clips")]
	public AudioClip[] steps_grass;		// play random clip from array
	public AudioClip[] steps_dirt;
	public AudioClip[] steps_stone;

	[Header("Weapon Audio Clips")]
	public AudioClip weapon_knife;
	public AudioClip weapon_pistol;

	#endregion

	#region API

	public static void Play_Clip(Vector3 worldPosition, AudioClip audioClip, float volumeScale = 1) {
		AudioSource audioSource = Instance.audioSourcePool.getNext ();
		audioSource.PlayOneShot (audioClip, volumeScale);
		audioSource.gameObject.transform.position = worldPosition;
	}

	public static void Play_RandomClip(Vector3 worldPosition, AudioClip[] audioClips, float volumeScale = 1) {
		AudioSource audioSource = Instance.audioSourcePool.getNext ();
		audioSource.PlayOneShot (audioClips[Random.Range(0, audioClips.Length)], volumeScale);
		audioSource.gameObject.transform.position = worldPosition;
	}

	public static void Play_StepGrass(Vector3 worldPosition, float volumeScale = 1) {
		Play_RandomClip (worldPosition, Instance.steps_grass, volumeScale);
	}

	public static void Play_StepDirt(Vector3 worldPosition, float volumeScale = 1) {
		Play_RandomClip (worldPosition, Instance.steps_dirt, volumeScale);
	}

	public static void Play_StepStone(Vector3 worldPosition, float volumeScale = 1) {
		Play_RandomClip (worldPosition, Instance.steps_stone, volumeScale);
	}

	public static void Play_WeaponKnife(Vector3 worldPosition, float volumeScale = 1) {
		Play_Clip (worldPosition, Instance.weapon_knife, volumeScale);
	}

	public static void Play_WeaponPistol(Vector3 worldPosition, float volumeScale = 1) {
		Play_Clip (worldPosition, Instance.weapon_pistol, volumeScale);
	}

	// TODO ... add other default sounds
	//	default sounds are: steps, grunts (zombies, player), shots (for each weapon)

	#endregion

	#region Control Methods

	private IndexedArray<AudioSource> audioSourcePool;
	private static AudioManager Instance;

	// Initialization
	void Awake () {
		Instance = this;

		// initialize audio sources
		AudioSource[] sources = new AudioSource[AUDIO_SOURCE_POOL_SIZE];
		for (int i = 0; i < AUDIO_SOURCE_POOL_SIZE; i++) {
			GameObject gObject = new GameObject ();
			gObject.transform.parent = transform;
			gObject.name = "AudioSource " + i;
			sources [i] = gObject.AddComponent<AudioSource> ();
			sources [i].playOnAwake = false;
		}
		audioSourcePool = new IndexedArray<AudioSource> (sources);
	}

	#endregion

}
