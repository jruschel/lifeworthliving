﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleObjectsProvider : MonoBehaviour {

    // Sprites
    [Header ("Sprites")]
    public Sprite sb_blank;
    public Sprite sb_x;

    private static SimpleObjectsProvider Instance;

	void Awake () {
        Instance = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public static GameObject Request_ButtonSpriteX(Color mainColor, float shadowScale, Color shadowColor) {
        GameObject gObject = new GameObject ();

        gObject.layer = LayerMask.NameToLayer ("UI");
        SpriteRenderer spriteRenderer = gObject.AddComponent<SpriteRenderer> ();
        spriteRenderer.sprite = Instance.sb_x;
        spriteRenderer.color = mainColor;
        gObject.AddComponent<BillboardObject> ();
        gObject.transform.localScale = Vector3.one * 0.5f;

        if (shadowScale > 0) {
            GameObject gShadowObject = new GameObject ();
            gShadowObject.transform.parent = gObject.transform;

            gShadowObject.layer = LayerMask.NameToLayer ("UI");
            SpriteRenderer shadowSpriteRenderer = gShadowObject.AddComponent<SpriteRenderer> ();
            shadowSpriteRenderer.sprite = Instance.sb_blank;
            shadowSpriteRenderer.color = shadowColor;
            shadowSpriteRenderer.sortingOrder = spriteRenderer.sortingOrder - 1;
            gShadowObject.transform.localScale = Vector3.one * shadowScale;
        }

        return gObject;
    }

    public static void ReturnObject(GameObject gObject) {
        Destroy (gObject);
    }
}
